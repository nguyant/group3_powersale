package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "WishAndCart", catalog = "psdb", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WishAndCart.findAll", query = "SELECT w FROM WishAndCart w"),
    @NamedQuery(name = "WishAndCart.findByWishlistID", query = "SELECT w FROM WishAndCart w WHERE w.wishlistID = :wishlistID"),
    @NamedQuery(name = "WishAndCart.findByDateCreated", query = "SELECT w FROM WishAndCart w WHERE w.dateCreated = :dateCreated"),
    @NamedQuery(name = "WishAndCart.findByIsCart", query = "SELECT w FROM WishAndCart w WHERE w.isCart = :isCart")})
public class WishAndCart implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "WishlistID", nullable = false)
    private Integer wishlistID;

    @Column(name = "DateCreated")
    @Temporal(TemporalType.DATE)
    private Date dateCreated;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IsCart", nullable = false)
    private int isCart;

    @JoinColumn(name = "ProductID", referencedColumnName = "ProductID", nullable = false)
    @ManyToOne(optional = false)
    private Products productID;

    @JoinColumn(name = "CustomerID", referencedColumnName = "CustomerID", nullable = false)
    @ManyToOne(optional = false)
    private Customers customerID;

    public WishAndCart() {
    }

    public WishAndCart(Integer wishlistID) {
        this.wishlistID = wishlistID;
    }

    public WishAndCart(Integer wishlistID, int isCart) {
        this.wishlistID = wishlistID;
        this.isCart = isCart;
    }

    public Integer getWishlistID() {
        return wishlistID;
    }

    public void setWishlistID(Integer wishlistID) {
        this.wishlistID = wishlistID;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getIsCart() {
        return isCart;
    }

    public void setIsCart(int isCart) {
        this.isCart = isCart;
    }

    public Products getProductID() {
        return productID;
    }

    public void setProductID(Products productID) {
        this.productID = productID;
    }

    public Customers getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customers customerID) {
        this.customerID = customerID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wishlistID != null ? wishlistID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WishAndCart)) {
            return false;
        }
        WishAndCart other = (WishAndCart) object;
        if ((this.wishlistID == null && other.wishlistID != null) || (this.wishlistID != null && !this.wishlistID.equals(other.wishlistID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.WishAndCart[ wishlistID=" + wishlistID + " ]";
    }

}
