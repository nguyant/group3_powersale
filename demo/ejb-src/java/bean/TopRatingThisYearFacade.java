package bean;

import entity.TopRatingThisYear;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class TopRatingThisYearFacade extends AbstractFacade<TopRatingThisYear> implements TopRatingThisYearFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TopRatingThisYearFacade() {
        super(TopRatingThisYear.class);
    }

}
