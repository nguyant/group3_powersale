package bean;

import entity.TopRatingThisYear;
import java.util.List;
import javax.ejb.Local;

@Local
public interface TopRatingThisYearFacadeLocal {

    void create(TopRatingThisYear topRatingThisYear);

    void edit(TopRatingThisYear topRatingThisYear);

    void remove(TopRatingThisYear topRatingThisYear);

    TopRatingThisYear find(Object id);

    List<TopRatingThisYear> findAll();

    List<TopRatingThisYear> findRange(int[] range);

    int count();

}
