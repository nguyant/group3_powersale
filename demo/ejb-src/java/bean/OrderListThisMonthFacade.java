package bean;

import entity.OrderListThisMonth;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class OrderListThisMonthFacade extends AbstractFacade<OrderListThisMonth> implements OrderListThisMonthFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrderListThisMonthFacade() {
        super(OrderListThisMonth.class);
    }

}
