package bean;

import entity.TopSellingThisYear;
import java.util.List;
import javax.ejb.Local;

@Local
public interface TopSellingThisYearFacadeLocal {

    void create(TopSellingThisYear topSellingThisYear);

    void edit(TopSellingThisYear topSellingThisYear);

    void remove(TopSellingThisYear topSellingThisYear);

    TopSellingThisYear find(Object id);

    List<TopSellingThisYear> findAll();

    List<TopSellingThisYear> findRange(int[] range);

    int count();

}
