package bean;

import entity.Customers;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class CustomersFacade extends AbstractFacade<Customers> implements CustomersFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomersFacade() {
        super(Customers.class);
    }

    @Override
    public List<Customers> login(String email, String pass) {
        Query q = getEntityManager().createQuery("SELECT c FROM Customers c WHERE c.email = :email and c.password = :pass");
        q.setParameter("email", email);
        q.setParameter("pass", pass);
        return q.getResultList();
    }

    @Override
    public List<Customers> findByMail(String email) {
        Query q = getEntityManager().createQuery("SELECT c FROM Customers c WHERE c.email = :email");
        q.setParameter("email", email);
        return q.getResultList();
    }

}
