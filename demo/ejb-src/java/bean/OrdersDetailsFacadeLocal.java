package bean;

import entity.OrdersDetails;
import java.util.List;
import javax.ejb.Local;

@Local
public interface OrdersDetailsFacadeLocal {

    void create(OrdersDetails ordersDetails);

    void edit(OrdersDetails ordersDetails);

    void remove(OrdersDetails ordersDetails);

    OrdersDetails find(Object id);

    List<OrdersDetails> findAll();

    List<OrdersDetails> findRange(int[] range);

    int count();

    List<OrdersDetails> findByOrderId(String id);

}
