package bean;

import entity.Comments;
import java.util.List;
import javax.ejb.Local;

@Local
public interface CommentsFacadeLocal {

    void create(Comments comments);

    void edit(Comments comments);

    void remove(Comments comments);

    Comments find(Object id);

    List<Comments> findAll();

    List<Comments> findRange(int[] range);

    int count();

    List<Comments> AllRatingByProductID(String proid);

}
