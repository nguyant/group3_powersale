package bean;

import entity.AverageRatings;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class AverageRatingsFacade extends AbstractFacade<AverageRatings> implements AverageRatingsFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AverageRatingsFacade() {
        super(AverageRatings.class);
    }

}
