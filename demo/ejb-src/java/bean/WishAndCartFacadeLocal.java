package bean;

import entity.WishAndCart;
import java.util.List;
import javax.ejb.Local;

@Local
public interface WishAndCartFacadeLocal {

    void create(WishAndCart wishAndCart);

    void edit(WishAndCart wishAndCart);

    void remove(WishAndCart wishAndCart);

    WishAndCart find(Object id);

    List<WishAndCart> findAll();

    List<WishAndCart> findRange(int[] range);

    int count();

    List<WishAndCart> findbyCustomer(String cusId);

    List<WishAndCart> findbyProduct(String productId, String cusId);

}
