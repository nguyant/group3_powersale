package bean;

import entity.Brands;
import java.util.List;
import javax.ejb.Local;

@Local
public interface BrandsFacadeLocal {

    void create(Brands brands);

    void edit(Brands brands);

    void remove(Brands brands);

    Brands find(Object id);

    List<Brands> findAll();

    List<Brands> findRange(int[] range);

    int count();

    List<Brands> AllBrands();

}
