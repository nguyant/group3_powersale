package bean;

import entity.TopSellingThisYear;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class TopSellingThisYearFacade extends AbstractFacade<TopSellingThisYear> implements TopSellingThisYearFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TopSellingThisYearFacade() {
        super(TopSellingThisYear.class);
    }

}
