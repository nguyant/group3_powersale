package bean;

import entity.WishAndCart;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class WishAndCartFacade extends AbstractFacade<WishAndCart> implements WishAndCartFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WishAndCartFacade() {
        super(WishAndCart.class);
    }

    @Override
    public List<WishAndCart> findbyCustomer(String cusId) {
        Query q = em.createQuery("SELECT w FROM WishAndCart w WHERE w.customerID.customerID = :cusId");
        q.setParameter("cusId", cusId);
        return q.getResultList();
    }

    @Override
    public List<WishAndCart> findbyProduct(String productId, String cusId) {
        Query q = em.createQuery("SELECT w FROM WishAndCart w WHERE w.productID.productID = :productId and w.customerID.customerID = :cusId");
        q.setParameter("productId", productId);
        q.setParameter("cusId", cusId);
        return q.getResultList();

    }

}
