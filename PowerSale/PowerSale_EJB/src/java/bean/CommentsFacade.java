package bean;

import entity.Comments;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class CommentsFacade extends AbstractFacade<Comments> implements CommentsFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommentsFacade() {
        super(Comments.class);
    }

    @Override
    public List<Comments> AllCommentsByProductID(String proid) {
        Query q = getEntityManager().createQuery("SELECT c FROM Comments c WHERE c.productID.productID = :proid AND c.isDisplay = 1");
        q.setParameter("proid", proid);
        return q.getResultList();
    }

}
