package bean;

import entity.Brands;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class BrandsFacade extends AbstractFacade<Brands> implements BrandsFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BrandsFacade() {
        super(Brands.class);
    }

    @Override
    public List<Brands> AllBrands() {
        Query q = getEntityManager().createQuery("SELECT b FROM Brands b WHERE b.isStatus = :status");
        boolean status = true;
        q.setParameter("status", status);
        return q.getResultList();
    }

}
