package bean;

import entity.Admins;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class AdminsFacade extends AbstractFacade<Admins> implements AdminsFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminsFacade() {
        super(Admins.class);
    }

    @Override
    public List<Admins> login(String username, String password) {
        Query q = getEntityManager().createQuery("SELECT a FROM Admins a WHERE a.email = :username and a.password = :password");
        q.setParameter("username", username);
        q.setParameter("password", password);
        return q.getResultList();
    }

}
