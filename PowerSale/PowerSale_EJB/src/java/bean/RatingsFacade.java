package bean;

import entity.Orders;
import entity.Ratings;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class RatingsFacade extends AbstractFacade<Ratings> implements RatingsFacadeLocal {

    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RatingsFacade() {
        super(Ratings.class);
    }

    @Override
    public List<Ratings> AllRatingByProductID(String proid) {
        Query q = getEntityManager().createQuery("SELECT r FROM Ratings r WHERE r.productID.productID = :proid");
        q.setParameter("proid", proid);
        return q.getResultList();
    }
    
    @Override
    public List<Ratings> DateRangeReport(Date startDate, Date endDate) {
        Query q = em.createQuery("SELECT r FROM Ratings r WHERE r.ratingDate >= :startDate and r.ratingDate <= :endDate");
        q.setParameter("startDate", startDate);
        q.setParameter("endDate", endDate);
        return q.getResultList();
    }

}
