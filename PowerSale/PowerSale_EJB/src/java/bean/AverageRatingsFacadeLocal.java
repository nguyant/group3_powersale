package bean;

import entity.AverageRatings;
import java.util.List;
import javax.ejb.Local;

@Local
public interface AverageRatingsFacadeLocal {

    void create(AverageRatings averageRatings);

    void edit(AverageRatings averageRatings);

    void remove(AverageRatings averageRatings);

    AverageRatings find(Object id);

    List<AverageRatings> findAll();

    List<AverageRatings> findRange(int[] range);

    int count();

}
