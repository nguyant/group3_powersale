package bean;

import entity.Ratings;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

@Local
public interface RatingsFacadeLocal {

    void create(Ratings ratings);

    void edit(Ratings ratings);

    void remove(Ratings ratings);

    Ratings find(Object id);

    List<Ratings> findAll();

    List<Ratings> findRange(int[] range);

    int count();

    List<Ratings> AllRatingByProductID(String proid);
    
    List<Ratings> DateRangeReport(Date startDate, Date endDate);

}
