package bean;

import entity.Customers;
import java.util.List;
import javax.ejb.Local;

@Local
public interface CustomersFacadeLocal {

    void create(Customers customers);

    void edit(Customers customers);

    void remove(Customers customers);

    Customers find(Object id);

    List<Customers> findAll();

    List<Customers> findRange(int[] range);

    int count();

    List<Customers> login(String email, String pass);

    List<Customers> findByMail(String email);

}
