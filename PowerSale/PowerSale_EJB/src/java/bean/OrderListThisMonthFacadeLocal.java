package bean;

import entity.OrderListThisMonth;
import java.util.List;
import javax.ejb.Local;

@Local
public interface OrderListThisMonthFacadeLocal {

    void create(OrderListThisMonth orderListThisMonth);

    void edit(OrderListThisMonth orderListThisMonth);

    void remove(OrderListThisMonth orderListThisMonth);

    OrderListThisMonth find(Object id);

    List<OrderListThisMonth> findAll();

    List<OrderListThisMonth> findRange(int[] range);

    int count();
    
}
