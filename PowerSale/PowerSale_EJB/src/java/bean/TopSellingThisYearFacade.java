/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import entity.TopSellingThisYear;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author KL
 */
@Stateless
public class TopSellingThisYearFacade extends AbstractFacade<TopSellingThisYear> implements TopSellingThisYearFacadeLocal {
    @PersistenceContext(unitName = "PowerSale_EJBPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TopSellingThisYearFacade() {
        super(TopSellingThisYear.class);
    }
    
}
