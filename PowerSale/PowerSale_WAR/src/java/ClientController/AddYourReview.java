package ClientController;

import bean.CommentsFacadeLocal;
import bean.CustomersFacadeLocal;
import bean.ProductsFacadeLocal;
import bean.RatingsFacadeLocal;
import entity.Comments;
import entity.Ratings;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddYourReview extends HttpServlet {

    @EJB
    RatingsFacadeLocal ratingFacade;
    @EJB
    CommentsFacadeLocal commentFacade;
    @EJB
    ProductsFacadeLocal proFacade;
    @EJB
    CustomersFacadeLocal cusFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String command = request.getParameter("command");
        String cusid = request.getParameter("cusid");
        String proid = request.getParameter("proid");

        if (command.equals("rating")) {
            int rating = Integer.parseInt(request.getParameter("rating"));
            try {
                Ratings ra = new Ratings();
                ra.setProductID(proFacade.find(proid));
                ra.setCustomerID(cusFacade.find(cusid));
                ra.setRate(rating);
                Date date = new Date();
                ra.setRatingDate(date);
                ratingFacade.create(ra);
            } catch (Exception e) {
            }
        } else if (command.equals("comment")) {
            String comment = request.getParameter("comment");
            try {
                Comments ra = new Comments();
                ra.setProductID(proFacade.find(proid));
                ra.setCustomerID(cusFacade.find(cusid));
                ra.setContent(comment);
                Date date = new Date();
                ra.setCommentDate(date);
                commentFacade.create(ra);
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
