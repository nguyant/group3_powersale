package ClientController;

import bean.CustomersFacadeLocal;
import bean.ProductsFacadeLocal;
import bean.WishAndCartFacadeLocal;
import entity.Customers;
import entity.Products;
import entity.WishAndCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddProductToCart extends HttpServlet {

    @EJB
    ProductsFacadeLocal productsFacade;
    @EJB
    WishAndCartFacadeLocal wishAndCartFacade;
    @EJB
    CustomersFacadeLocal customersFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String command = request.getParameter("command");
        String productID = request.getParameter("productID");
        String customerID = request.getParameter("cusID");

        try {
            Products products = productsFacade.find(productID);
            Customers customers = customersFacade.find(customerID);
            List<WishAndCart> currentlist = wishAndCartFacade.findbyProduct(productID, customerID);
            WishAndCart wishandcart = new WishAndCart();

            if (command.equals("plus")) {
                if (currentlist.isEmpty()) {
                    wishandcart.setWishlistID(wishAndCartFacade.findAll().size() + 1);
                    wishandcart.setProductID(products);
                    wishandcart.setCustomerID(customers);
                    Date date = new Date();
                    wishandcart.setDateCreated(date);
                    wishandcart.setIsCart(1);
                    wishAndCartFacade.create(wishandcart);
                } else {
                    if (currentlist.size() == 1) {
                        if (currentlist.get(0).getIsCart() == 0) {
                            wishandcart.setWishlistID(wishAndCartFacade.findAll().size() + 1);
                            wishandcart.setProductID(products);
                            wishandcart.setCustomerID(customers);
                            Date date = new Date();
                            wishandcart.setDateCreated(date);
                            wishandcart.setIsCart(1);
                            wishAndCartFacade.create(wishandcart);
                        } else if (currentlist.get(0).getIsCart() != 0) {
                            int curQuantity = currentlist.get(0).getIsCart() + 1;
                            wishandcart.setIsCart(curQuantity);
                            wishAndCartFacade.edit(wishandcart);
                        }
                    } else if (currentlist.size() == 2) {
                        for (WishAndCart item : currentlist) {
                            if (item.getIsCart() != 0) {
                                int curQuantity = item.getIsCart() + 1;
                                item.setIsCart(curQuantity);
                                wishAndCartFacade.edit(item);
                            }
                        }
                    }

                }
            } else if (command.equals("remove")) {
                for (WishAndCart item : currentlist) {
                    if(item.getIsCart() != 0) {
                        wishAndCartFacade.remove(item);
                    }
                }
                
            }

            List<Products> wishlist = new ArrayList<>();
            List<Products> cartlist = new ArrayList<>();

            HttpSession session = request.getSession();
            for (WishAndCart item : wishAndCartFacade.findbyCustomer(customerID)) {
                if (item.getIsCart() == 0) {
                    wishlist.add(item.getProductID());
                } else {
                    cartlist.add(item.getProductID());
                }
            }
            session.setAttribute("wishlist", wishlist);
            session.setAttribute("cart", cartlist);
            session.setAttribute("countWishlist", wishlist.size());
            session.setAttribute("countCart", cartlist.size());

        } catch (Exception e) {
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
