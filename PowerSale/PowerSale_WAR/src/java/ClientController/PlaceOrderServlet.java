package ClientController;

import bean.OrdersDetailsFacadeLocal;
import bean.OrdersFacadeLocal;
import bean.ProductsFacadeLocal;
import bean.WishAndCartFacadeLocal;
import entity.Customers;
import entity.Orders;
import entity.OrdersDetails;
import entity.Products;
import entity.WishAndCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PlaceOrderServlet extends HttpServlet {

    @EJB
    ProductsFacadeLocal prosFacade;
    @EJB
    OrdersFacadeLocal ordersFacade;
    @EJB
    OrdersDetailsFacadeLocal ordersDetailsFacade;
    @EJB
    WishAndCartFacadeLocal wishAndCartFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int num = ordersFacade.count() + 1;
        String id = num + "";
        int lenNum = 7;
        int lenZero = lenNum - id.length();
        for (int i = 0; i < lenZero; i++) {
            id = "0" + id;
        }

        String orderid = "OD" + id;
        String Shipname = request.getParameter("first-name") + " " + request.getParameter("last-name");
        String ShipPhone = request.getParameter("tel");
        String ShipAddress = request.getParameter("address");
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String ShipDate = formatter.format(date);
        String Shipnote = request.getParameter("shipnote");
        String OrderDate = formatter.format(date);
        String processStt = request.getParameter("processstt");
        String payMethod = request.getParameter("payment");

        HttpSession session = request.getSession();

        Customers cus = (Customers) session.getAttribute("login_account");
        Map<Products, WishAndCart> cartquantity = new HashMap<>();

        for (WishAndCart item : wishAndCartFacade.findbyCustomer(cus.getCustomerID())) {
            if (item.getIsCart() != 0) {
                cartquantity.put(item.getProductID(), item);
            }
        }

        int ordertotal = 0;

        for (Map.Entry<Products, WishAndCart> key : cartquantity.entrySet()) {
            Products products = key.getKey();
            WishAndCart wishAndCart = key.getValue();

            ordertotal += (products.getPrice() * (100 - products.getDiscountProduct()) / 100) * wishAndCart.getIsCart();
        }

        //Orders orders = new Orders(orderid,cart.totalCart(),Shipname,ShipPhone,ShipAddress,date,payMethod,processStt);
        Orders orders = new Orders();
        orders.setOrderID(orderid);
        orders.setCustomerID(cus);
        orders.setShipName(Shipname);
        orders.setShipPhone(ShipPhone);
        orders.setShipAddress(ShipAddress);
        orders.setShipDate(date);
        orders.setShipNote(Shipnote);
        orders.setPaymentMethod(payMethod);
        orders.setOrderDate(date);
        orders.setProcessStatus(processStt);
        orders.setTotal(ordertotal);
        orders.setIsStatus(true);
        ordersFacade.create(orders);

        for (Map.Entry<Products, WishAndCart> key : cartquantity.entrySet()) {
            Products products = key.getKey();
            WishAndCart wishAndCart = key.getValue();

            int numOdd = ordersDetailsFacade.count() + 1;
            int orderDetailid = numOdd + 1;
            OrdersDetails ordersDetails = new OrdersDetails();
            ordersDetails.setOdID(orderDetailid);
            ordersDetails.setOrderID(orders);
            ordersDetails.setProductID(products);
            ordersDetails.setQuantity(wishAndCart.getIsCart());
            ordersDetails.setSellingPrice(products.getPrice() * (100 - products.getDiscountProduct()) / 100);
            ordersDetails.setIsStatus(true);
            ordersDetailsFacade.create(ordersDetails);

            int thisqty = products.getQuantity();
            products.setQuantity(thisqty - wishAndCart.getIsCart());
            prosFacade.edit(products);
        }

        for (WishAndCart item : wishAndCartFacade.findbyCustomer(cus.getCustomerID())) {
            if (item.getIsCart() != 0) {
                wishAndCartFacade.remove(item);
            }
        }

        List<Products> wishlist = new ArrayList<>();
        List<Products> cartlist = new ArrayList<>();

        for (WishAndCart item : wishAndCartFacade.findbyCustomer(cus.getCustomerID())) {
            if (item.getIsCart() == 0) {
                wishlist.add(item.getProductID());
            } else {
                cartlist.add(item.getProductID());
            }
        }

        session.setAttribute("wishlist", wishlist);
        session.setAttribute("cart", cartlist);
        session.setAttribute("countWishlist", wishlist.size());
        session.setAttribute("countCart", cartlist.size());

        request.getRequestDispatcher("orderComplete.jsp").forward(request, response);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
