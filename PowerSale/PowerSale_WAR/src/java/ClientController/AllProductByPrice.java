package ClientController;

import bean.ProductsFacadeLocal;
import entity.Products;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AllProductByPrice extends HttpServlet {

    @EJB
    ProductsFacadeLocal proFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        List<Products> proList = new ArrayList<>();

        Float min = 0.0f;
        Float max = 0.0f;

        String minstr = request.getParameter("minprice");
        String maxstr = request.getParameter("maxprice");

        if (minstr.isEmpty() || minstr.equals("")) {
            min = 0.0f;
        } else {
            min = Float.parseFloat(minstr);
        }

        if (maxstr.isEmpty() || maxstr.equals("")) {
            max = 0.0f;
        } else {
            max = Float.parseFloat(maxstr);
        }

        if (min == 0.0f && max == 0.0f) {
            request.getRequestDispatcher("AllProduct").forward(request, response);
        } else if (min != 0.0f && max == 0.0f) {
            proList = proFacade.AllProductByPrice(min, Float.MAX_VALUE);
        } else {
            proList = proFacade.AllProductByPrice(min, max);
        }

        Collections.reverse(proList);
        session.setAttribute("productList", proList);
        session.setAttribute("productListCount", proList.size());
        request.getRequestDispatcher("product.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
