package ClientController;

import bean.BrandsFacadeLocal;
import bean.CategoriesFacadeLocal;
import bean.ProductsFacadeLocal;
import entity.Brands;
import entity.Categories;
import entity.Products;
import entity.TopRatingThisYear;
import entity.TopSellingThisYear;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class initPage extends HttpServlet {

    @EJB
    ProductsFacadeLocal proFacade;
    @EJB
    CategoriesFacadeLocal cateFacade;
    @EJB
    BrandsFacadeLocal brandFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        HttpSession session = request.getSession();
        
        List<Categories> categoriesList = cateFacade.AllCategories();
        List<Brands> brandsList = brandFacade.AllBrands();
        List<Products> newProductList = proFacade.AllProductNew();
        List<TopRatingThisYear> topRatingList = proFacade.TopRatingThisYear();
        List<TopSellingThisYear> topSellingList = proFacade.TopSellingThisYear();
        Collections.reverse(newProductList);
        
        session.setAttribute("categoriesList", categoriesList);
        session.setAttribute("brandsList", brandsList);
        session.setAttribute("newProductList", newProductList);
        session.setAttribute("topRatingList", topRatingList);
        session.setAttribute("topSellingList", topSellingList);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
