package ClientController;

import bean.CustomersFacadeLocal;
import bean.ProductsFacadeLocal;
import bean.WishAndCartFacadeLocal;
import entity.Products;
import entity.WishAndCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RemoveProductoutWislist extends HttpServlet {

    @EJB
    ProductsFacadeLocal productsFacade;
    @EJB
    WishAndCartFacadeLocal wishAndCartFacade;
    @EJB
    CustomersFacadeLocal customersFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String productId = request.getParameter("productId");
        String customerId = request.getParameter("cusId");
        try {
            WishAndCart wishandcart = wishAndCartFacade.findbyProduct(productId, customerId).get(0);
            wishAndCartFacade.remove(wishandcart);
            HttpSession session = request.getSession();

            List<Products> wishlist = new ArrayList<>();
            List<Products> cartlist = new ArrayList<>();

            for (WishAndCart item : wishAndCartFacade.findbyCustomer(customerId)) {
                if (item.getIsCart() == 0) {
                    wishlist.add(item.getProductID());
                } else {
                    cartlist.add(item.getProductID());
                }
            }
            session.setAttribute("wishlist", wishlist);
            session.setAttribute("cart", cartlist);
            session.setAttribute("countWishlist", wishlist.size());
            session.setAttribute("countCart", cartlist.size());
        } catch (Exception e) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
