package ClientController;

import bean.CustomersFacadeLocal;
import bean.WishAndCartFacadeLocal;
import entity.Products;
import entity.WishAndCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginCheckOutServlet extends HttpServlet {

    @EJB
    CustomersFacadeLocal cusFacade;
    @EJB
    WishAndCartFacadeLocal wishAndCartFacade;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if (cusFacade.login(email, password).size() > 0) {
            if (cusFacade.login(email, password).get(0).getIsStatus() == true) {
                session.setAttribute("login_account", cusFacade.login(email, password).get(0));
                session.setAttribute("login_message", null);
                List<Products> wishlist = new ArrayList<>();
                List<Products> cartlist = new ArrayList<>();

                for (WishAndCart item : wishAndCartFacade.findbyCustomer(cusFacade.login(email, password).get(0).getCustomerID())) {
                    if (item.getIsCart() == 0) {
                        wishlist.add(item.getProductID());
                    } else {
                        cartlist.add(item.getProductID());
                    }
                }
                session.setAttribute("wishlist", wishlist);
                session.setAttribute("cart", cartlist);
                session.setAttribute("countWishlist", wishlist.size());
                session.setAttribute("countCart", cartlist.size());
                request.getRequestDispatcher("checkout.jsp").forward(request, response);
            } else {
                session.setAttribute("login_message", "<p class=\"login-box-msg\" style=\"color:red\">your account is banned</p>");
                request.getRequestDispatcher("loginCheckout.jsp").forward(request, response);
            }
        } else {
            session.setAttribute("login_message", "<p class=\"login-box-msg\" style=\"color:red\">email or password incorrect</p>");
            request.getRequestDispatcher("loginCheckout.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
