<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Registration - PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Registration</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- Start register area -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-3">
                        </div>

                        <div class="col-md-6">
                            <form id="register-form" action="Register" class="login login-form" method="post">
                                <div class="login-content">
                                    <div class="login-title">
                                        <h3 class="title">Register an Account</h3>
                                    </div>
                                    <p class="form-row form-row-first">
                                        <label for="first-name">First Name <span class="required">*</span></label>
                                        <input type="text" id="first-name" name="first-name" placeholder="Enter your First Name" class="input-text">
                                    </p>

                                    <p class="form-row form-row-first">
                                        <label for="last-name">Last Name <span class="required">*</span></label>
                                        <input type="text" id="last-name" name="last-name" placeholder="Enter your Last Name" class="input-text">
                                    </p>

                                    <p class="form-row form-row-first">
                                        <label for="email">Email <span class="required">*</span></label>
                                        <span id="email-result"></span>
                                        <input type="email" id="email" name="email" placeholder="Enter your Email" class="input-text">
                                    </p>
                                    <p class="form-row form-row-last">
                                        <label for="password">Password <span class="required">*</span></label>
                                        <input type="password" id="password" name="password" placeholder="Enter your Password" class="input-text">
                                    </p>

                                    <p class="form-row form-row-first">
                                        <label for="confirmpass">Confirm Password <span class="required">*</span></label>
                                        <input type="password" id="confirmpass" name="confirmpass" placeholder="Confirm Password" class="input-text">
                                    </p>

                                    <p class="form-row form-row-first">
                                        <label for="address">Address <span class="required">*</span></label>
                                        <input type="text" id="address" name="address" placeholder="Enter your Address" class="input-text">
                                    </p>

                                    <p class="form-row form-row-first">
                                        <label for="tel">Phone <span class="required">*</span></label>
                                        <input type="text" id="tel" name="tel" placeholder="Enter your Phone" class="input-text" data-inputmask="'mask': '(999) 999-9999'">
                                    </p>

                                    <p class="form-row form-row-first">
                                        <label for="gender">Gender <span class="required">*</span></label>
                                        <select id="gender" name="gender" class="input-cbo">
                                            <option value="--">Choose a gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </p>

                                    <div class="clear"></div>

                                    <p class="form-row">
                                        <input type="submit" value="Login" name="login" class="button">
                                    </p>
                                </div>

                                <br/>
                                <div class="form-group" style="float: right">
                                    <span>Already have an account?</span>
                                    <a href="login.jsp"><span style="color: blue">Login</span></a>
                                </div>

                                <div class="clear"></div>
                            </form>
                        </div>

                        <div class="col-md-3">
                        </div>

                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- End register area -->

            <!-- Footer -->
        <jsp:include page="client-footer.jsp"></jsp:include>

<!--        <script type="text/javascript">
            $(":input").inputmask();
            $('#phone').inputmask({'mask': '(999) 999-9999'});
        </script>-->
        
        <script type="text/javascript">
            $().ready(function() {
                $("#register-form").validate({
                    onfocusout: false,
                    onkeyup: false,
                    onclick: false,
                    rules: {
                        "first-name": {
                            required: true,
                            maxlength: 50
                        },
                        "last-name": {
                            required: true,
                            maxlength: 50
                        },
                        "email": {
                            required: true,
                            email: true,
                            maxlength: 100
                        },
                        "password": {
                            required: true,
                            minlength: 6,
                            maxlength: 30
                        },
                        "confirmpass": {
                            required: true,
                            equalTo: "#password",
                            minlength: 6
                        },
                        "address": {
                            required: true,
                            validateAddress: true
                        },
                        "tel": {
                            required: true,
                            validatePhone: true
                        },
                        "gender": {
                            required: true
                        }
                    },
                    messages: {
                        "first-name": {
                            required: "Please enter a first name",
                            maxlength: "Your first name must be maximum 50 characters"
                        },
                        "last-name": {
                            required: "Please enter a last name",
                            maxlength: "Your last name must be maximum 50 characters"
                        },
                        "email": {
                            required: "Please enter a valid email address",
                            email: "Please enter a valid email address",
                            maxlength: "Your email must be maximum 100 characters"
                        },
                        "password": {
                            required: "Please provide a password",
                            minlength: "Your password must consist of at least 6 characters",
                            maxlength: "Your password must be maximum 30 characters"
                        },
                        "confirmpass": {
                            required: "Please provide a password",
                            equalTo: "Please enter the same password as above",
                            minlength: "Your password must consist of at least 6 characters"
                        },
                        "address": {
                            required: "Please enter a your address"
                        },
                        "tel": {
                            required: "Please enter a phone number"
                        },
                        "gender": {
                            required: "Please choose a gender"
                        }
                    }
                });
                $.validator.addMethod("validatePhone", function(value, element) {
                    return this.optional(element) || /^[(]{1}[0]{1}[0-9\-\s\)\+]{12}$/i.test(value);
                },"Please enter a valid phone number");
                $.validator.addMethod("validateAddress", function(value, element) {
                    return this.optional(element) || /^\d+[ |/](?:[/A-Za-z0-9-]+[ ]?)+(?:,)+(?:[ A-Za-z0-9-]+[ ]?)+(?:,)+(?:[A-Za-z0-9 -]+[ ]?)?(?:,)?(?:[A-Za-z -]+[ ]?)$/i.test(value);
                },"Please enter a valid address (ex: 50 Vo Van Kiet, district 1, Ho Chi Minh city)");
            });
            $(function() {
                $("[data-mask]").inputmask();
            });
            
            /*************** check Email unique **********/
            $(document).ready(function() {
                var x_timer;
                $("#email").keyup(function(e) {
                    clearTimeout(x_timer);
                    var email = $(this).val();
                    x_timer = setTimeout(function() {
                        check_email_ajax(email);
                    }, 1000);
                });

                function check_email_ajax(email) {
                    $("#email-result").html('<label id="email-resultError" class="control-label pull-right" value="false" style="color: orange"><i class="fa fa-bell-o"></i> Waiting ...</label>');
                    $.post('adminCheckAddEmailCustomer', {'email': email}, function(data) {
                        $("#email-result").html(data);
                    });
                }
            });
            /*************** focus Email error  **********/
            $('#register-form').submit(function(event) {
                var errors = $('#email-resultError').attr('value');
                if (errors === 'false' || errors === null) {
                    $('#email').focus();
                    event.preventDefault();
                }
            });
        </script>
    </body>
</html>
