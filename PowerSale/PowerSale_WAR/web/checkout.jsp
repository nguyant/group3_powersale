<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri = "http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Check Out - PowerSale</title>
    </head>
    <body>
        <!--Header-->
        <jsp:include page="client-header.jsp"></jsp:include>
        <jsp:include page="initCheckoutPage"/>
        <!-- Start big title area -->
        <div class="product-big-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-bit-title text-center">
                            <h2>Check Out</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End big title area -->

        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-content-right">
                            <div class="woocommerce">
                                <form enctype="multipart/form-data" action="PlaceOrderServlet" class="checkout" method="post" name="checkout">
                                    <div class="row">
                                        <div class="col-md-2"></div>

                                        <div id="customer_details" class="col-md-8">
                                            <div class="woocommerce-billing-fields">
                                                <h3>Orders Details</h3>

                                                <p id="billing_first_name_field" class="form-row form-row-first validate-required">
                                                    <label class="" for="first-name">First Name <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" value="${sessionScope.login_account.firstName}" placeholder="Enter First Name" id="first-name" name="first-name" class="input-text ">
                                                </p>

                                                <p id="billing_last_name_field" class="form-row form-row-last validate-required">
                                                    <label class="" for="last-name">Last Name <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" value="${sessionScope.login_account.lastName}" placeholder="Enter Last Name" id="last-name" name="last-name" class="input-text ">
                                                </p>

                                                <p id="billing_email_field" class="form-row form-row-first validate-required validate-email">
                                                    <label class="" for="email">Email Address <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" value="${sessionScope.login_account.email}" placeholder="Enter Email" id="email" name="email" class="input-text ">
                                                </p>

                                                <p id="billing_address_1_field" class="form-row form-row-wide address-field validate-required">
                                                    <label class="" for="address">Address <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" value="${sessionScope.login_account.address}" placeholder="Enter Address" id="address" name="address" class="input-text ">
                                                </p>

                                                <p id="billing_phone_field" class="form-row form-row-last validate-required validate-phone">
                                                    <label class="" for="tel">Phone <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" value="${sessionScope.login_account.phone}" placeholder="Enter Phone" id="tel" name="tel" class="input-text ">
                                                </p>

                                                <p id="billing_phone_field" class="form-row form-row-last validate-required validate-phone">
                                                    <label class="" for="shipnote">Ship Note <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <textarea class="input-text" id="shipnote" name="shipnote" placeholder="Order Notes"></textarea>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-md-2"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2"></div>

                                        <div id="order_review" class="col-md-8">
                                            <h3 id="order_review_heading">Your order</h3>

                                            <table cellspacing="0" class="shop_table cart">
                                                <thead>
                                                    <tr>
                                                        <th class="product-thumbnail">&nbsp;</th>
                                                        <th class="product-name">Product</th>
                                                        <th class="product-price">Price</th>
                                                        <th class="product-quantity">Quantity</th>
                                                        <th class="product-subtotal">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${cartquantity}" var="p">
                                                        <tr class="cart_item">
                                                            <td class="product-thumbnail">
                                                                <a href="ProductDetail?proid=${p.key.productID}"><img width="145" height="145" class="shop_thumbnail" src="${p.key.image1}"></a>
                                                            </td>

                                                            <td class="product-name">
                                                                <a href="ProductDetail?proid=${p.key.productID}">${p.key.productName}</a>
                                                            </td>

                                                            <td class="product-price">
                                                                <span class="amount">
                                                                    <ins>$<fmt:formatNumber type="number" maxFractionDigits="0" value="${p.key.price*(100-p.key.discountProduct)/100}"/></ins>
                                                                    <c:if test="${p.key.discountProduct ne 0}">
                                                                        <del class="product-old-price">$${p.key.price}</del>
                                                                    </c:if>
                                                                </span>
                                                            </td>

                                                            <td class="product-quantity">
                                                                <div class="quantity buttons_added">
                                                                    ${p.value.isCart}
                                                                </div>
                                                            </td>

                                                            <td class="product-subtotal">
                                                                <span class="amount" id="prototal${p.key.productID}}">$${(p.key.price*(100-p.key.discountProduct)/100) * p.value.isCart}</span>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                    <tr>
                                                        <td colspan="2">Cart Total</td>
                                                        <td id="ort" colspan="3">$${ordertotal}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Shipping Plan</td>
                                                        <td colspan="3">Free Shipping</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Order Total</td>
                                                        <td id="ort" colspan="3">$${ordertotal}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="actions" colspan="5">
                                                            <input type="button" onclick="placeorder()" data-value="Place order" value="Place order" id="place_order" name="woocommerce_checkout_place_order" class="button alt">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-2"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Footer-->
        <jsp:include page="client-footer.jsp"/>

        <script type="text/javascript">
            $().ready(function() {
                $("#PlaceOrderForm").validate({
                    onfocusout: false,
                    onkeyup: false,
                    onclick: false,
                    rules: {
                        "first-name": {
                            required: true,
                            maxlength: 50
                        },
                        "last-name": {
                            required: true,
                            maxlength: 50
                        },
                        "email": {
                            required: true,
                            email: true,
                            maxlength: 100
                        },
                        "address": {
                            required: true,
                            validateAddress: true
                        },
                        "tel": {
                            required: true,
                            validatePhone: true
                        }
                    },
                    messages: {
                        "first-name": {
                            required: "Please enter a first name",
                            maxlength: "Your first name must be maximum 50 characters"
                        },
                        "last-name": {
                            required: "Please enter a last name",
                            maxlength: "Your last name must be maximum 50 characters"
                        },
                        "email": {
                            required: "Please enter a valid email address",
                            email: "Please enter a valid email address",
                            maxlength: "Your email must be maximum 100 characters"
                        },
                        "address": {
                            required: "Please enter a your address"
                        },
                        "tel": {
                            required: "Please enter a phone number"
                        }
                    }
                });
                $.validator.addMethod("validatePhone", function(value, element) {
                    return this.optional(element) || /^[(]{1}[0]{1}[0-9\-\s\)\+]{12}$/i.test(value);
                }, "Please enter a valid phone number");
                $.validator.addMethod("validateAddress", function(value, element) {
                    return this.optional(element) || /^\d+[ |/](?:[/A-Za-z0-9-]+[ ]?)+(?:,)+(?:[ A-Za-z0-9-]+[ ]?)+(?:,)+(?:[A-Za-z0-9 -]+[ ]?)?(?:,)?(?:[A-Za-z -]+[ ]?)$/i.test(value);
                }, "Please enter a valid address (ex: 50 Vo Van Kiet, district 1, Ho Chi Minh city)");
            });
            $(function() {
                $("[data-mask]").inputmask();
            });

            function placeorder() {
                var firstname = $("#first-name").val();
                var lastname = $("#last-name").val();
                var tel = $("#tel").val();
                var address = $("#address").val();
                var shipnote = $("#shipnote").val();
                var processstt = "Processing";
                var payment = "Cash";

                $.ajax({
                    url: "PlaceOrderServlet?first-name=" + firstname + "&last-name=" + lastname + "&tel=" + tel + "&address=" + address + "&shipnote=" + shipnote + "&processstt=" + processstt + "&payment=" + payment,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {
                        location.href = 'orderComplete.jsp';
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });
            }
        </script>

    </body>
</html>
