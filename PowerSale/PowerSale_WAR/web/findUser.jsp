<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Reset Password - PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Reset Password</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-3">                        
                        </div>

                        <div class="col-md-5 order-details">      
                            <form id="retrieve-form" action="ForgetPasswordServlet" class="login login-form" method="post">
                                <div class="login-content">
                                    <div class="login-title">
                                        <h3 class="title">Retrieve Your Password</h3>
                                    </div>
                                    <p class="form-row form-row-last">
                                        <label for="password">Email Address <span class="required">*</span></label>
                                        <input type="email" id="email" name="email" placeholder="Enter your Email" class="input-text" required="true">
                                    </p>

                                    <div class="clear"></div>

                                    <p class="form-row">
                                        <input type="submit" value="Submit" name="login" class="button">
                                    </p>
                                </div>

                                <div class="clear"></div>
                            </form>
                        </div>

                        <div class="col-md-4">                        
                        </div>                

                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /SECTION -->

            <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>
    </body>
</html>
