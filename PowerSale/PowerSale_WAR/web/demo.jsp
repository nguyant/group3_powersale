<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- có thể có hoặc không, kiểm tra vói các jsp cùng tên của project dntShop -->
<!DOCTYPE html>
<html>
    <head>
        <!-- phần phải có -->
        <jsp:include page="client-layout.jsp"/>
        <!-- hết phần phải có-->
        <title> --Title-- </title><!--thay đổi phù hợp từng file-->
    </head>
    <body>
        <!-- Header: phải có -->
        <jsp:include  page="client-header.jsp"></jsp:include>
        <!-- End Header: phải có -->
        
        <!-- content: kiểm tra các tên biến, kiểm tra vói các jsp cùng tên của project dntShop, ngoại trừ tên class -->
        <!-- breadcumb của dntShop sẽ là big title area của mình -->
        
        <!-- Footer: phải có -->
        <jsp:include page="client-footer.jsp"/>
        <!-- End Footer: phải có -->
        
        <!-- có thể có hoặc không, nếu có thì kiểm tra tên biến -->
        <script type="text/javascript"></script>
    </body>
</html>
