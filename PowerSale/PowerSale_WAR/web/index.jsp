<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start slider area -->
            <div class="slider-area">
                <!-- Slider: top rating -->
                <div class="block-slider block-slider4">
                    <ul class="" id="bxslider-home4">
                    <c:forEach var="c" items="${sessionScope.categoriesList}">
                        <li>
                            <img src="${c.categoryImage}">
                            <div class="caption-group">
                                <h2 class="caption title" style="text-shadow: 0px 0px 4px #FFFFFF">${c.categoryName}</h2>
                                <!--<h4 class="caption subtitle">${c.categoryName}</h4>-->
                                <a class="caption button-radius" href="AllProductByCategory?cateid=${c.categoryID}"><span class="icon"></span> Shop now</a>
                            </div>
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <!-- ./Slider -->
        </div>
        <!-- End slider area -->

        <!-- Start main content area -->
        <div class="maincontent-area">
            <div class="zigzag-bottom"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="latest-product">
                            <h2 class="section-title">Latest Laptops</h2>
                            <div class="product-carousel">
                                <c:forEach var="n" items="${sessionScope.newProductList}">
                                    <div class="single-product">
                                        <div class="product-f-image">
                                            <img src="${n.image1}" alt="">
                                            <div class="product-hover">

                                                <c:choose>
                                                    <c:when test="${sessionScope.login_account eq null}">
                                                        <a href="login.jsp" class="add-to-cart-link"><i class="fas fa-cart-plus"></i> Add to cart</a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <a href="#" class="add-to-cart-link" onclick='addProductToCart("${n.productID}", "${sessionScope.login_account.customerID}")'><i class="fas fa-cart-plus"></i> Add to cart</a>
                                                    </c:otherwise>
                                                </c:choose>

                                                <c:choose>
                                                    <c:when test="${sessionScope.login_account eq null}">
                                                        <a href="login.jsp" class="add-to-wish-link"><i class="far fa-heart"></i> Add to Wish</a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:choose>
                                                            <c:when test="${sessionScope.wishlist.contains(n) eq true}">
                                                                <a href="#" class="add-to-wish-link" onclick='removeProductWishlist("${n.productID}", "${sessionScope.login_account.customerID}")'><i class="fas fa-heart"></i> Remove Wish</a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <a href="#" class="add-to-wish-link" onclick='addProductWishlist("${n.productID}", "${sessionScope.login_account.customerID}")'><i class="far fa-heart"></i> Add to Wish</a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:otherwise>
                                                </c:choose>

                                                <a href="ProductDetail?proid=${n.productID}" class="view-details-link"><i class="fa fa-link"></i> See details</a>

                                            </div>
                                        </div>

                                        <h2><a href="ProductDetail?proid=${n.productID}">${n.productName}</a></h2>

                                        <div class="product-carousel-price">
                                            <ins>$<fmt:formatNumber type="number" maxFractionDigits="0" value="${n.price*(100-n.discountProduct)/100}"/></ins>
                                            <c:if test="${n.discountProduct ne 0}">
                                                <del class="product-old-price">$${n.price}</del>
                                            </c:if>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End main content area -->

        <!-- Start product widget area -->
        <div class="product-widget-area">
            <div class="zigzag-bottom"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="single-product-widget">
                            <h2 class="product-wid-title">Top Laptop Sale</h2>
                            <a href="" class="wid-view-more">View All</a>
                            <c:forEach var="n" items="${sessionScope.topSellingList}">
                                <div class="single-wid-product">
                                    <a href="ProductDetail?proid=${n.productID}"><img src="${n.image1}" alt="" class="product-thumb"></a>
                                    <h2><a href="ProductDetail?proid=${n.productID}">${n.productName}</a></h2>
                                    <div class="product-wid-rating">
                                        <c:choose>
                                            <c:when test="${empty avgStars}">
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach begin="1" end="${avgStars}">
                                                    <i class="fas fa-star"></i>
                                                </c:forEach>
                                                <c:forEach begin="1" end="${5-avgStars}">
                                                    <i class="far fa-star"></i>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div class="product-wid-price">
                                        <ins>$<fmt:formatNumber type="number" maxFractionDigits="0" value="${n.price*(100-n.discountProduct)/100}"/></ins>
                                        <c:if test="${n.discountProduct ne 0}">
                                            <del class="product-old-price">$${n.price}</del>
                                        </c:if>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-product-widget">
                            <h2 class="product-wid-title">Top Rate</h2>
                            <a href="#" class="wid-view-more">View All</a>
                            <c:forEach var="n" items="${sessionScope.topRatingList}">
                                <div class="single-wid-product">
                                    <a href="ProductDetail?proid=${n.productID}"><img src="${n.image1}" alt="" class="product-thumb"></a>
                                    <h2><a href="ProductDetail?proid=${n.productID}">${n.productName}</a></h2>
                                    <div class="product-wid-rating">
                                        <c:if test="${n.averageRating < 1 || empty n.averageRating}">
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </c:if>
                                        <c:if test="${n.averageRating >= 1 && n.averageRating < 2}">
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </c:if>
                                        <c:if test="${n.averageRating >= 2 && n.averageRating < 3}">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </c:if>
                                        <c:if test="${n.averageRating >= 3 && n.averageRating < 4}">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </c:if>
                                        <c:if test="${n.averageRating >= 4 && n.averageRating < 5}">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </c:if>
                                        <c:if test="${n.averageRating == 5}">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </c:if>
                                    </div>
                                    <div class="product-wid-price">
                                        <ins>$<fmt:formatNumber type="number" maxFractionDigits="0" value="${n.price*(100-n.discountProduct)/100}"/></ins>
                                        <c:if test="${n.discountProduct ne 0}">
                                            <del class="product-old-price">$${n.price}</del>
                                        </c:if>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End product widget area -->

        <!-- Start brands area -->
        <div class="brands-area">
            <h2 class="section-title">All Brands</h2>
            <div class="zigzag-bottom"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="brand-wrapper">
                            <div class="brand-list">
                                <c:forEach var="n" items="${sessionScope.brandsList}">
                                    <img href="AllProductByBrand?brandid=${n.brandID}" src="${n.brandImages}" alt="">
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End brands area -->

        <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>

        <script type="text/javascript">
            function addProductToCart(productid, customerId)
            {
                $.ajax({
                    url: "AddProductToCart?command=plus&productID=" + productid + "&cusID=" + customerId,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {
                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });
            }
            function addProductWishlist(productid, customerId)
            {

                $.ajax({
                    url: "AddProductToWishlist?productId=" + productid + "&cusId=" + customerId,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {
                        location.reload();
                        return false;
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });

            }
            function removeProductWishlist(productid, customerId)
            {
                $.ajax({
                    url: "RemoveProductoutWislist?productId=" + productid + "&cusId=" + customerId,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {
                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });
            }
        </script>
    </body>
</html>
