<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri = "http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Product Details - PowerSale</title>
    </head>
    <body>
        <!--Header-->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Product Details</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <div class="product-breadcroumb">
                        <a href="">Home</a>
                        <a href="AllProductByCategory?cateid=${catepro.categoryID}">${catepro.categoryName}</a>
                    <a href="ProductDetail?proid=${pro.productID}">${pro.productName}</a>
                </div>

                <!-- row -->
                <div class="row">
                    <!-- Product main img -->
                    <div class="col-md-1"></div>
                    <div class="product-img-container col-md-4">
                        <div class="main-img-slides">
                            <c:if test="${not empty pro.image1}">
                                <div class="numbertext-img">1 / 4</div>
                                <img src="${pro.image1}" class="main-img">
                            </c:if>
                        </div>

                        <div class="main-img-slides">
                            <c:if test="${not empty pro.image2}">
                                <div class="numbertext-img">2 / 4</div>
                                <img src="${pro.image2}" class="main-img">
                            </c:if>
                        </div>

                        <div class="main-img-slides">
                            <c:if test="${not empty pro.image3}">
                                <div class="numbertext-img">3 / 4</div>
                                <img src="${pro.image3}" class="main-img">
                            </c:if>
                        </div>

                        <div class="main-img-slides">
                            <c:if test="${not empty pro.image4}">
                                <div class="numbertext-img">4 / 4</div>
                                <img src="${pro.image4}" class="main-img">
                            </c:if>
                        </div>
                        <!-- /Product main img -->

                        <a class="slides-prev" onclick="plusSlides(-1)"><i class="fas fa-angle-left"></i></a>
                        <a class="slides-next" onclick="plusSlides(1)"><i class="fas fa-angle-right"></i></a>

                        <div class="caption-container">
                            <p id="caption-img"></p>
                        </div>

                        <!-- Product thumb img -->
                        <div class="row">
                            <c:if test="${not empty pro.image1}">
                                <div class="col-sm-3">
                                    <img class="thumb-img cursor" src="${pro.image1}" onclick="currentSlide(1)">
                                </div>
                            </c:if>

                            <c:if test="${not empty pro.image2}">
                                <div class="col-sm-3">
                                    <img class="thumb-img cursor" src="${pro.image2}" onclick="currentSlide(2)">
                                </div>
                            </c:if>

                            <c:if test="${not empty pro.image3}">
                                <div class="col-sm-3">
                                    <img class="thumb-img cursor" src="${pro.image3}" onclick="currentSlide(3)">
                                </div>
                            </c:if>

                            <c:if test="${not empty pro.image4}">
                                <div class="col-sm-3">
                                    <img class="thumb-img cursor" src="${pro.image4}" onclick="currentSlide(4)">
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <!-- /Product thumb img -->

                    <!-- Product details -->
                    <div class="col-md-6">
                        <div class="product-details">
                            <h2 class="product-name">${pro.productName}</h2>
                            <div class="product-inner-price">
                                <ins>$<fmt:formatNumber type="number" maxFractionDigits="0" value="${pro.price*(100-pro.discountProduct)/100}"/> </ins>
                                <c:if test="${pro.discountProduct ne 0}">
                                    <del class="product-old-price">$${pro.price}</del>
                                </c:if>
                            </div>
                            <div class="product-wid-rating">
                                <c:choose>
                                    <c:when test="${empty avgStars}">
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach begin="1" end="${avgStars}">
                                            <i class="fas fa-star"></i>
                                        </c:forEach>
                                        <c:forEach begin="1" end="${5-avgStars}">
                                            <i class="far fa-star"></i>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </div>

                            <form action="" class="cart">
                                <div class="row">
                                    <div class="quantity col-sm-6">
                                        <input id="pro-quantity" type="number" size="4" class="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1">
                                    </div>
                                    <c:choose>
                                        <c:when test="${sessionScope.login_account eq null}">
                                            <button class="add_to_cart_button col-sm-6" type="button" onclick="location.href = 'login.jsp'"><i class="fa fa-shopping-cart"></i> Add To Cart</button>
                                        </c:when>
                                        <c:otherwise>
                                            <button class="add_to_cart_button col-sm-6" type="button" ><i class="fa fa-shopping-cart"></i> Add To Cart</button>
                                        </c:otherwise>
                                    </c:choose>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <c:choose>
                                            <c:when test="${sessionScope.login_account eq null}">
                                                <button class="add_to_cart_button" type="button" onclick="location.href = 'login.jsp'"><i class="far fa-heart"></i> Add To Wish</button>
                                            </c:when>
                                            <c:otherwise>
                                                <c:choose>
                                                    <c:when test="${sessionScope.wishlist.contains(pro) eq true}">
                                                        <button class="add_to_cart_button" type="button" onclick='removeProductWishlist("${pro.productID}", "${sessionScope.login_account.customerID}")'><i class="fas fa-heart"></i> Remove Wish</button>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <button class="add_to_cart_button" type="button" onclick='addProductWishlist("${pro.productID}", "${sessionScope.login_account.customerID}")'><i class="far fa-heart"></i> Add To Wish</button>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </form>

                            <div class="product-inner-category">
                                <p>Category: <a href="AllProductByBrand?brandid=${pro.brandID.brandID}">
                                        <c:choose>
                                            <c:when test="${not empty pro.brandID.brandImages}">
                                                <img class="img-circle" src="${pro.brandID.brandImages}" width="30px" height="30px">
                                            </c:when>
                                            <c:otherwise>
                                                ${pro.brandID.brandName}
                                            </c:otherwise>
                                        </c:choose>
                                    </a>
                                </p>
                            </div> 
                            <p>${pro.descriptions}</p>
                        </div>
                    </div>
                    <!-- /Product details -->

                    <!-- Product tab -->
                    <div class="row">
                        <div class="col-md-12">
                            <div role="tabpanel">
                                <ul class="product-tab" role="tablist">
                                    <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Description</a></li>
                                    <li role="presentation"><a href="#ratings" aria-controls="ratings" role="tab" data-toggle="tab">Ratings (${ratingListCount})</a></li>
                                    <li role="presentation"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Comments ()</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- Start tab details -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="details">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>${pro.descriptions}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <table cellspacing="0" class="shop_table cart">
                                                    <tbody>
                                                        <c:if test="${not empty prodetail.cpu}">
                                                            <tr class="cart_item">
                                                                <th scope="row"><span>CPU</span></th>
                                                                <td><h6>${prodetail.cpu}</h6></td>
                                                            </tr>
                                                        </c:if>
                                                        <c:if test="${not empty prodetail.memory}">
                                                            <tr class="cart_item">
                                                                <th scope="row"><span>Memory</span></th>
                                                                <td><h6>${prodetail.memory}</h6></td>
                                                            </tr>
                                                        </c:if>
                                                        <c:if test="${not empty prodetail.vga}">
                                                            <tr class="cart_item">
                                                                <th scope="row"><span>VGA</span></th>
                                                                <td><h6>${prodetail.vga}</h6></td>
                                                            </tr>
                                                        </c:if>
                                                        <c:if test="${not empty prodetail.hdd}">
                                                            <tr class="cart_item">
                                                                <th scope="row"><span>HDD</span></th>
                                                                <td><h6>${prodetail.hdd}</h6></td>
                                                            </tr>
                                                        </c:if>
                                                        <c:if test="${not empty prodetail.camera}">
                                                            <tr class="cart_item">
                                                                <th scope="row"><span>Camera</span></th>
                                                                <td><h6>${prodetail.camera}</h6></td>
                                                            </tr>
                                                        </c:if>
                                                        <c:if test="${not empty prodetail.display}">
                                                            <tr class="cart_item">
                                                                <th scope="row"><span>Display</span></th>
                                                                <td><h6>${prodetail.display}</h6></td>
                                                            </tr>
                                                        </c:if>
                                                        <c:if test="${not empty prodetail.battery}">
                                                            <tr class="cart_item">
                                                                <th scope="row"><span>Battery</span></th>
                                                                <td><h6>${prodetail.battery}</h6></td>
                                                            </tr>
                                                        </c:if>
                                                        <c:if test="${not empty prodetail.weights}">
                                                            <tr class="cart_item">
                                                                <th scope="row"><span>Weight</span></th>
                                                                <td><h6>${prodetail.weights}</h6></td>
                                                            </tr>
                                                        </c:if>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End tab details -->

                                    <!-- Start tab ratings-->
                                    <div role="tabpanel" class="tab-pane fade" id="ratings">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h2>Average Ratings</h2>
                                                <div class="rating-avg">
                                                    <span>
                                                        <c:choose>
                                                            <c:when test="${empty avgRating}">
                                                                0.0
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:formatNumber type="number" minFractionDigits="1" maxFractionDigits="1" value="${avgRating}"/>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </span>
                                                    <div class="rating-stars">
                                                        <c:choose>
                                                            <c:when test="${empty avgStar}">
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:forEach begin="1" end="${avgStar}">
                                                                    <i class="fas fa-star"></i>
                                                                </c:forEach>
                                                                <c:forEach begin="1" end="${5-avgStar}">
                                                                    <i class="far fa-star"></i>
                                                                </c:forEach>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <h2>Your Ratings</h2>
                                                <div class="submit-review">
                                                    <form id="ReviewForm" class="review-form" method="post">
                                                        <div class="input-rating">
                                                            <span>Your Rating: </span>
                                                            <div class="stars">
                                                                <input id="star5" name="rating" value="5" type="radio"><label for="star5"></label>
                                                                <input id="star4" name="rating" value="4" type="radio"><label for="star4"></label>
                                                                <input id="star3" name="rating" value="3" type="radio"><label for="star3"></label>
                                                                <input id="star2" name="rating" value="2" type="radio"><label for="star2"></label>
                                                                <input id="star1" name="rating" value="1" type="radio"><label for="star1"></label>
                                                            </div>
                                                            <span id="rating-result"></span>
                                                        </div>
                                                        <p>
                                                            <c:if test="${sessionScope.login_account eq null}">
                                                                <input type="button" onclick="location.href = 'login.jsp'" value="Submit">
                                                            </c:if>
                                                            <c:if test="${sessionScope.login_account ne null}">
                                                                <input type="button" onclick="addNewRating()" value="Submit">
                                                            </c:if>
                                                        </p>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2>User's Ratings</h2>
                                                <ul class="rating">
                                                    <li>
                                                        <div class="rating-chooser">
                                                            <div class="rating-wrap-post">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                        </div>
                                                        <div class="rating-progress">
                                                            <div style="width: ${ratingListCountR5/ratingListCount*100}%;"></div>
                                                        </div>
                                                        <span class="sum">${ratingListCountR5}</span>
                                                    </li>
                                                    <li>
                                                        <div class="rating-chooser">
                                                            <div class="rating-wrap-post">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            </div>
                                                        </div>
                                                        <div class="rating-progress">
                                                            <div style="width: ${ratingListCountR4/ratingListCount*100}%;"></div>
                                                        </div>
                                                        <span class="sum">${ratingListCountR4}</span>
                                                    </li>
                                                    <li>
                                                        <div class="rating-chooser">
                                                            <div class="rating-wrap-post">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            </div>
                                                        </div>
                                                        <div class="rating-progress">
                                                            <div style="width: ${ratingListCountR3/ratingListCount*100}%;"></div>
                                                        </div>
                                                        <span class="sum">${ratingListCountR3}</span>
                                                    </li>
                                                    <li>
                                                        <div class="rating-chooser">
                                                            <div class="rating-wrap-post">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            </div>
                                                        </div>
                                                        <div class="rating-progress">
                                                            <div style="width: ${ratingListCountR2/ratingListCount*100}%;"></div>
                                                        </div>
                                                        <span class="sum">${ratingListCountR2}</span>
                                                    </li>
                                                    <li>
                                                        <div class="rating-chooser">
                                                            <div class="rating-wrap-post">
                                                                <i class="fas fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            </div>
                                                        </div>
                                                        <div class="rating-progress">
                                                            <div style="width: ${ratingListCountR1/ratingListCount*100}%;"></div>
                                                        </div>
                                                        <span class="sum">${ratingListCountR1}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="reviews">
                                                    <ul class="reviews">
                                                        <c:forEach var="r" items="${ratingList}">
                                                            <li class="contentPage">
                                                                <div class="review-heading">
                                                                    <h5 class="name">${r.customerID.firstName} ${r.customerID.lastName}</h5>
                                                                    <p class="date"><fmt:formatDate value="${r.ratingDate}" pattern="dd-MM-yyyy" /></p>
                                                                    <div class="review-rating">
                                                                        <c:forEach begin="1" end="${r.rate}">
                                                                            <i class="fa fa-star"></i>
                                                                        </c:forEach>
                                                                        <c:forEach begin="1" end="${5-r.rate}">
                                                                            <i class="fa fa-star-o empty"></i>
                                                                        </c:forEach>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                    <ul id="pagination">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End tab ratings-->

                                    <!-- Start tab comments-->
                                    <div role="tabpanel" class="tab-pane fade" id="comments">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p><label for="review">Your Comments</label></p>
                                                <p><textarea name="review" id="comment-content" rows="10" style="width:100%;"></textarea></p>
                                                <span id="comment-result"></span>
                                                <p>
                                                    <c:if test="${sessionScope.login_account eq null}">
                                                        <input type="button" onclick="location.href = 'login.jsp'" value="Submit">
                                                    </c:if>
                                                    <c:if test="${sessionScope.login_account ne null}">
                                                        <input type="button" onclick="addNewComment()" value="Submit">
                                                    </c:if>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="reviews">
                                                    <ul class="reviews">
                                                        <c:forEach var="c" items="${commentList}">
                                                            <li class="contentPage">
                                                                <div class="review-heading">
                                                                    <h5 class="name">${c.customerID.firstName} ${c.customerID.lastName}</h5>
                                                                    <p class="date"><fmt:formatDate value="${c.commentDate}" pattern="dd-MM-yyyy" /></p>
                                                                </div>
                                                                <div class="review-body">
                                                                    <p>${c.content}</p>
                                                                </div>
                                                            </li>
                                                            <c:if test="${empty c.respond}">
                                                                <li class="contentPage" style="padding-left: 165px;">
                                                                    <div class="review-heading">
                                                                        <h5 class="name">${c.customerID.firstName} ${c.customerID.lastName}</h5>
                                                                        <p class="date"><fmt:formatDate value="${c.respondDate}" pattern="dd-MM-yyyy" /></p>
                                                                    </div>
                                                                    <div class="review-body">
                                                                        <p>${c.respond}</p>
                                                                    </div>
                                                                </li>
                                                            </c:if>
                                                        </c:forEach>
                                                    </ul>
                                                    <ul id="pagination">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End tab comments-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /product tab -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!--Footer-->
        <jsp:include page="client-footer.jsp"/>

        <script type="text/javascript">
            function addProductToCartQuantity(productid, customerId)
            {
                var quantity = $('#pro-quantity').val();

                $.ajax({
                    url: "AddProductToCartWithQuatityServlet?productID=" + productid + "&cusID=" + customerId, "&quantity=" + quantity,
                            type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {

                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });
            }

            function addNewRating()
            {
//                var content = $('#content').val();
                var rating = $('input:radio[name="rating"]:checked').val();
                if (content.trim() === "") {
                    $("#rating-result").html('<label class="control-label" style="color: red; font-weight: normal;">You need review product</label>');
                    event.preventDefault();
                }
                else if (rating === undefined) {
                    $("#rating-result").html('<label class="control-label" style="color: red; font-weight: normal;">You need rating product</label>');
                    event.preventDefault();
                } else {
                    $.ajax({
                        url: "AddYourReview?command=rating&proid=" + '${pro.productID}' + "&cusid=" + '${sessionScope.login_account.customerID}' + "&rating=" + rating,
                        type: "POST",
                        //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                        success: function()
                        {
                            location.reload();
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            //
                        }
                    });
                }
            }

            function addNewComment()
            {
                var comment = $('#comment-content').val();
//                var rating = $('input:radio[name="rating"]:checked').val();
                if (content.trim() === "") {
                    $("#comment-result").html('<label class="control-label" style="color: red; font-weight: normal;">You need type something to comment product</label>');
                    event.preventDefault();
//                }
//                else if (rating === undefined) {
//                    $("#rating-result").html('<label class="control-label" style="color: red; font-weight: normal;">You need rating product</label>');
//                    event.preventDefault();
                } else {
                    $.ajax({
                        url: "AddYourReview?command=comment&proid=" + '${pro.productID}' + "&cusid=" + '${sessionScope.login_account.customerID}' + "&=comment" + comment,
                        type: "POST",
                        //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                        success: function()
                        {
                            location.reload();
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            //
                        }
                    });
                }
            }

            function addProductWishlist(productid, customerId)
            {

                $.ajax({
                    url: "AddProductToWishlist?productId=" + productid + "&cusId=" + customerId,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {

                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });

            }

            function removeProductWishlist(productid, customerId)
            {
                $.ajax({
                    url: "RemoveProductoutWislist?productId=" + productid + "&cusId=" + customerId,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {

                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });
            }
        </script>

        <script type="text/javascript">
            $(function() {
                var pageSize = 3; // Hiển thị 6 sản phẩm trên 1 trang
                showPage = function(page) {
                    $(".contentPage").hide();
                    $(".contentPage").each(function(n) {
                        if (n >= pageSize * (page - 1) && n < pageSize * page)
                            $(this).show();
                    });
                };
                showPage(1);
                ///** Cần truyền giá trị vào đây **///
                var totalRows = ${ratingListCount}; // Tổng số sản phẩm hiển thị
                var btnPage = 5; // Số nút bấm hiển thị di chuyển trang
                var iTotalPages = Math.ceil(totalRows / pageSize);

                var obj = $('#pagination').twbsPagination({
                    totalPages: iTotalPages,
                    visiblePages: btnPage,
                    onPageClick: function(event, page) {
                        /* console.info(page); */
                        showPage(page);
                    }
                });
                /*console.info(obj.data());*/
            });
        </script>

        <script>
            var slideIndex = 1;
            showSlides(slideIndex);

            function plusSlides(n) {
                showSlides(slideIndex += n);
            }

            function currentSlide(n) {
                showSlides(slideIndex = n);
            }

            function showSlides(n) {
                var i;
                var slides = document.getElementsByClassName("main-img-slides");
                var dots = document.getElementsByClassName("thumb-img");
                var captionText = document.getElementById("caption-img");
                if (n > slides.length) {
                    slideIndex = 1
                }
                if (n < 1) {
                    slideIndex = slides.length
                }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
                captionText.innerHTML = dots[slideIndex - 1].alt;
            }
        </script>
    </body>
</html>
