<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Change Passwordn Successfully - PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Change Password</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-10 col-md-offset-1 text-center">
                            <span class="icon"><i class="fa fa-check-circle-o" style="font-size: 100px; color: #31b131;"></i></span>
                            <h2>Change password successful!</h2>
                            <p>
                                <a href="index.jsp" class="newsletter-btn">Go back to Home</a>
                            </p>
                        </div>       

                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /SECTION -->

            <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>
    </body>
</html>
