<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri = "http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Your Wishlist - PowerSale</title>
    </head>
    <body>
        <!--Header-->
        <jsp:include page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Your Wishlist</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- Start product area -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-1"></div>
                        <!-- STORE -->
                        <div id="store" class="col-md-10">

                            <!-- store products -->
                            <div class="row">

                            <c:if test="${wishlistcount eq 0}">
                                <div class="col-md-12" style="height: 100px;">
                                    Your Wishlist is empty. Do you want to <a href="AllProduct">WISH some product</a>?
                                </div>
                            </c:if>
                            <c:forEach items="${wishlist}" var="p">
                                <jsp:include page="ProductStarByProID?proid=${p.productID}"/>
                                <!-- product -->
                                <div class="contentPage">
                                    <div class="col-md-3 col-xs-6">
                                        <div class="single-product">
                                            <div class="product-f-image">
                                                <a href="ProductDetail?proid=${p.productID}"><img src="${p.image1}" alt=""></a>
                                                <div class="product-hover">
                                                    <a href="#" class="add-to-cart-link" onclick='addProductToCart("${p.productID}", "${sessionScope.login_account.customerID}")'><i class="fas fa-cart-plus"></i> Add to cart</a>
                                                    <a href="#" class="add-to-wish-link" onclick='removeProductWishlist("${p.productID}", "${sessionScope.login_account.customerID}")'><i class="fas fa-heart"></i> Remove Wish</a>
                                                    <a href="ProductDetail?proid=${p.productID}" class="view-details-link"><i class="fa fa-link"></i> See details</a>
                                                </div>
                                            </div>

                                            <h2><a href="ProductDetail?proid=${p.productID}">${p.productName}</a></h2>

                                            <div class="product-wid-rating">
                                                <c:choose>
                                                    <c:when test="${empty avgStars}">
                                                        <i class="far fa-star"></i>
                                                        <i class="far fa-star"></i>
                                                        <i class="far fa-star"></i>
                                                        <i class="far fa-star"></i>
                                                        <i class="far fa-star"></i>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:forEach begin="1" end="${avgStars}">
                                                            <i class="fas fa-star"></i>
                                                        </c:forEach>
                                                        <c:forEach begin="1" end="${5-avgStars}">
                                                            <i class="far fa-star"></i>
                                                        </c:forEach>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>

                                            <div class="product-carousel-price">
                                                <ins>$<fmt:formatNumber type="number" maxFractionDigits="0" value="${p.price*(100-p.discountProduct)/100}"/></ins>
                                                <c:if test="${p.discountProduct ne 0}">
                                                    <del class="product-old-price">$${p.price}</del>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /product -->
                            </c:forEach>
                        </div>
                        <!-- /store products -->

                        <!-- store bottom filter -->
                        <div class="store-filter clearfix">
                            <span class="store-qty"></span>
                            <ul id="pagination"></ul>
                        </div>
                        <!-- /store bottom filter -->
                    </div>
                    <!-- /STORE -->
                    <div class="col-md-1"></div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- End product area -->

        <!--Footer-->
        <jsp:include page="client-footer.jsp"/>

        <script type="text/javascript">
            function addProductToCart(productid, customerId)
            {
                $.ajax({
                    url: "AddProductToCart?command=plus&productID=" + productid + "&cusID=" + customerId,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {

                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });
            }
            function removeProductWishlist(productid, customerId)
            {
                $.ajax({
                    url: "RemoveProductoutWislist?productId=" + productid + "&cusId=" + customerId,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {

                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });
            }
        </script>

        <script type="text/javascript">
            $(function() {
                var pageSize = 6; // Hiển thị 6 sản phẩm trên 1 trang
                showPage = function(page) {
                    $(".contentPage").hide();
                    $(".contentPage").each(function(n) {
                        if (n >= pageSize * (page - 1) && n < pageSize * page)
                            $(this).show();
                    });
                }
                showPage(1);
                ///** Cần truyền giá trị vào đây **///
                var totalRows = ${productListCount}; // Tổng số sản phẩm hiển thị
                var btnPage = 5; // Số nút bấm hiển thị di chuyển trang
                var iTotalPages = Math.ceil(totalRows / pageSize);

                var obj = $('#pagination').twbsPagination({
                    totalPages: iTotalPages,
                    visiblePages: btnPage,
                    onPageClick: function(event, page) {
                        /* console.info(page); */
                        showPage(page);
                    }
                });
                /*console.info(obj.data());*/
            });
        </script>


    </body>
</html>
