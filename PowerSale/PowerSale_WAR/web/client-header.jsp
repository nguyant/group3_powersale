<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <ul>
                        
                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <c:choose>
                    <c:when test="${empty sessionScope.login_account}">
                        <div class="header-right">
                            <ul class="list-unstyled list-inline">
                                <li><a href="login.jsp"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                            </ul>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="header-right">
                            <ul class="list-unstyled list-inline">
                                <li class="dropdown dropdown-small">
                                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="value">${sessionScope.login_account.firstName} ${sessionScope.login_account.lastName} </span><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="myAccount.jsp"><i class="fas fa-user"></i> My Account</a></li>
                                        <li><a href="ViewMyOrdersByCusID?cusid=${sessionScope.login_account.customerID}"><i class="fas fa-th-list"></i> Order History</a></li>
                                        <li><a href="getCartServet?customerId=${sessionScope.login_account.customerID}"><i class="fas fa-shopping-cart"></i> My Cart</a></li>
                                        <li><a href="getWishlistServet?customerId=${sessionScope.login_account.customerID}"><i class="fas fa-heart"></i> My Wishlist</a></li>
                                        <li><a href="Logout"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </c:otherwise>
                </c:choose>

            </div>
        </div>
    </div>
</div>
<!-- End header area -->

<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1>
                        <a href="index.jsp"><img width="200" height="80" src="content/img/logoPS(Final).png"></a>
                    </h1>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="shopping-item">
                            <c:choose>
                                <c:when test="${sessionScope.login_account eq null}">
                                    <a href="login.jsp">Wishlist <i class="fas fa-heart"></i> <span class="product-count">0</span></a>
                                    </c:when>
                                    <c:otherwise>
                                    <a href="getWishlistServet?customerId=${sessionScope.login_account.customerID}">Wishlist <i class="fas fa-heart"></i> <span class="product-count">
                                            <c:choose>
                                                <c:when test="${empty sessionScope.countWishlist}">
                                                    0
                                                </c:when>
                                                <c:otherwise>
                                                    ${sessionScope.countWishlist}
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="shopping-item">
                            <c:choose>
                                <c:when test="${sessionScope.login_account eq null}">
                                    <a href="login.jsp">Cart <i class="fas fa-shopping-cart"></i> <span class="product-count">0</span></a>
                                </c:when>
                                <c:otherwise>
                                    <a href="login.jsp">Cart <i class="fas fa-shopping-cart"></i> <span class="product-count">
                                            <c:choose>
                                                <c:when test="${empty sessionScope.countCart}">
                                                    0
                                                </c:when>
                                                <c:otherwise>
                                                    ${sessionScope.countCart}
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End site branding area -->

<div class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="AllProduct">All Products</a></li>
                        <c:forEach var="c" items="${sessionScope.categoriesList}">
                        <li><a href="AllProductByCategory?cateid=${c.categoryID}">${c.categoryName}</a></li>
                        </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End mainmenu area -->