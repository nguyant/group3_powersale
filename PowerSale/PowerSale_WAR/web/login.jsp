<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Login - PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Login</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- Start login area -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-3">
                        </div>

                        <div class="col-md-6">
                            <form id="login-form" action="Login" class="login login-form" method="post">
                                <div class="login-content">
                                    <div class="login-title">
                                        <h3 class="title">Login To Your Account</h3>
                                    </div>
                                    <p class="form-row form-row-first">
                                        <label for="email">Email <span class="required">*</span></label>
                                        <input type="text" id="email" name="email" placeholder="Enter your Email" class="input-text">
                                    </p>
                                    <p class="form-row form-row-last">
                                        <label for="password">Password <span class="required">*</span></label>
                                        <input type="password" id="password" name="password" placeholder="Enter your Password" class="input-text">
                                    </p>

                                    <div class="clear"></div>

                                    <p class="form-row">
                                        <input type="submit" value="Login" name="login" class="button">
                                    </p>
                                </div>
                                
                                <c:if test="${not empty sessionScope.login_message}">
                                    ${sessionScope.login_message}
                                </c:if>

                                <br/>
                                <div class="form-group" style="float: right">
                                    <a href="findUser.jsp"><span style="color: blue">Forgot Password?</span></a>
                                </div>
                                <div class="form-group" style="float: left">
                                    <span>New member?</span>
                                    <a href="register.jsp"><span style="color: blue">Register</span></a>
                                </div>

                                <div class="clear"></div>
                            </form>
                        </div>

                        <div class="col-md-3">                        
                        </div>                

                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- End login area -->

            <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>
    </body>
</html>
