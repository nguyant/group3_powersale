<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Register Successfully - PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Register Successfully</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 text-center">
                            <span class="icon"><i class="fa fa-check-circle-o" style="font-size: 100px; color: #31b131;"></i></span>
                            <h2>You have been successfully registered!</h2>
                            <p>
                                <a href="login.jsp" class="newsletter-btn">Go back to Login</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
            <!-- /SECTION -->

            <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>
    </body>
</html>
