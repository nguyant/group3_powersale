<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="footer-top-area">
    <!--<div class="zigzag-bottom"></div>-->
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="footer-about-us">
                    <h2>
                        <a href="index.html"><img width="200" height="80" src="content/img/logoPS(Final).png"></a>
                    </h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis sunt id doloribus vero quam laborum quas alias dolores blanditiis iusto consequatur, modi aliquid eveniet eligendi iure eaque ipsam iste, pariatur omnis sint!
                        Suscipit, debitis, quisquam. Laborum commodi veritatis magni at?</p>
                    <div class="footer-social">
                        <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" target="_blank"><i class="fab fa-twitter"></i></a>
                        <a href="#" target="_blank"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="footer-menu">
                    <h2 class="footer-wid-title">User Navigation </h2>
                    <ul>
                        <li><a href="myAccount.jsp"><i class="fas fa-user"></i> My Account</a></li>
                        <li><a href="ViewMyOrdersByCusID?cusid=${sessionScope.login_account.customerID}"><i class="fas fa-th-list"></i> Order History</a></li>
                        <li><a href="getWishlistServet?customerId=${sessionScope.login_account.customerID}"><i class="fas fa-shopping-cart"></i> My Cart</a></li>
                        <li><a href="#"><i class="fas fa-heart"></i> My Wishlist</a></li>
                        <li><a href="Logout"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="footer-menu">
                    <h2 class="footer-wid-title">Categories</h2>
                    <ul>
                        <c:forEach var="c" items="${sessionScope.categoriesList}">
                            <li><a href="AllProductByCategory?cateid=${c.categoryID}">${c.categoryName}</a></li>
                        </c:forEach>
                    </ul>
                </div>
            </div>

            <!-- <div class="col-md-3 col-sm-6">
                <div class="footer-newsletter">
                    <h2 class="footer-wid-title">Newsletter</h2>
                    <p>Sign up to our newsletter and get exclusive deals you wont find anywhere else straight to your inbox!</p>
                    <div class="newsletter-form">
                        <form action="#">
                            <input type="email" placeholder="Type your email">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<!-- End footer top area -->

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy; 2015 PowerSale. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End footer bottom area -->

<!-- Latest jQuery form server -->
<script src="content/js/jquery.min.js"></script>

<!-- Bootstrap JS form CDN -->
<script src="content/js/bootstrap.min.js"></script>

<!-- jQuery sticky menu -->
<script src="content/js/owl.carousel.min.js"></script>
<script src="content/js/jquery.sticky.js"></script>

<!-- jQuery easing -->
<script src="content/js/jquery.easing.1.3.min.js"></script>

<!-- Main Script -->
<script src="content/js/main.js"></script>

<!-- Jquery Plugins -->
<script src="content/js/nouislider.min.js" type="text/javascript"></script>
<script src="content/js/jquery.zoom.min.js" type="text/javascript"></script>
<script src="content/js/jquery.twbsPagination.js" type="text/javascript"></script>
<script src="content/js/jquery.validate.min.js" type="text/javascript"></script>

<!-- Slider -->
<script type="text/javascript" src="content/js/bxslider.min.js"></script>
<script type="text/javascript" src="content/js/script.slider.js"></script>