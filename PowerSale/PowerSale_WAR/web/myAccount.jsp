<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri = "http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>My Account</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Product</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- Start account info area -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-6">
                            <form id="accinfo-form" action="EditMyProfile" class="login login-form" method="post">
                                <div class="login-content">
                                    <div class="login-title">
                                        <h3 class="title">Account Profiles ID: ${sessionScope.login_account.customerID}</h3>
                                </div>

                                <p class="form-row form-row-first">
                                    <label for="first-name">First Name <span class="required">*</span></label>
                                    <input type="text" id="first-name" name="first-name" value="${sessionScope.login_account.firstName}" placeholder="Enter your First Name" class="input-text">
                                </p>

                                <p class="form-row form-row-first">
                                    <label for="last-name">Last Name <span class="required">*</span></label>
                                    <input type="text" id="last-name" name="last-name" value="${sessionScope.login_account.lastName}" placeholder="Enter your Last Name" class="input-text">
                                </p>

                                <p class="form-row form-row-first">
                                    <label for="email">Email <span class="required">*</span></label>
                                    <input type="text" id="email" name="email" value="${sessionScope.login_account.email}" placeholder="Enter your Email" class="input-text">
                                </p>

                                <p class="form-row form-row-first">
                                    <label for="address">Address <span class="required">*</span></label>
                                    <input type="text" id="address" name="address" value="${sessionScope.login_account.address}" placeholder="Enter your Address" class="input-text">
                                </p>

                                <p class="form-row form-row-first">
                                    <label for="tel">Phone <span class="required">*</span></label>
                                    <input type="text" id="tel" name="tel" value="${sessionScope.login_account.phone}" placeholder="Enter your Phone" class="input-text" data-inputmask="'mask': '(999) 999-9999'" data-mask>
                                </p>

                                <p class="form-row form-row-first">
                                    <label for="gender">Gender <span class="required">*</span></label>
                                    <select id="gender" name="gender" class="input-cbo">
                                        <c:choose>
                                            <c:when test="${sessionScope.login_account.gender eq true}">
                                                <option value="Male" selected="true">Male</option>
                                                <option value="Female">Female</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="Male">Male</option>
                                                <option value="Female" selected="true">Female</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </select>
                                </p>

                                <div class="clear"></div>

                                <p class="form-row">
                                    <input type="submit" value="Update Profiles" name="login" class="button">
                                </p>
                            </div>

                            <br/>
                            <div class="form-group" style="float: right">
                                <a href="changePassword.jsp"><span style="color: blue">Change password</span></a>
                            </div>

                            <div class="clear"></div>
                        </form>
                    </div>

                    <div class="col-md-1">
                    </div>

                    <div class="col-md-5">
                        <form id="formAvatar" action="EditMyAvatar" method="post" enctype="multipart/form-data">
                            <div class="login-content">
                                <div class="login-title">
                                    <h3 class="title">AVATAR</h3>
                                </div>                            
                                <div class="form-group">
                                    <input class="input" type="hidden" name="cusID" value="${sessionScope.login_account.customerID}">
                                    <img class="img-rounded" id="blah" src="${sessionScope.login_account.avatar}" style="margin:auto; width:150px;display:block;" onclick="document.getElementById('exampleInputFile').click();">
                                    <input type="file" id="exampleInputFile" name="inputImage" accept="image/*" onchange="readURL(this);" style="display: none;">
                                    <br/><span>Click on the image to change your avatar</span>
                                    <input type="submit" class="input order-submit" style="font-weight: bold; color: red;" value="CHANGE">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- End account info area -->

        <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>

        <script type="text/javascript">
            function readURL(input) {
                /*************** check image **********/
                var fileInput = document.getElementById('exampleInputFile');
                var filePath = fileInput.value;
                var allowedExtensions = /(\.jpg|\.png|\.jpeg|\.gif)$/i;
                if (!allowedExtensions.exec(filePath)) {
                    alert('Please upload file having extensions .jpg/.png/.jpeg/.gif only.');
                    fileInput.value = '';
                    $('#blah').attr('src', '${sessionScope.login_account.avatar}');
                    return false;
                } else {
                    //Image preview
                    if (fileInput.files && fileInput.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#blah')
                                    .attr('src', e.target.result)
                                    .width(150)
                                    .height(150);
                        };
                        reader.readAsDataURL(input.files[0]);
                    }
                }
            }
        </script>
        <script type="text/javascript">
            $().ready(function() {
                $("#accinfo-form").validate({
                    onfocusout: false,
                    onkeyup: false,
                    onclick: false,
                    rules: {
                        "first-name": {
                            required: true,
                            maxlength: 50
                        },
                        "last-name": {
                            required: true,
                            maxlength: 50
                        },
                        "email": {
                            required: true,
                            email: true,
                            maxlength: 100
                        },
                        "address": {
                            required: true,
                            validateAddress: true
                        },
                        "tel": {
                            required: true,
                            validatePhone: true
                        },
                        "gender": {
                            required: true
                        }
                    },
                    messages: {
                        "first-name": {
                            required: "Please enter a first name",
                            maxlength: "Your first name must be maximum 50 characters"
                        },
                        "last-name": {
                            required: "Please enter a last name",
                            maxlength: "Your last name must be maximum 50 characters"
                        },
                        "email": {
                            required: "Please enter a valid email address",
                            email: "Please enter a valid email address",
                            maxlength: "Your email must be maximum 100 characters"
                        },
                        "address": {
                            required: "Please enter a your address"
                        },
                        "tel": {
                            required: "Please enter a phone number"
                        },
                        "gender": {
                            required: "Please choose a gender"
                        }
                    }
                });
                $.validator.addMethod("validatePhone", function(value, element) {
                    return this.optional(element) || /^[(]{1}[0]{1}[0-9\-\s\)\+]{12}$/i.test(value);
                }, "Please enter a valid phone number");
                $.validator.addMethod("validateAddress", function(value, element) {
                    return this.optional(element) || /^\d+[ |/](?:[/A-Za-z0-9-]+[ ]?)+(?:,)+(?:[ A-Za-z0-9-]+[ ]?)+(?:,)+(?:[A-Za-z0-9 -]+[ ]?)?(?:,)?(?:[A-Za-z -]+[ ]?)$/i.test(value);
                }, "Please enter a valid address (ex: 50 Vo Van Kiet, district 1, Ho Chi Minh city)");
            });
//            $(function() {
//                $("[data-mask]").inputmask();
//            });
        </script>

        <script type="text/javascript">
            function edit_profiles() {
                var customerID = '${sessionScope.login_account.customerID}';
                var firstname = $('#first-name').val();
                var lastname = $('#last-name').val();
                var tel = $('#tel').val();
                var address = $('#address').val();
                var gender = $('#gender').val();

                if (!($("#accinfo-form").valid())) {
                    swal("Error. Please check Account Information again!");
                } else {
                    $.ajax({
                        url: "EditMyProfile?customerID=" + customerID + "&firstname=" + firstname + "&lastname=" + lastname + "&tel=" + tel + "&address=" + address + "&gender=" + gender,
                        type: "POST",
                        success: function()
                        {
                            location.reload();
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            alert(orderid + "\n" + qt1 + "\n" + productid);
                        }
                    });
                }
            }
        </script>
    </body>
</html>
