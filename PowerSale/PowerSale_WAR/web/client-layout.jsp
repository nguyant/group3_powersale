<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="content/img/logoPS(Simple).png">

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet" href="content/css/bootstrap.min.css">

<!-- nouislider -->
<link type="text/css" rel="stylesheet" href="content/css/client/nouislider.min.css"/>

<!-- Font Awesome -->
<link rel="stylesheet" href="content/css/font-awesome.min.css">
<link rel="stylesheet" href="vendor/fontawesome/css/all.min.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="content/css/owl.carousel.css">
<link rel="stylesheet" href="content/css/style.css">
<link rel="stylesheet" href="content/css/responsive.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<jsp:include page="initPage"></jsp:include>
