<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Order Completed - PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Order Completed</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 text-center">
                            <span class="icon"><i class="fa fa-shopping-cart" style="font-size: 100px; color: #d30819;"></i></span>
                            <h2>Thank you for purchasing, Your order is complete</h2>
                            <p>
                                <a href="AllProduct" class="newsletter-btn">Continue Shopping</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->

            <!-- /SECTION -->

            <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>
    </body>
</html>
