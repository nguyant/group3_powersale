<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Reset Password - PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>
        
        <!-- Start big title area -->
        <div class="product-big-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-bit-title text-center">
                            <h2>Reset Password</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End big title area -->
            
            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-3">                        
                        </div>

                        <div class="col-md-6">                      
                            <form id="register-form" action="resetPasswordServlet" class="login login-form" method="post">
                                <div class="login-content">
                                    <div class="login-title">
                                        <h3 class="title">Reset Password Form</h3>
                                    </div>

                                <p class="form-row form-row-last">
                                    <label for="password">New Password <span class="required">*</span></label>
                                    <input type="password" id="password" name="password" placeholder="Enter New Password" class="input-text">
                                </p>

                                <p class="form-row form-row-first">
                                    <label for="password_confirm">Confirm Password <span class="required">*</span></label>
                                    <input type="text" id="password_confirm" name="password_confirm" placeholder="Confirm Password" class="input-text">
                                </p>

                                <div class="clear"></div>

                                <p class="form-row">
                                    <input type="submit" value="Change" name="login" class="button">
                                </p>
                            </div>

                            <div class="clear"></div>
                        </form>
                    </div>

                    <div class="col-md-3">                        
                    </div>                

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->
        
        <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>
        
        <script type="text/javascript">
            $().ready(function() {
                $("#formLogin").validate({
                    onfocusout: false,
                    onkeyup: false,
                    onclick: false,
                    rules: {
                        "password": {
                            required: true,
                            minlength: 6,
                            maxlength: 30
                        },
                        "password_confirm": {
                            required: true,
                            equalTo: "#password",
                            minlength: 6
                        }
                    },
                    messages: {
                        "password": {
                            required: "Please provide a new password",
                            minlength: "Your password must consist of at least 6 characters",
                            maxlength: "Your password must be maximum 30 characters"
                        },
                        "password_confirm": {
                            required: "Please provide a new password",
                            equalTo: "Please enter the same password as above",
                            minlength: "Your password must consist of at least 6 characters"
                        }
                    }
                });
            });
        </script>
    </body>
</html>
