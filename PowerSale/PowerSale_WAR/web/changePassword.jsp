<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Change Password - PowerSale</title>
    </head>
    <body>
        <!-- Header -->
        <jsp:include  page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Change Password</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">

                        <div class="col-md-3">                        
                        </div>

                        <div class="col-md-6">                      
                            <form id="register-form" action="changePassword" class="login login-form" method="post">
                                <div class="login-content">
                                    <div class="login-title">
                                        <h3 class="title">Change Password Form</h3>
                                    </div>
                                    <p class="form-row form-row-last">
                                        <label for="password">Current Password <span class="required">*</span></label>
                                        <input type="password" id="curpass" name="curpass" placeholder="Enter current Password" class="input-text">
                                        <input type="password" id="oldpass" name="oldpass" value="${sessionScope.login_account.password}" class="hidden">
                                </p>

                                <p class="form-row form-row-last">
                                    <label for="pass">New Password <span class="required">*</span></label>
                                    <input type="password" id="pass" name="pass" placeholder="Enter New Password" class="input-text">
                                </p>

                                <p class="form-row form-row-first">
                                    <label for="confirmpass">Confirm Password <span class="required">*</span></label>
                                    <input type="text" id="confirmpass" name="confirmpass" placeholder="Confirm Password" class="input-text">
                                </p>

                                <div class="clear"></div>

                                <p class="form-row">
                                    <input type="submit" value="Change" name="login" class="button">
                                </p>
                            </div>

                            <div class="clear"></div>
                        </form>
                    </div>

                    <div class="col-md-3">                        
                    </div>                

                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!-- Footer -->
        <jsp:include page="client-footer.jsp"/>

        <script type="text/javascript">
            $().ready(function() {
                $("#PasswordForm").validate({
                    onfocusout: false,
                    onkeyup: false,
                    onclick: false,
                    rules: {
                        "curpass": {
                            required: true,
                            equalTo: "#oldpass"
                        },
                        "pass": {
                            required: true,
                            minlength: 6,
                            maxlength: 30
                        },
                        "confirmpass": {
                            required: true,
                            equalTo: "#pass",
                            minlength: 6
                        }
                    },
                    messages: {
                        "curpass": {
                            required: "Please enter a current password",
                            equalTo: "current password incorrect"
                        },
                        "pass": {
                            required: "Please provide a new password",
                            minlength: "Your password must consist of at least 6 characters",
                            maxlength: "Your password must be maximum 30 characters"
                        },
                        "confirmpass": {
                            required: "Please provide a new password",
                            equalTo: "Please enter the same password as above",
                            minlength: "Your password must consist of at least 6 characters"
                        }
                    }
                });
            });
        </script>
    </body>
</html>
