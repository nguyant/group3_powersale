<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri = "http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="client-layout.jsp"/>
        <title>Your Cart - PowerSale</title>
    </head>
    <body>
        <!--Header-->
        <jsp:include page="client-header.jsp"></jsp:include>

            <!-- Start big title area -->
            <div class="product-big-title-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-bit-title text-center">
                                <h2>Your Cart</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End big title area -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-content-right">
                                <div class="woocommerce">
                                    <form method="post" action="checkout.jsp">
                                        <table cellspacing="0" class="shop_table cart">
                                            <thead>
                                                <tr>
                                                    <th class="product-remove">&nbsp;</th>
                                                    <th class="product-thumbnail">&nbsp;</th>
                                                    <th class="product-name">Product</th>
                                                    <th class="product-price">Price</th>
                                                    <th class="product-quantity">Quantity</th>
                                                    <th class="product-subtotal">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${cartquantity}" var="p">
                                                <tr class="cart_item">
                                                    <td class="product-remove">
                                                        <a title="Remove this item" class="remove" href="#" onclick="RemoveCart('${p.key.productID}', '${p.value.customerID.customerID}')"><i class="fas fa-times"></i></a>
                                                    </td>

                                                    <td class="product-thumbnail">
                                                        <a href="ProductDetail?proid=${p.key.productID}"><img width="145" height="145" class="shop_thumbnail" src="${p.key.image1}"></a>
                                                    </td>

                                                    <td class="product-name">
                                                        <a href="ProductDetail?proid=${p.key.productID}">${p.key.productName}</a>
                                                    </td>

                                                    <td class="product-price">
                                                        <span class="amount">
                                                            <ins>$<fmt:formatNumber type="number" maxFractionDigits="0" value="${p.key.price*(100-p.key.discountProduct)/100}"/></ins>
                                                            <c:if test="${p.key.discountProduct ne 0}">
                                                                <del class="product-old-price">$${p.key.price}</del>
                                                            </c:if>
                                                        </span>
                                                    </td>

                                                    <td class="product-quantity">
                                                        <div class="quantity buttons_added">
                                                            <input type="number" id="qt${p.key.productID}" onchange="edit_posale('${p.key.productID}', '${p.value.customerID.customerID}', '${(p.key.price*(100-p.key.discountProduct)/100)}', '${(p.key.price*(100-p.key.discountProduct)/100) * p.value.isCart}', '${p.key.quantity}')" size="4" class="input-text qty text" title="Qty" value="${p.value.isCart}" min="0" step="1">
                                                        </div>
                                                    </td>

                                                    <td class="product-subtotal">
                                                        <span class="amount" id="prototal${p.key.productID}}">$${(p.key.price*(100-p.key.discountProduct)/100) * p.value.isCart}</span>
                                                    </td>

                                                </tr>
                                            </c:forEach>
                                            <tr>
                                                <td colspan="3">Cart Total</td>
                                                <td id="ort" colspan="3">$${ordertotal}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">Shipping Plan</td>
                                                <td colspan="3">Free Shipping</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">Order Total</td>
                                                <td id="ort" colspan="3">$${ordertotal}</td>
                                            </tr>
                                            <tr>
                                                <td class="actions" colspan="6">
                                                    <input type="submit" onclick="location.href='checkout.jsp'" value="Checkout" name="proceed" class="checkout-button button alt wc-forward">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!--Footer-->
        <jsp:include page="client-footer.jsp"/>

        <script type="text/javascript">
            function RemoveCart(productid, customerid) {
                $.ajax({
                    url: "AddProductToCart?command=remove&productID=" + productid + "&cusID=" + customerid,
                    type: "POST",
                    //data: {name: name1, price: price1, product_id: id, number: number, registerid: 75, waiter: waiterID},
                    success: function()
                    {
                        location.reload();
                    },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            alert("error");
                        }
                    });
                }

            function edit_posale(productid, customerid, curprice, curprototal, limit) {
                var curordertotal = ${ordertotal};

                var qt1 = parseInt($('#qt' + productid).val());

                if (qt1 === NaN) {

                }
                else if (qt1 === 0) {
                    confirm("Quantity equal 0? Do you really want to remove this?");
                } else if (qt1 > 0) {
                    if (qt1 > parseInt(limit) && qt1 > 99) {
                        swal("Quantity isn't more than stock quantity(" + limit "). Quantity isn't more than 99.");
                    } else {
                        $.ajax({
                            url: "AddProductToCartWithQuatityServlet?command=plus&productID=" + productid + "&cusID=" + customerid + "&quantity=" + qt1,
                            type: "POST",
                            success: function()
                            {
//                                var afterprototal = parseInt(curprice) * qt1;
//                                $('#prototal' + productid).html(afterprototal);
//                                var afterordertotal = curordertotal - parseInt(curprototal) + afterprototal;
//                                $('#ort').html(afterordertotal);
                                location.reload();
                            },
                            error: function(jqXHR, textStatus, errorThrown)
                            {
                                alert(orderid + "\n" + qt1 + "\n" + productid);
                            }
                        });
                    }
                }
            }

        </script>

    </body>
</html>
