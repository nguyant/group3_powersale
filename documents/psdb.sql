--USE master
--DROP DATABASE psdb

CREATE DATABASE psdb
GO
USE psdb
GO

CREATE TABLE Admins
(
	AdminID	varchar(10) PRIMARY KEY,
	Email varchar(100) not null,
	[Password] varchar(30) not null,
	FullName nvarchar(50) not null,
	Avatar varchar(200) not null,
	CreatedDate date default getDate(),
	AdminRole int default 0,
	IsStatus bit default 1
)
GO

CREATE TABLE Brands (
	BrandID varchar(10) primary key,
	BrandName nvarchar(100) not null,
	BrandImages nvarchar(max),
	Descriptions nvarchar(max),
	IsStatus bit default 1 
)
GO

CREATE TABLE Categories (
	CategoryID varchar(10) primary key,
	CategoryName nvarchar(100) not null,
	CategoryImage varchar(max) not null,
	IsStatus bit default 1,
)
GO

CREATE TABLE Products (
	ProductID varchar(10) primary key,
	ProductName nvarchar(100) not null,
	Price int not null,
	DiscountProduct int not null,
	CategoryID varchar(10) not null, 
	BrandID varchar(10) not null,
	Descriptions nvarchar(max),
	Quantity int not null, --0: can not purchase.
	Feature nvarchar(100), --New, Hot, Sale, etc.
	Image1 varchar(max),
	Image2 varchar(max),
	Image3 varchar(max),
	Image4 varchar(max),
	IsStatus bit default 1,
	CONSTRAINT FK_ProductsCategories FOREIGN KEY (CategoryID) REFERENCES Categories(CategoryID),
	CONSTRAINT FK_ProductsBrands FOREIGN KEY (BrandID) REFERENCES Brands(BrandID)
)
GO

CREATE TABLE ProductsDetails (
	ProductDetailsID int identity(1,1) primary key,
	ProductID varchar(10) not null,
	CPU	nvarchar(200),
	Memory nvarchar(200),
	VGA nvarchar(200),
	HDD nvarchar(200),
	Camera nvarchar(200),
	Display nvarchar(300),	
	Battery nvarchar(100),
	Weights nvarchar(20),
	CONSTRAINT FK_ProductsProDetails FOREIGN KEY (ProductID) REFERENCES Products(ProductID),
)
GO

CREATE TABLE Customers (
	CustomerID varchar(10) primary key,
	Email varchar(100) not null,
	[Password] varchar(30) not null,
	FirstName nvarchar(50) not null,
	LastName nvarchar(50) not null,
	Gender bit default 1,
	Phone varchar(20),
	[Address] nvarchar(max),
	Avatar varchar(200) not null,
	CreatedDate date default getDate(),
	IsStatus bit default 1
)
GO

CREATE TABLE Ratings (
	RatingID int identity(1,1) primary key,
	ProductID varchar(10) not null,
	CustomerID varchar(10) not null,
	Rate int not null,
	RatingDate date default getdate(),
	CONSTRAINT FK_RatingProduct FOREIGN KEY (ProductID) REFERENCES Products(ProductID),
	CONSTRAINT FK_RatingCustomer FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID)
)
GO

CREATE TABLE Comments (
	CommentID int identity(1,1) primary key,
	ProductID varchar(10) not null,
	CustomerID varchar(10) not null,
	Content nvarchar(max),
	Respond nvarchar(max),
	CommentDate date default getdate(),
	RespondDate date default getdate(),
	IsDisplay bit not null default 1,
	CONSTRAINT FK_CommentProduct FOREIGN KEY (ProductID) REFERENCES Products(ProductID),
	CONSTRAINT FK_CommentCustomer FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID)
)
GO

CREATE VIEW AverageRatings AS
	SELECT ROW_NUMBER() OVER (ORDER BY p.ProductName) AS id, p.ProductID, p.ProductName, AVG(convert(decimal(4,2),r.Rate)) AS averageRating, AVG(r.Rate) AS star
	FROM Products p JOIN Ratings r ON p.ProductID = r.ProductID
	GROUP BY p.ProductName, p.ProductID
GO

CREATE TABLE WishAndCart (
	WishlistID int identity(1,1) primary key,
	CustomerID varchar(10) not null foreign key references Customers(CustomerID),
	ProductID varchar(10) foreign key references Products(ProductID) not null,
	DateCreated date default getDate(),
	IsCart int default 0 not null
)
GO

CREATE TABLE Orders (
	OrderID varchar(10) primary key,
	CustomerID varchar(10) not null foreign key references Customers(CustomerID),
	Total int not null,
	ShipName nvarchar(100) not null,
	ShipPhone varchar(20) not null,
	ShipAddress nvarchar(200) not null,
	ShipDate date not null,
	ShipNote nvarchar(max),
	OrderDate date default getDate(),
	PaymentMethod nvarchar(100) not null, --Cash, Bank Transfer, Credit Card
	ProcessStatus nvarchar(50) not null, -- Processing, Shipping, Completedd
	IsStatus bit default 0
)
GO

CREATE TABLE OrdersDetails (
	OdID int identity(1,1) primary key,
	OrderID varchar(10) not null foreign key references Orders(OrderID),
	ProductID varchar(10) foreign key references Products(ProductID) not null,
	SellingPrice int not null,
	Quantity int not null,
	IsStatus bit default 1
)	
GO

CREATE VIEW OrderListThisMonth AS
	SELECT * FROM Orders WHERE MONTH(OrderDate) = datepart(month,getdate())
GO

CREATE VIEW TopSellingThisYear AS 
SELECT TOP 10 p.ProductID, p.ProductName, p.Image1, p.Price, p.DiscountProduct, c.CategoryName, SUM(convert(decimal(4,0),r.Quantity)) AS TopSelling
	FROM Products p JOIN OrdersDetails r ON p.ProductID = r.ProductID JOIN Categories c ON p.CategoryID = c.CategoryID JOIN Orders o ON o.OrderID = r.OrderID
	WHERE YEAR(o.OrderDate) = datepart(YEAR,getdate())
	GROUP BY p.ProductName, p.ProductID, p.Image1, p.Price, p.DiscountProduct,c.CategoryName
	ORDER BY TopSelling DESC, p.Price DESC
GO

CREATE VIEW TopRatingThisYear AS 	
	SELECT TOP 10 p.ProductID, p.ProductName, p.Image1, p.Price, p.DiscountProduct, c.CategoryName,AVG(convert(decimal(4,2),r.Rate)) AS averageRating, COUNT(convert(decimal(4,0),r.Rate)) AS countRating
	FROM Products p JOIN Ratings r ON p.ProductID = r.ProductID JOIN Categories c ON p.CategoryID = c.CategoryID
	WHERE YEAR(r.RatingDate) = datepart(YEAR,getdate())
	GROUP BY p.ProductName, p.ProductID,p.Image1,p.Price,p.DiscountProduct,c.CategoryName
	ORDER BY averageRating DESC, countRating DESC
GO

CREATE VIEW TopRatingDateRange AS 	
	SELECT TOP 1000000 p.ProductID, p.ProductName, p.Image1, p.Price, p.DiscountProduct, c.CategoryName,AVG(convert(decimal(4,2),r.Rate)) AS averageRating, COUNT(convert(decimal(4,0),r.Rate)) AS countRating
	FROM Products p JOIN Ratings r ON p.ProductID = r.ProductID JOIN Categories c ON p.CategoryID = c.CategoryID
	GROUP BY p.ProductName, p.ProductID,p.Image1,p.Price,p.DiscountProduct,c.CategoryName
	ORDER BY averageRating DESC, countRating DESC
GO

CREATE VIEW Report AS
	SELECT row_number() OVER (ORDER BY a.OrderID) AS rowid, a.OrderID, b.ProductID,c.ProductName, a.OrderDate, a.ShipDate,b.SellingPrice, b.Quantity, d.CustomerID, d.FirstName,d.LastName,a.ShipName, a.ProcessStatus,Total=(b.SellingPrice*b.Quantity)      
	FROM Orders a JOIN OrdersDetails b ON a.OrderID = b.OrderID JOIN Products c ON b.ProductID=c.ProductID JOIN Customers d ON a.CustomerID =d.CustomerID
GO

INSERT Admins(AdminID, Email, [Password], FullName, Avatar) VALUES
('AD001','tan@mail.com', '123456', 'Nguyen Nhat Tan', 'content/img/Avatars/default-avatar.jpg'),
('AD002','anh@mail.com', '123456', 'Le Duc Anh','content/img/Avatars/default-avatar5.jpg'),
('AD003','nhi@mail.com', '123456', 'Nguyen Le Ngoc Nhi', 'content/img/Avatars/default-avatar1.jpg'),
('AD004','nam@mail.com', '123456', 'Nguyen Hoang Nam', 'content/img/Avatars/default-avatar2.jpg')
GO

INSERT Customers(CustomerID, Email, [Password], FirstName, LastName, Gender, Phone, Avatar, [Address]) VALUES
('CS001','thang@mail.com', '123456', 'Thang', 'Nguyen', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar.jpg','590 CMT8, Q3, HCM'),
('CS002','duy@mail.com', '123456', 'Duy', 'Khac', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS003','brian@mail.com', '123456', 'Brian', 'Welcker', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS004','tete@mail.com', '123456', 'Tete', 'Mensa-Annan', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS005','syed@mail.com', '123456', 'Syed', 'Abbas', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar.jpg','590 CMT8, Q3, HCM'),
('CS006','tuan@mail.com', '123456', 'Tuan', 'Tran', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS007','nam@mail.com', '123456', 'Nam', 'Nguyen', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar.jpg','590 CMT8, Q3, HCM'),
('CS008','ronald@mail.com', '123456', 'Ronald', 'Adina', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS009','samuel@mail.com', '123456', 'Samuel', 'Agcaoili', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS010','james@mail.com', '123456', 'James', 'Aguilar', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS011','gail@mail.com', '123456', 'Gail', 'Erickson', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS012','janice@mail.com', '123456', 'Janice', 'Galvin', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS013','jill@mail.com', '123456', 'Jill', 'Williams', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar5.jpg','590 CMT8, Q3, HCM'),
('CS014','catherine@mail.com', '123456', 'Catherine', 'Abel', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS015','kim@mail.com', '123456', 'Kim', 'Abercrombie', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS016','frances@mail.com', '123456', 'Frances', 'Adams', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar5.jpg','590 CMT8, Q3, HCM'),
('CS017','margaret@mail.com', '123456', 'Margaret', 'Smith', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS018','carla@mail.com', '123456', 'Carla', 'Adams', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS019','kimitry@mail.com', '123456', 'Kim', 'Akers', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS020','lili@mail.com', '123456', 'Lili', 'Alameda', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar5.jpg','590 CMT8, Q3, HCM'),
('CS021','TwannaEvans@mail.com', '123456', 'Twanna', 'Evans', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS022','AnnEvans@mail.com', '123456', 'Ann', 'Evans', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS023','JohnEvans@mail.com', '123456', 'John', 'Evans', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS024','MarcFaeber@mail.com', '123456', 'Marc', 'Faeber', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar.jpg','590 CMT8, Q3, HCM'),
('CS025','FadiFakhouri@mail.com', '123456', 'Fadi', 'Fakhouri', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar.jpg','590 CMT8, Q3, HCM'),
('CS026','CarolynFarino@mail.com', '123456', 'Carolyn', 'Farino', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS027','GeriFarrell@mail.com', '123456', 'Geri', 'Farrell', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS028','HanyingFeng@mail.com', '123456', 'Hanying', 'Feng', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS029','RhodaFinley@mail.com', '123456', 'Rhoda', 'Finley', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS030','DuaneFitzgerald@mail.com', '123456', 'Duane', 'Fitzgerald', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS031','JamesFine@mail.com', '123456', 'James', 'Fine', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS032','KathieFlood@mail.com', '123456', 'Kathie', 'Flood', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS033','JayFluegel@mail.com', '123456', 'Jay', 'Fluegel', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS034','KellyFocht@mail.com', '123456', 'Kelly', 'Focht', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS035','JeffreyFord@mail.com', '123456', 'Jeffrey', 'Ford', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS036','GarthFort@mail.com', '123456', 'Garth', 'Fort', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar.jpg','590 CMT8, Q3, HCM'),
('CS037','DorothyFox@mail.com', '123456', 'Dorothy', 'Fox', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar4.jpg','590 CMT8, Q3, HCM'),
('CS038','JudithFrazier@mail.com', '123456', 'Judith', 'Frazier', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS039','JohnFredericksen@mail.com', '123456', 'John', 'Fredericksen', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS040','SusanFrench@mail.com', '123456', 'Susan', 'French', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar5.jpg','590 CMT8, Q3, HCM'),
('CS041','LiamFriedland@mail.com', '123456', 'Liam', 'Friedland', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS042','MihailFrintu@mail.com', '123456', 'Mihail', 'Frintu', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS043','JohnFord@mail.com', '123456', 'John', 'Ford', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar.jpg','590 CMT8, Q3, HCM'),
('CS044','PaulFulton@mail.com', '123456', 'Paul', 'Fulton', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS045','DonFunk@mail.com', '123456', 'Don', 'Funk', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS046','BobGage@mail.com', '123456', 'Bob', 'Gage', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar.jpg','590 CMT8, Q3, HCM'),
('CS047','AldeenGallagher@mail.com', '123456', 'Aldeen', 'Gallagher', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar3.jpg','590 CMT8, Q3, HCM'),
('CS048','MichaelGalos@mail.com', '123456', 'Michael', 'Galos', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar2.jpg','590 CMT8, Q3, HCM'),
('CS049','JonGanio@mail.com', '123456', 'Jon', 'Ganio', 1, '(096) 737-4508', 'content/img/Avatars/default-avatar1.jpg','590 CMT8, Q3, HCM'),
('CS050','KathleenGarza@mail.com', '123456', 'Kathleen', 'Garza', 0, '(096) 737-4508', 'content/img/Avatars/default-avatar5.jpg','590 CMT8, Q3, HCM')
GO

INSERT Brands(BrandID, BrandName, BrandImages, Descriptions) VALUES
('BR001','Dell', 'content/img/Brands/dell.png', 'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use Dell in reference to client solutions and corporate functions.'),
('BR002','HP', 'content/img/Brands/hp.png', 'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use Dell in reference to client solutions and corporate functions.'),
('BR003','Asus', 'content/img/Brands/asus.png', 'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use Dell in reference to client solutions and corporate functions.'),
('BR004','Apple', 'content/img/Brands/apple.png', 'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use Dell in reference to client solutions and corporate functions.'),
('BR005','Sony', 'content/img/Brands/sony.png', 'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use Dell in reference to client solutions and corporate functions.'),
('BR006','MSI', 'content/img/Brands/msi.png', 'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use Dell in reference to client solutions and corporate functions.'),
('BR007','Lenovo', 'content/img/Brands/lenovo.png', 'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use Dell in reference to client solutions and corporate functions.'),
('BR008','Acer', 'content/img/Brands/acer.png', 'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use Dell in reference to client solutions and corporate functions.')
GO

INSERT Categories(CategoryID, CategoryName, CategoryImage) VALUES
('CA001','Laptops','content/img/Categories/laptops.png'),
('CA002','Headphones','content/img/Categories/headphones.png'),
('CA003','Keyboards','content/img/Categories/keyboards.png'),
('CA004','Mouses','content/img/Categories/mouses.png')
GO

INSERT Products(ProductID, ProductName, Price,DiscountProduct, CategoryID, BrandID, Descriptions, Quantity, Feature, Image1, Image2, Image3, Image4) VALUES
('PR023', 'Laptop MSI', 1100,0,'CA001', 'BR006','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/MSI-1.jpg', 'content/img/Products/MSI-2.jpg','content/img/Products/MSI-3.jpg', 'content/img/Products/MSI -4.jpg'),
('PR024', 'Laptop Lenovo Thinkpad', 1100,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/LenovoThinkpad-1.jpg', 'content/img/Products/LenovoThinkpad-2.jpg','content/img/Products/LenovoThinkpad-3.jpg', 'content/img/Products/LenovoThinkpad-4.jpg'),
('PR025', 'Laptop Lenovo pad', 700,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/Lenovo-1.png', 'content/img/Products/Lenovo-2.png','content/img/Products/Lenovo-3.png', 'content/img/Products/Lenovo-4.png'),
('PR026', 'Lenovo Ideapad', 300,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/Lenovo Ideapad-1.jpg', 'content/img/Products/Lenovo Ideapad-2.jpg','content/img/Products/Lenovo Ideapad-3.jpg', 'content/img/Products/Lenovo Ideapad-4.jpg'),
('PR027', 'Lenovo Legion', 1300,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/lnvLegion.jpg', null,null, null),
('PR028', 'Lenovo IdeaPad320', 850,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/lnvIdeaPad320.jpg', null,null, null),
('PR029', 'Lenovo IdeaPad330', 725,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/lnvIdeaPad330.jpg', null,null, null),
('PR030', 'Lenovo Yoga520', 580,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/lnvYoga520.jpg', null,null, null),
('PR031', 'Lenovo IdeaPad330s', 550,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'New','content/img/Products/lnvIdeaPad330s.jpg', null,null, null),
('PR032', 'Lenovo IdeaPad130', 450,0,'CA001', 'BR007','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/lnvIdeaPad130.jpg', null,null, null),
('PR033', 'DellVostro', 725,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/DellVostro1.png', null,null, null),
('PR034', 'Dell5570A', 720,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/Dell5570A-1.png', 'content/img/Products/Dell5570A-2.png', 'content/img/Products/Dell5570A-3.png', 'content/img/Products/Dell5570A-4.png'),
('PR035', 'Dell7370', 975,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'Hot','content/img/Products/Dell7370-1.png', 'content/img/Products/Dell7370-2.png', 'content/img/Products/Dell7370-3.png', 'content/img/Products/Dell7370-4.png'),
('PR036', 'Dell7570', 1350,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'New','content/img/Products/Dell7570.jpg',null,null,null),
('PR037', 'Dell7373', 1350,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'Hot','content/img/Products/Dell7373.jpg',null,null,null),
('PR038', 'Dell5379', 1250,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'Hot','content/img/Products/Dell5379.jpg',null,null,null),
('PR039', 'Dell3578', 1000,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/Dell3578.jpg',null,null,null),
('PR040', 'DellVostro5568', 800,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/DellVostro5568.png',null,null,null),
('PR041', 'Dell3576', 650,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/Dell3576.png',null,null,null),
('PR042', 'Dell3567', 600,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/Dell3567.png',null,null,null),
('PR043', 'Dell3476', 600,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/Dell3476.png',null,null,null),
('PR044', 'DellVostro3568', 590,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/DellVostro3568.png',null,null,null),
('PR045', 'DellVostro3468', 595,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/DellVostro3468.jpg',null,null,null),
('PR046', 'DellVostro3368', 500,0,'CA001', 'BR001','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/DellVostro3368.png',null,null,null),
('PR048', 'Asus vivo406', 485,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asvivo406-1.png', 'content/img/Products/asvivo406-2.png', 'content/img/Products/asvivo406-3.png', 'content/img/Products/asvivo406-4.png'),
('PR049', 'Asus x541', 450,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asx541-1.jpg', 'content/img/Products/asx541-2.jpg', 'content/img/Products/asx541-3.jpg', 'content/img/Products/asx541-4.jpg'),
('PR050', 'Asus zenbook', 2050,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/aszenbook.jpg',null ,null,null ),
('PR051', 'Asus zenbook430', 1050,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/aszenbook430.jpg',null ,null,null ),
('PR052', 'Asus vivo410UA', 990,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asvivo410UA.jpg',null ,null,null ),
('PR053', 'Asus vivo510UQ', 850,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asvivo510UQ.jpg',null ,null,null ),
('PR054', 'Asus vivo510UAi5', 800,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asvivo510UAi5.jpg',null ,null,null ),
('PR055', 'Asus vivo410UAi5', 750,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asvivo410UAi5.jpg',null ,null,null ),
('PR056', 'Asus S510Uai3', 650,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asS510Uai3.jpg',null ,null,null ),
('PR057', 'Asus vivotp410UAi3', 670,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asvivotp410UAi3.jpg',null ,null,null ),
('PR058', 'Asus vivos410i3', 645,0,'CA001', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/asvivos410i3.jpg',null ,null,null ),
('PR059', 'MacBook-MNYL2ZP-A ', 1800,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'Hot','content/img/Products/MacBook12-MNYL2ZP-A-1.jpg', 'content/img/Products/MacBook12-MNYL2ZP-A-2.jpg', 'content/img/Products/MacBook12-MNYL2ZP-A-3.jpg', 'content/img/Products/MacBook12-MNYL2ZP-A-4.jpg'),
('PR060', 'MacBook-MPXV2', 2250,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/MacBook13MPXV2-1.jpg', 'content/img/Products/MacBook13MPXV2-2.jpg', 'content/img/Products/MacBook13MPXV2-3.jpg',null),
('PR061', 'MacBook-MQD32LL', 1150,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/MacBookAirMQD32LL.jpg', null, null,null),
('PR062', 'MacBookAir-MPXT2LL', 1850,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'Hot','content/img/Products/MacBook13MPXT2LL.jpg', null, null,null),
('PR063', 'MacBook-MPTT2', 3000,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'New','content/img/Products/MacBook15MPTT2-1.png', 'content/img/Products/MacBook15MPTT2-2.jpg', 'content/img/Products/MacBook15MPTT2-3.jpg',null),
('PR064', 'MacBook-MPXQ2', 1550,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/MacBook13MPXQ2-2.jpg', 'content/img/Products/MacBook13MPXQ2-3.jpg', null,null),
('PR065', 'MacBook-MPXU2', 1950,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'New','content/img/Products/MacBook13MPXU2-1.jpg', 'content/img/Products/MacBook13MPXU2-2.jpg', 'content/img/Products/MacBook13MPXU2-3.jpg',null),
('PR066', 'MacBook-MNYN2', 1915,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/MacBook12MNYN2-1.png','content/img/Products/MacBook12MNYN2-2.jpg','content/img/Products/MacBook12MNYN2-3.jpg',null),
('PR067', 'MacBook-MPXW2', 2550,0,'CA001', 'BR004','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'New','content/img/Products/MacBook13MPXW2-1.jpg', 'content/img/Products/MacBook13MPXW2-2.jpg', 'content/img/Products/MacBook13MPXW2-3.jpg',null),
('PR068', 'HP Envy 13 ah0025 TU', 2550,3,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'Hot','content/img/Products/HPEnvy13ah0025TU-1.png', 'content/img/Products/HPEnvy13ah0025TU-2.jpg', 'content/img/Products/HPEnvy13ah0025TU-3.jpg',null),
('PR069', 'HP Pavilion Gaming 15', 1250,0,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'New','content/img/Products/HPPavilionGaming15-cx0179TX-1.jpg', 'content/img/Products/HPPavilionGaming15-cx0179TX-2.jpg', 'content/img/Products/HPPavilionGaming15-cx0179TX-3.jpg',null),
('PR070', 'HP 14-cK0066 TU', 350,0,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/HP14-cK0066TU-1.jpg', 'content/img/Products/HP14-cK0066TU-2.jpg','content/img/Products/HP14-cK0066TU-3.jpg',null),
('PR071', 'HP Pavilion 14', 750,0,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/HPPavilion14-ce0023TU-1.jpg', 'content/img/Products/HPPavilion14-ce0023TU-2.jpg', 'content/img/Products/HPPavilion14-ce0023TU-3.jpg',null),
('PR072', 'HP Envy 13-ah0027 TU', 1300,5,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/HPEnvy13-ah0027TU-1.jpg','content/img/Products/HPEnvy13-ah0027TU-2.png', 'content/img/Products/HPEnvy13-ah0027TU-3.jpg',null),
('PR073', 'HP 15-da0051 TU', 495,4,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/HP15-da0051TU-1.jpg', 'content/img/Products/HP15-da0051TU-2.jpg','content/img/Products/HP15-da0051TU-3.jpg',null),
('PR074', 'HP Probook 430G5',1100,0,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/HPProbook430G5-2XR79PA-1.png','content/img/Products/HPProbook430G5-2XR79PA-2.jpg','content/img/Products/HPProbook430G5-2XR79PA-3.jpg',null),
('PR075', 'HP Pavilion 14',750,2,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/HPPavilion14-ce0024TU-1.jpg', 'content/img/Products/HPPavilion14-ce0024TU-2.jpg', 'content/img/Products/HPPavilion14-ce0024TU-3.jpg',null),
('PR076', 'HP Pavilion X36014',650,0,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/HPPavilionX36014-cd0082TU-1.jpg', 'content/img/Products/HPPavilionX36014-cd0082TU-2.jpg', 'content/img/Products/HPPavilionX36014-cd0082TU-3.jpg',null),
('PR077', 'HP Pavilion15-cc058TX',925,0,'CA001', 'BR002','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/HPPavilion15-cc058TX-1.jpg', 'content/img/Products/HPPavilion15-cc058TX-2.jpg', 'content/img/Products/HPPavilion15-cc058TX-3.jpg',null),
('PR078', 'Asus Cerberus',50,0,'CA004', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/Asus Cerberus.jpg', null, null,null),
('PR079', 'Asus Strix Wireless',85,0,'CA004', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/AsusStrixWireless.jpg', null, null,null),
('PR080', 'Asus Strix Wireless',25,0,'CA003', 'BR003','A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ',10,'--','content/img/Products/CameraQuestekWinQB.png', null, null,null)
GO

INSERT ProductsDetails(ProductID, CPU, Memory, VGA, HDD, Camera, Display, Battery, Weights) VALUES
('PR023','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR024','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR025','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR026','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR027','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR028','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR029','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR030','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR031','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR032','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR033','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR034','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR035','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR036','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR037','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR038','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR039','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR040','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR041','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR042','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR043','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR044','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR045','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR046','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR048','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR049','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR050','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR051','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR052','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR053','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR054','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR055','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR056','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR057','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR058','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR059','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR060','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR061','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR062','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR063','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR064','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR065','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR066','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR067','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR068','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR069','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR070','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR071','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR072','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR073','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR074','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR075','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR076','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg'),
('PR077','Core i3-8130U 2.20Ghz','4GB DDR4 Bus 2400Mhz','Intel UHD 620M','1 TB SATA3 + SSD M.2 PCIe','2MP','15.6 inch backlit FHD(1920x1080) IPS','3 Cells 42Whrs','1.6 Kg')
GO

INSERT Orders(OrderID, CustomerID, Total, ShipName,ShipPhone,ShipAddress,ShipDate,ShipNote,OrderDate,PaymentMethod,ProcessStatus,IsStatus)
VALUES ('O001','CS001','15000', 'Thang Nguyen','(096) 737-4508','590 CMT8, Q3, HCM','2018-10-10','n/a','2018-10-11','Cash','Processing','1'),
('O002','CS002','13000', 'Duy Khac','(096) 737-4508','590 CMT8, Q3, HCM','2018-11-10','n/a','2018-11-11','Cash','Shipping','1'),
('O003','CS003','19000', 'Brian Welcker','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-10','n/a','2018-12-11','Cash','Completed','1'),
('O004','CS004','15000', 'Tete Mensa-Annan','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-14','n/a','2018-12-15','Cash','Processing','1'),
('O005','CS005','13000', 'Syed Abbas','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-17','n/a','2018-12-18','Cash','Shipping','1'),
('O006','CS006','19000', 'Tuan Tran','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-20','n/a','2018-12-21','Cash','Completed','1'),
('O007','CS007','15000', 'Nam Nguyen','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-22','n/a','2018-12-23','Cash','Processing','1'),
('O008','CS008','13000', 'Ronald Adina','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-22','n/a','2018-12-24','Cash','Shipping','1'),
('O009','CS009','19000', 'Samuel Agcaoili','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-23','n/a','2018-12-25','Cash','Completed','1'),
('O010','CS010','15000', 'James Aguilar','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-25','n/a','2018-12-26','Cash','Processing','1'),
('O011','CS011','13000', 'Gail Erickson','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-26','n/a','2018-12-27','Cash','Shipping','1'),
('O012','CS012','19000', 'Janice Galvin','(096) 737-4508','590 CMT8, Q3, HCM','2018-12-27','n/a','2018-12-28','Cash','Completed','1')
GO

INSERT OrdersDetails(OrderID, ProductID, SellingPrice, Quantity) VALUES
('O001', 'PR023', 1100, 4),
('O001', 'PR034', 720, 4),
('O001', 'PR041', 650, 4),

('O002', 'PR024', 1100, 4),
('O002', 'PR035', 975, 4),
('O002', 'PR042', 600, 4),

('O003', 'PR025', 700, 4),
('O003', 'PR036', 1350, 4),
('O003', 'PR043', 600, 4),

('O004', 'PR026', 300, 4),
('O004', 'PR037', 1350, 4),
('O004', 'PR044', 600, 4),

('O005', 'PR027', 1300, 4),
('O005', 'PR038', 1250, 4),
('O005', 'PR045', 595, 4),

('O006', 'PR028', 850, 4),
('O006', 'PR039', 1000, 4),
('O006', 'PR046', 500, 4),

('O007', 'PR023', 1100, 4),
('O007', 'PR029', 725, 4),
('O007', 'PR048', 485, 4),

('O008', 'PR025', 700, 4),
('O008', 'PR030', 580, 4),
('O008', 'PR049', 450, 4),

('O009', 'PR027', 1300, 4),
('O009', 'PR031', 550, 4),
('O009', 'PR050', 2050, 4),

('O010', 'PR028', 850, 4),
('O010', 'PR032', 450, 4),
('O010', 'PR051', 1050, 4),

('O011', 'PR029', 725, 4),
('O011', 'PR033', 725, 4),
('O011', 'PR052', 990, 4),

('O012', 'PR030', 580, 4),
('O012', 'PR034', 720, 4),
('O012', 'PR053', 850, 4)
GO

INSERT Ratings(ProductID,CustomerID,Rate) 
VALUES('PR023','CS001',4),
('PR024','CS002',4),
('PR025','CS003',4),
('PR026','CS004',4),
('PR028','CS006',4),
('PR027','CS005',4),
('PR029','CS007',4),
('PR030','CS008',4),
('PR031','CS009',4),
('PR032','CS010',4),
('PR023','CS011',2),
('PR024','CS012',2),
('PR025','CS013',2),
('PR026','CS014',2),
('PR027','CS015',2),
('PR028','CS016',2),
('PR029','CS017',2),
('PR030','CS018',2),
('PR031','CS019',2),
('PR032','CS020',2),
('PR023','CS021',3),
('PR024','CS022',3),
('PR025','CS023',3),
('PR026','CS024',3),
('PR027','CS025',3),
('PR028','CS026',3),
('PR029','CS027',3),
('PR030','CS028',3),
('PR031','CS029',3),
('PR032','CS030',3),
('PR023','CS031',5),
('PR024','CS032',5),
('PR025','CS033',5),
('PR026','CS034',5),
('PR027','CS035',5),
('PR028','CS036',5),
('PR029','CS037',5),
('PR030','CS038',5),
('PR031','CS039',5),
('PR032','CS040',5)
GO

INSERT INTO Comments(ProductID,CustomerID,Content,Respond,IsDisplay) VALUES 
('PR023','CS001','This is Very Good','Thankyou',1),
('PR024','CS002','Nice','Thankyou',1),
('PR025','CS003','I Like It','Thankyou',1),
('PR026','CS004','Good','Thankyou',1),
('PR027','CS005','Very Nice','Thankyou',1),
('PR028','CS006','Good For me','Thankyou',1),

('PR023','CS007','This is Very Good','Thankyou',1),
('PR024','CS008','Nice','Thankyou',1),
('PR025','CS009','I Like It','Thankyou',1),
('PR026','CS010','Good','Thankyou',1),
('PR027','CS011','Very Nice','Thankyou',1),
('PR028','CS012','Good For me','Thankyou',1),

('PR029','CS001','This is Very Good','Thankyou',1),
('PR030','CS002','Nice','Thankyou',1),
('PR031','CS003','I Like It','Thankyou',1),
('PR032','CS004','Good','Thankyou',1),
('PR033','CS005','Very Nice','Thankyou',1),
('PR034','CS006','Good For me','Thankyou',1),

('PR035','CS013','This is Very Good','Thankyou',1),
('PR036','CS014','Nice','Thankyou',1),
('PR037','CS015','I Like It','Thankyou',1),
('PR038','CS016','Good','Thankyou',1),
('PR039','CS017','Very Nice','Thankyou',1),
('PR040','CS018','Good For me','Thankyou',1)
GO

INSERT INTO WishAndCart(CustomerID,ProductID,IsCart) VALUES
('CS001','PR023',1),
('CS002','PR024',2),
('CS003','PR025',3),
('CS004','PR026',4),
('CS005','PR027',5),
('CS006','PR028',6),

('CS007','PR029',7),
('CS008','PR030',8),
('CS009','PR031',9),
('CS010','PR032',10),
('CS011','PR033',9),
('CS012','PR034',8),

('CS013','PR035',1),
('CS014','PR036',2),
('CS015','PR037',3),
('CS016','PR038',4),
('CS017','PR039',5),
('CS018','PR040',6),

('CS019','PR041',7),
('CS020','PR042',8),
('CS021','PR043',9),
('CS022','PR044',10),
('CS023','PR045',9),
('CS024','PR046',8)
GO

INSERT Products(ProductID, ProductName, Price,DiscountProduct,CategoryID,
BrandID, Descriptions, Quantity, Feature, Image1, Image2, Image3, Image4)
VALUES
--Dell-----Mouse
('PR081','Mouse Dell', 1000,0,'CA004','BR001','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/dell/m1.jpg',
'content/img/Products/dell/m1.jpg',
'content/img/Products/dell/m1.jpg',
'content/img/Products/dell/m1.jpg'),

('PR082','Mouse Dell', 1200,0,'CA004','BR001','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/dell/m2.jpg',
'content/img/Products/dell/m2.jpg',
'content/img/Products/dell/m2.jpg',
'content/img/Products/dell/m2.jpg'),

('PR083','Mouse Dell', 1000,0,'CA004','BR001','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/dell/m3.jpg',
'content/img/Products/dell/m3.jpg',
'content/img/Products/dell/m3.jpg',
'content/img/Products/dell/m3.jpg'),

('PR084','Mouse Dell', 1300,0,'CA004','BR001','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/dell/m4.jpg',
'content/img/Products/dell/m4.jpg',
'content/img/Products/dell/m4.jpg',
'content/img/Products/dell/m4.jpg'),
--Acer-----Keyboard
('PR085','Keyboard Acer', 1200,0,'CA003','BR008','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/acer/k1.jpg',
'content/img/Products/acer/k1.jpg',
'content/img/Products/acer/k1.jpg',
'content/img/Products/acer/k1.jpg'),

('PR086','Keyboard Acer', 1200,0,'CA003','BR008','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/acer/k2.jpg',
'content/img/Products/acer/k2.jpg',
'content/img/Products/acer/k2.jpg',
'content/img/Products/acer/k2.jpg'),

('PR087','Keyboard Acer', 1200,0,'CA003','BR008','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/acer/k3.jpg',
'content/img/Products/acer/k3.jpg',
'content/img/Products/acer/k3.jpg',
'content/img/Products/acer/k3.jpg'),
--Acer-----Mouse
('PR088','Mouse Acer', 1200,0,'CA004','BR008','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/acer/m1.jpg',
'content/img/Products/acer/m1.jpg',
'content/img/Products/acer/m1.jpg',
'content/img/Products/acer/m1.jpg'),

('PR089','Mouse Acer', 1200,0,'CA004','BR008','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/acer/m2.jpg',
'content/img/Products/acer/m2.jpg',
'content/img/Products/acer/m2.jpg',
'content/img/Products/acer/m2.jpg'),

('PR090','Mouse Acer', 1200,0,'CA004','BR008','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/acer/m3.jpg',
'content/img/Products/acer/m3.jpg',
'content/img/Products/acer/m3.jpg',
'content/img/Products/acer/m3.jpg'),
--Apple-----Mouse
('PR091','Mouse Apple', 1200,0,'CA004','BR004','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/apple/m1.jpg',
'content/img/Products/apple/m1.jpg',
'content/img/Products/apple/m1.jpg',
'content/img/Products/apple/m1.jpg'),

('PR092','Mouse Apple', 1200,0,'CA004','BR004','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/apple/m2.jpg',
'content/img/Products/apple/m2.jpg',
'content/img/Products/apple/m2.jpg',
'content/img/Products/apple/m2.jpg'),
--Apple-----Keyboard
('PR093','Keyboard Apple', 1200,0,'CA003','BR004','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/apple/k2.jpg',
'content/img/Products/apple/k2.jpg',
'content/img/Products/apple/k2.jpg',
'content/img/Products/apple/k2.jpg'),

('PR094','Keyboard Apple', 1200,0,'CA003','BR004','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/apple/k1.jpg',
'content/img/Products/apple/k1.jpg',
'content/img/Products/apple/k1.jpg',
'content/img/Products/apple/k1.jpg'),

('PR095','Keyboard Apple', 1200,0,'CA003','BR004','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/apple/k3.jpg',
'content/img/Products/apple/k3.jpg',
'content/img/Products/apple/k3.jpg',
'content/img/Products/apple/k3.jpg'),
--Asus-----phone
('PR096','Headphone Asus', 1200,0,'CA002','BR003','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/asus/1.jpg',
'content/img/Products/asus/1.jpg',
'content/img/Products/asus/1.jpg',
'content/img/Products/asus/1.jpg'),

('PR097','Headphone Asus', 1200,0,'CA002','BR003','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/asus/2.jpg',
'content/img/Products/asus/2.jpg',
'content/img/Products/asus/2.jpg',
'content/img/Products/asus/2.jpg'),

('PR098','Headphone Asus', 1200,0,'CA002','BR003','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/asus/3.jpg',
'content/img/Products/asus/3.jpg',
'content/img/Products/asus/3.jpg',
'content/img/Products/asus/3.jpg'),

('PR099','Headphone Asus', 1200,0,'CA002','BR003','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/asus/4.jpg',
'content/img/Products/asus/4.jpg',
'content/img/Products/asus/4.jpg',
'content/img/Products/asus/4.jpg'),

('PR100','Headphone Asus', 1200,0,'CA002','BR003','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/asus/5.jpg',
'content/img/Products/asus/5.jpg',
'content/img/Products/asus/5.jpg',
'content/img/Products/asus/5.jpg'),
--Asus-----Keyboard
('PR101','Keyboard Asus', 1200,0,'CA003','BR003','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/asus/k2.jpg',
'content/img/Products/asus/k2.jpg',
'content/img/Products/asus/k2.jpg',
'content/img/Products/asus/k2.jpg'),

('PR102','Keyboard Asus', 1200,0,'CA003','BR003','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/asus/k1.jpg',
'content/img/Products/asus/k1.jpg',
'content/img/Products/asus/k1.jpg',
'content/img/Products/asus/k1.jpg'),

('PR103','Keyboard Asus', 1200,0,'CA003','BR003','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/asus/k3.jpg',
'content/img/Products/asus/k3.jpg',
'content/img/Products/asus/k3.jpg',
'content/img/Products/asus/k3.jpg'),
--Asus-----Mouse
('PR104','Mouse Asus', 1200,0,'CA004','BR003','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/asus/m1.jpg',
'content/img/Products/asus/m1.jpg',
'content/img/Products/asus/m1.jpg',
'content/img/Products/asus/m1.jpg'),

('PR105','Mouse Asus', 1200,0,'CA004','BR003','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/asus/m2.jpg',
'content/img/Products/asus/m2.jpg',
'content/img/Products/asus/m2.jpg',
'content/img/Products/asus/m2.jpg'),

('PR106','Mouse Asus', 1200,0,'CA004','BR003','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/asus/m3.jpg',
'content/img/Products/asus/m3.jpg',
'content/img/Products/asus/m3.jpg',
'content/img/Products/asus/m3.jpg'),

('PR107','Mouse Asus', 1200,0,'CA004','BR003','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/asus/m4.jpg',
'content/img/Products/asus/m4.jpg',
'content/img/Products/asus/m4.jpg',
'content/img/Products/asus/m4.jpg'),
--HP-----Mouse
('PR108','Mouse HP', 1200,0,'CA004','BR002','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/HP/m1.jpg',
'content/img/Products/HP/m1.jpg',
'content/img/Products/HP/m1.jpg',
'content/img/Products/HP/m1.jpg'),

('PR109','Mouse HP', 1200,0,'CA004','BR002','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/HP/m2.jpg',
'content/img/Products/HP/m2.jpg',
'content/img/Products/HP/m2.jpg',
'content/img/Products/HP/m2.jpg'),

('PR110','Mouse HP', 1200,0,'CA004','BR002','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/HP/m3.jpg',
'content/img/Products/HP/m3.jpg',
'content/img/Products/HP/m3.jpg',
'content/img/Products/HP/m3.jpg'),

('PR111','Mouse HP', 1200,0,'CA004','BR002','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/HP/m4.jpg',
'content/img/Products/HP/m4.jpg',
'content/img/Products/HP/m4.jpg',
'content/img/Products/HP/m4.jpg'),

('PR112','Mouse HP', 1200,0,'CA004','BR002','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/HP/m5.jpg',
'content/img/Products/HP/m5.jpg',
'content/img/Products/HP/m5.jpg',
'content/img/Products/HP/m5.jpg'),
--HP-----Headphone
('PR113','Headphone HP', 1200,0,'CA002','BR002','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/HP/1.jpg',
'content/img/Products/HP/1.jpg',
'content/img/Products/HP/1.jpg',
'content/img/Products/HP/1.jpg'),

('PR114','Headphone HP', 1200,0,'CA002','BR002','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/HP/2.jpg',
'content/img/Products/HP/2.jpg',
'content/img/Products/HP/2.jpg',
'content/img/Products/HP/2.jpg'),

('PR115','Headphone HP', 1200,0,'CA002','BR002','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/HP/3.jpg',
'content/img/Products/HP/3.jpg',
'content/img/Products/HP/3.jpg',
'content/img/Products/HP/3.jpg'),

('PR116','Headphone HP', 1200,0,'CA002','BR002','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/HP/4.jpg',
'content/img/Products/HP/4.jpg',
'content/img/Products/HP/4.jpg',
'content/img/Products/HP/4.jpg'),
--HP-----Keyboard

('PR117','Keyboard HP', 1200,0,'CA003','BR002','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/HP/k2.jpg',
'content/img/Products/HP/k2.jpg',
'content/img/Products/HP/k2.jpg',
'content/img/Products/HP/k2.jpg'),

('PR118','Keyboard HP', 1200,0,'CA003','BR002','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/HP/k1.jpg',
'content/img/Products/HP/k1.jpg',
'content/img/Products/HP/k1.jpg',
'content/img/Products/HP/k1.jpg'),

('PR119','Keyboard HP', 1200,0,'CA003','BR002','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/HP/k3.jpg',
'content/img/Products/HP/k3.jpg',
'content/img/Products/HP/k3.jpg',
'content/img/Products/HP/k3.jpg'),

--Lenovo-----Keyboard

('PR120','Keyboard lenovo', 1200,0,'CA003','BR007','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/lenovo/k2.jpg',
'content/img/Products/lenovo/k2.jpg',
'content/img/Products/lenovo/k2.jpg',
'content/img/Products/lenovo/k2.jpg'),

('PR121','Keyboard lenovo', 1200,0,'CA003','BR007','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/lenovo/k1.jpg',
'content/img/Products/lenovo/k1.jpg',
'content/img/Products/lenovo/k1.jpg',
'content/img/Products/lenovo/k1.jpg'),

('PR122','Keyboard lenovo', 1200,0,'CA003','BR007','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/lenovo/k3.jpg',
'content/img/Products/lenovo/k3.jpg',
'content/img/Products/lenovo/k3.jpg',
'content/img/Products/lenovo/k3.jpg'),

--Lenovo-----Phone

('PR123','Headphone lenovo', 1200,0,'CA002','BR007','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/lenovo/1.jpg',
'content/img/Products/lenovo/1.jpg',
'content/img/Products/lenovo/1.jpg',
'content/img/Products/lenovo/1.jpg'),

('PR124','Headphone lenovo', 1200,0,'CA002','BR007','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/lenovo/2.jpg',
'content/img/Products/lenovo/2.jpg',
'content/img/Products/lenovo/2.jpg',
'content/img/Products/lenovo/2.jpg'),

('PR125','Headphone lenovo', 1200,0,'CA002','BR007','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/lenovo/3.jpg',
'content/img/Products/lenovo/3.jpg',
'content/img/Products/lenovo/3.jpg',
'content/img/Products/lenovo/3.jpg'),

--Lenovo-----Mouse
('PR126','Mouse lenovo', 1200,0,'CA004','BR007','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/lenovo/m1.jpg',
'content/img/Products/lenovo/m1.jpg',
'content/img/Products/lenovo/m1.jpg',
'content/img/Products/lenovo/m1.jpg'),

('PR127','Mouse lenovo', 1200,0,'CA004','BR007','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/lenovo/m2.jpg',
'content/img/Products/lenovo/m2.jpg',
'content/img/Products/lenovo/m2.jpg',
'content/img/Products/lenovo/m2.jpg'),

('PR128','Mouse lenovo', 1200,0,'CA004','BR007','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/lenovo/m3.jpg',
'content/img/Products/lenovo/m3.jpg',
'content/img/Products/lenovo/m3.jpg',
'content/img/Products/lenovo/m3.jpg'),

('PR129','Mouse lenovo', 1200,0,'CA004','BR007','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/lenovo/m4.jpg',
'content/img/Products/lenovo/m4.jpg',
'content/img/Products/lenovo/m4.jpg',
'content/img/Products/lenovo/m4.jpg'),


--MSI-----Mouse
('PR130','Mouse MSI', 1200,0,'CA004','BR006','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/MSI/m1.jpg',
'content/img/Products/MSI/m1.jpg',
'content/img/Products/MSI/m1.jpg',
'content/img/Products/MSI/m1.jpg'),

('PR131','Mouse MSI', 1200,0,'CA004','BR006','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/MSI/m2.jpg',
'content/img/Products/MSI/m2.jpg',
'content/img/Products/MSI/m2.jpg',
'content/img/Products/MSI/m2.jpg'),

('PR132','Mouse MSI', 1200,0,'CA004','BR006','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/MSI/m3.jpg',
'content/img/Products/MSI/m3.jpg',
'content/img/Products/MSI/m3.jpg',
'content/img/Products/MSI/m3.jpg'),

--MSI-----Phone

('PR133','Headphone MSI', 1200,0,'CA002','BR006','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/MSI/1.jpg',
'content/img/Products/MSI/1.jpg',
'content/img/Products/MSI/1.jpg',
'content/img/Products/MSI/1.jpg'),

('PR134','Headphone MSI', 1200,0,'CA002','BR006','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/MSI/2.jpg',
'content/img/Products/MSI/2.jpg',
'content/img/Products/MSI/2.jpg',
'content/img/Products/MSI/2.jpg'),

('PR135','Headphone MSI', 1200,0,'CA002','BR006','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/MSI/3.jpg',
'content/img/Products/MSI/3.jpg',
'content/img/Products/MSI/3.jpg',
'content/img/Products/MSI/3.jpg'),

--MSI-----Keyboard

('PR136','Keyboard MSI', 1200,0,'CA003','BR006','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/MSI/k2.jpg',
'content/img/Products/MSI/k2.jpg',
'content/img/Products/MSI/k2.jpg',
'content/img/Products/MSI/k2.jpg'),

('PR137','Keyboard MSI', 1200,0,'CA003','BR006','A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.',10,'--',
'content/img/Products/MSI/k1.jpg',
'content/img/Products/MSI/k1.jpg',
'content/img/Products/MSI/k1.jpg',
'content/img/Products/MSI/k1.jpg'),

--Sony-----Phone

('PR138','Headphone Sony', 1200,0,'CA002','BR005','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/Sony/1.jpg',
'content/img/Products/Sony/1.jpg',
'content/img/Products/Sony/1.jpg',
'content/img/Products/Sony/1.jpg'),

('PR139','Headphone Sony', 1200,0,'CA002','BR005','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/Sony/2.jpg',
'content/img/Products/Sony/2.jpg',
'content/img/Products/Sony/2.jpg',
'content/img/Products/Sony/2.jpg'),

('PR140','Headphone Sony', 1200,0,'CA002','BR005','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/Sony/3.jpg',
'content/img/Products/Sony/3.jpg',
'content/img/Products/Sony/3.jpg',
'content/img/Products/Sony/3.jpg'),

('PR141','Headphone Sony', 1200,0,'CA002','BR005','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/Sony/4.jpg',
'content/img/Products/Sony/4.jpg',
'content/img/Products/Sony/4.jpg',
'content/img/Products/Sony/4.jpg'),

('PR142','Headphone Sony', 1200,0,'CA002','BR005','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/Sony/5.jpg',
'content/img/Products/Sony/5.jpg',
'content/img/Products/Sony/5.jpg',
'content/img/Products/Sony/5.jpg'),

('PR143','Headphone Sony', 1200,0,'CA002','BR005','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/Sony/6.jpg',
'content/img/Products/Sony/6.jpg',
'content/img/Products/Sony/6.jpg',
'content/img/Products/Sony/6.jpg'),

('PR145','Headphone Sony', 1200,0,'CA002','BR005','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/Sony/7.jpg',
'content/img/Products/Sony/7.jpg',
'content/img/Products/Sony/7.jpg',
'content/img/Products/Sony/7.jpg'),

('PR146','Headphone Sony', 1200,0,'CA002','BR005','They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.',10,'--',
'content/img/Products/Sony/8.jpg',
'content/img/Products/Sony/8.jpg',
'content/img/Products/Sony/8.jpg',
'content/img/Products/Sony/8.jpg'),

--Sony-----Mouse
('PR147','Mouse Sony', 1200,0,'CA004','BR005','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/MSonySI/m1.jpg',
'content/img/Products/Sony/m1.jpg',
'content/img/Products/Sony/m1.jpg',
'content/img/Products/Sony/m1.jpg'),

('PR148','Mouse Sony', 1200,0,'CA004','BR005','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/Sony/m2.jpg',
'content/img/Products/Sony/m2.jpg',
'content/img/Products/Sony/m2.jpg',
'content/img/Products/Sony/m2.jpg'),

('PR149','Mouse Sony', 1200,0,'CA004','BR005','It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.',10,'--',
'content/img/Products/Sony/m3.jpg',
'content/img/Products/Sony/m3.jpg',
'content/img/Products/Sony/m3.jpg',
'content/img/Products/Sony/m3.jpg')
GO

INSERT ProductsDetails(ProductID, Weights) VALUES
('PR081','1.2 Kg'),('PR082','1.2 Kg'),('PR083','1.2 Kg'),('PR084','1.2 Kg'),('PR085','1.2 Kg'),('PR086','1.2 Kg'),
('PR087','1.2 Kg'),('PR088','1.2 Kg'),('PR089','1.2 Kg'),('PR090','1.2 Kg'),('PR091','1.2 Kg'),('PR092','1.2 Kg'),
('PR093','1.2 Kg'),('PR094','1.2 Kg'),('PR095','1.2 Kg'),('PR096','1.2 Kg'),('PR097','1.2 Kg'),('PR098','1.2 Kg'),
('PR099','1.2 Kg'),('PR100','1.2 Kg'),('PR101','1.2 Kg'),('PR102','1.2 Kg'),('PR103','1.2 Kg'),('PR104','1.2 Kg'),
('PR105','1.2 Kg'),('PR106','1.2 Kg'),('PR107','1.2 Kg'),('PR108','1.2 Kg'),('PR109','1.2 Kg'),('PR110','1.2 Kg'),
('PR111','1.2 Kg'),('PR112','1.2 Kg'),('PR113','1.2 Kg'),('PR114','1.2 Kg'),('PR115','1.2 Kg'),('PR116','1.2 Kg'),
('PR117','1.2 Kg'),('PR118','1.2 Kg'),('PR119','1.2 Kg'),('PR120','1.2 Kg'),('PR121','1.2 Kg'),('PR122','1.2 Kg'),
('PR123','1.2 Kg'),('PR124','1.2 Kg'),('PR125','1.2 Kg'),('PR126','1.2 Kg'),('PR127','1.2 Kg'),('PR128','1.2 Kg'),
('PR129','1.2 Kg'),('PR130','1.2 Kg'),('PR131','1.2 Kg'),('PR132','1.2 Kg'),('PR133','1.2 Kg'),('PR134','1.2 Kg'),
('PR135','1.2 Kg'),('PR136','1.2 Kg'),('PR137','1.2 Kg'),('PR138','1.2 Kg'),('PR139','1.2 Kg'),('PR140','1.2 Kg'),
('PR141','1.2 Kg'),('PR142','1.2 Kg'),('PR143','1.2 Kg'),('PR145','1.2 Kg'),('PR146','1.2 Kg'),
('PR147','1.2 Kg'),('PR148','1.2 Kg'),('PR149','1.2 Kg')
GO

select * from Comments where ProductID like 'PR067'