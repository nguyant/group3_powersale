USE [master]
GO
/****** Object:  Database [psdb]    Script Date: 2018-12-25 05:28:47 PM ******/
CREATE DATABASE [psdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'psdb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\psdb.mdf' , SIZE = 3136KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'psdb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\psdb_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [psdb] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [psdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [psdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [psdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [psdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [psdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [psdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [psdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [psdb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [psdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [psdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [psdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [psdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [psdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [psdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [psdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [psdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [psdb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [psdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [psdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [psdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [psdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [psdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [psdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [psdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [psdb] SET RECOVERY FULL 
GO
ALTER DATABASE [psdb] SET  MULTI_USER 
GO
ALTER DATABASE [psdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [psdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [psdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [psdb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'psdb', N'ON'
GO
USE [psdb]
GO
/****** Object:  Table [dbo].[Admins]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admins](
	[AdminID] [varchar](10) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Password] [varchar](30) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
	[Avatar] [varchar](200) NOT NULL,
	[CreatedDate] [date] NULL DEFAULT (getdate()),
	[AdminRole] [int] NULL DEFAULT ((0)),
	[IsStatus] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Brands]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Brands](
	[BrandID] [varchar](10) NOT NULL,
	[BrandName] [nvarchar](100) NOT NULL,
	[BrandImages] [nvarchar](max) NULL,
	[Descriptions] [nvarchar](max) NULL,
	[IsStatus] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[BrandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryID] [varchar](10) NOT NULL,
	[CategoryName] [nvarchar](100) NOT NULL,
	[CategoryImage] [varchar](max) NOT NULL,
	[IsStatus] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comments](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [varchar](10) NOT NULL,
	[CustomerID] [varchar](10) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[Respond] [nvarchar](max) NULL,
	[CommentDate] [date] NULL DEFAULT (getdate()),
	[RespondDate] [date] NULL DEFAULT (getdate()),
	[IsDisplay] [bit] NOT NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerID] [varchar](10) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Password] [varchar](30) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Gender] [bit] NULL DEFAULT ((1)),
	[Phone] [varchar](20) NULL,
	[Address] [nvarchar](max) NULL,
	[Avatar] [varchar](200) NOT NULL,
	[CreatedDate] [date] NULL DEFAULT (getdate()),
	[IsStatus] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [varchar](10) NOT NULL,
	[CustomerID] [varchar](10) NOT NULL,
	[Total] [int] NOT NULL,
	[ShipName] [nvarchar](100) NOT NULL,
	[ShipPhone] [varchar](20) NOT NULL,
	[ShipAddress] [nvarchar](200) NOT NULL,
	[ShipDate] [date] NOT NULL,
	[ShipNote] [nvarchar](max) NULL,
	[OrderDate] [date] NULL DEFAULT (getdate()),
	[PaymentMethod] [nvarchar](100) NOT NULL,
	[ProcessStatus] [nvarchar](50) NOT NULL,
	[IsStatus] [bit] NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrdersDetails]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrdersDetails](
	[OdID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [varchar](10) NOT NULL,
	[ProductID] [varchar](10) NOT NULL,
	[SellingPrice] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[IsStatus] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[OdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Products]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Products](
	[ProductID] [varchar](10) NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[Price] [int] NOT NULL,
	[DiscountProduct] [int] NOT NULL,
	[CategoryID] [varchar](10) NOT NULL,
	[BrandID] [varchar](10) NOT NULL,
	[Descriptions] [nvarchar](max) NULL,
	[Quantity] [int] NOT NULL,
	[Feature] [nvarchar](100) NULL,
	[Image1] [varchar](max) NULL,
	[Image2] [varchar](max) NULL,
	[Image3] [varchar](max) NULL,
	[Image4] [varchar](max) NULL,
	[IsStatus] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductsDetails]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductsDetails](
	[ProductDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [varchar](10) NOT NULL,
	[CPU] [nvarchar](200) NULL,
	[Memory] [nvarchar](200) NULL,
	[VGA] [nvarchar](200) NULL,
	[HDD] [nvarchar](200) NULL,
	[Camera] [nvarchar](200) NULL,
	[Display] [nvarchar](300) NULL,
	[Battery] [nvarchar](100) NULL,
	[Weights] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ratings]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ratings](
	[RatingID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [varchar](10) NOT NULL,
	[CustomerID] [varchar](10) NOT NULL,
	[Rate] [int] NOT NULL,
	[RatingDate] [date] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[RatingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WishAndCart]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WishAndCart](
	[WishlistID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [varchar](10) NOT NULL,
	[ProductID] [varchar](10) NOT NULL,
	[DateCreated] [date] NULL DEFAULT (getdate()),
	[IsCart] [int] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[WishlistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[AverageRatings]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[AverageRatings] AS
	SELECT ROW_NUMBER() OVER (ORDER BY p.ProductName) AS id, p.ProductID, p.ProductName, AVG(convert(decimal(4,2),r.Rate)) AS averageRating, AVG(r.Rate) AS star
	FROM Products p JOIN Ratings r ON p.ProductID = r.ProductID
	GROUP BY p.ProductName, p.ProductID

GO
/****** Object:  View [dbo].[OrderListThisMonth]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[OrderListThisMonth] AS
	SELECT * FROM Orders WHERE MONTH(OrderDate) = datepart(month,getdate())

GO
/****** Object:  View [dbo].[Report]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Report] AS
	SELECT row_number() OVER (ORDER BY a.OrderID) AS rowid, a.OrderID, b.ProductID,c.ProductName, a.OrderDate, a.ShipDate,b.SellingPrice, b.Quantity, d.CustomerID, d.FirstName,d.LastName,a.ShipName, a.ProcessStatus,Total=(b.SellingPrice*b.Quantity)      
	FROM Orders a JOIN OrdersDetails b ON a.OrderID = b.OrderID JOIN Products c ON b.ProductID=c.ProductID JOIN Customers d ON a.CustomerID =d.CustomerID

GO
/****** Object:  View [dbo].[TopRatingDateRange]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TopRatingDateRange] AS 	
	SELECT TOP 1000000 p.ProductID, p.ProductName, p.Image1, p.Price, p.DiscountProduct, c.CategoryName,AVG(convert(decimal(4,2),r.Rate)) AS averageRating, COUNT(convert(decimal(4,0),r.Rate)) AS countRating
	FROM Products p JOIN Ratings r ON p.ProductID = r.ProductID JOIN Categories c ON p.CategoryID = c.CategoryID
	GROUP BY p.ProductName, p.ProductID,p.Image1,p.Price,p.DiscountProduct,c.CategoryName
	ORDER BY averageRating DESC, countRating DESC

GO
/****** Object:  View [dbo].[TopRatingThisYear]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TopRatingThisYear] AS 	
	SELECT TOP 10 p.ProductID, p.ProductName, p.Image1, p.Price, p.DiscountProduct, c.CategoryName,AVG(convert(decimal(4,2),r.Rate)) AS averageRating, COUNT(convert(decimal(4,0),r.Rate)) AS countRating
	FROM Products p JOIN Ratings r ON p.ProductID = r.ProductID JOIN Categories c ON p.CategoryID = c.CategoryID
	WHERE YEAR(r.RatingDate) = datepart(YEAR,getdate())
	GROUP BY p.ProductName, p.ProductID,p.Image1,p.Price,p.DiscountProduct,c.CategoryName
	ORDER BY averageRating DESC, countRating DESC

GO
/****** Object:  View [dbo].[TopSellingThisYear]    Script Date: 2018-12-25 05:28:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TopSellingThisYear] AS 
SELECT TOP 10 p.ProductID, p.ProductName, p.Image1, p.Price, p.DiscountProduct, c.CategoryName, SUM(convert(decimal(4,0),r.Quantity)) AS TopSelling
	FROM Products p JOIN OrdersDetails r ON p.ProductID = r.ProductID JOIN Categories c ON p.CategoryID = c.CategoryID JOIN Orders o ON o.OrderID = r.OrderID
	WHERE YEAR(o.OrderDate) = datepart(YEAR,getdate())
	GROUP BY p.ProductName, p.ProductID, p.Image1, p.Price, p.DiscountProduct,c.CategoryName
	ORDER BY TopSelling DESC, p.Price DESC

GO
INSERT [dbo].[Admins] ([AdminID], [Email], [Password], [FullName], [Avatar], [CreatedDate], [AdminRole], [IsStatus]) VALUES (N'AD001', N'tan@mail.com', N'123456', N'Nguyen Nhat Tan', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 0, 1)
INSERT [dbo].[Admins] ([AdminID], [Email], [Password], [FullName], [Avatar], [CreatedDate], [AdminRole], [IsStatus]) VALUES (N'AD002', N'anh@mail.com', N'123456', N'Le Duc Anh', N'content/img/Avatars/default-avatar5.jpg', CAST(N'2018-12-18' AS Date), 0, 1)
INSERT [dbo].[Admins] ([AdminID], [Email], [Password], [FullName], [Avatar], [CreatedDate], [AdminRole], [IsStatus]) VALUES (N'AD003', N'nhi@mail.com', N'123456', N'Nguyen Le Ngoc Nhi', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 0, 1)
INSERT [dbo].[Admins] ([AdminID], [Email], [Password], [FullName], [Avatar], [CreatedDate], [AdminRole], [IsStatus]) VALUES (N'AD004', N'nam@mail.com', N'123456', N'Nguyen Hoang Nam', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 0, 1)
INSERT [dbo].[Brands] ([BrandID], [BrandName], [BrandImages], [Descriptions], [IsStatus]) VALUES (N'BR001', N'Dell', N'content/img/Brands/dell.png', N'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use ?Dell? in reference to client solutions and corporate functions.', 1)
INSERT [dbo].[Brands] ([BrandID], [BrandName], [BrandImages], [Descriptions], [IsStatus]) VALUES (N'BR002', N'HP', N'content/img/Brands/hp.png', N'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use ?Dell? in reference to client solutions and corporate functions.', 1)
INSERT [dbo].[Brands] ([BrandID], [BrandName], [BrandImages], [Descriptions], [IsStatus]) VALUES (N'BR003', N'Asus', N'content/img/Brands/asus.png', N'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use ?Dell? in reference to client solutions and corporate functions.', 1)
INSERT [dbo].[Brands] ([BrandID], [BrandName], [BrandImages], [Descriptions], [IsStatus]) VALUES (N'BR004', N'Apple', N'content/img/Brands/apple.png', N'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use ?Dell? in reference to client solutions and corporate functions.', 1)
INSERT [dbo].[Brands] ([BrandID], [BrandName], [BrandImages], [Descriptions], [IsStatus]) VALUES (N'BR005', N'Sony', N'content/img/Brands/sony.png', N'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use ?Dell? in reference to client solutions and corporate functions.', 1)
INSERT [dbo].[Brands] ([BrandID], [BrandName], [BrandImages], [Descriptions], [IsStatus]) VALUES (N'BR006', N'MSI', N'content/img/Brands/msi.png', N'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use ?Dell? in reference to client solutions and corporate functions.', 1)
INSERT [dbo].[Brands] ([BrandID], [BrandName], [BrandImages], [Descriptions], [IsStatus]) VALUES (N'BR007', N'Lenovo', N'content/img/Brands/lenovo.png', N'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use ?Dell? in reference to client solutions and corporate functions.', 1)
INSERT [dbo].[Brands] ([BrandID], [BrandName], [BrandImages], [Descriptions], [IsStatus]) VALUES (N'BR008', N'Acer', N'content/img/Brands/acer.png', N'The Dell name is a globally recognized brand and leader in the client solutions category among consumers and business/institutional customers. Use ?Dell? in reference to client solutions and corporate functions.', 1)
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [CategoryImage], [IsStatus]) VALUES (N'CA001', N'Laptops', N'content/img/Categories/laptops.png', 1)
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [CategoryImage], [IsStatus]) VALUES (N'CA002', N'Headphones', N'content/img/Categories/headphones.png', 1)
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [CategoryImage], [IsStatus]) VALUES (N'CA003', N'Keyboards', N'content/img/Categories/keyboards.png', 1)
INSERT [dbo].[Categories] ([CategoryID], [CategoryName], [CategoryImage], [IsStatus]) VALUES (N'CA004', N'Mouses', N'content/img/Categories/mouses.png', 1)
SET IDENTITY_INSERT [dbo].[Comments] ON 

INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (1, N'PR023', N'CS001', N'This is Very Good', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (2, N'PR024', N'CS002', N'Nice', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (3, N'PR025', N'CS003', N'I Like It', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (4, N'PR026', N'CS004', N'Good', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (5, N'PR027', N'CS005', N'Very Nice', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (6, N'PR028', N'CS006', N'Good For me', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (7, N'PR023', N'CS007', N'This is Very Good', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (8, N'PR024', N'CS008', N'Nice', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (9, N'PR025', N'CS009', N'I Like It', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (10, N'PR026', N'CS010', N'Good', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (11, N'PR027', N'CS011', N'Very Nice', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (12, N'PR028', N'CS012', N'Good For me', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (13, N'PR029', N'CS001', N'This is Very Good', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (14, N'PR030', N'CS002', N'Nice', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (15, N'PR031', N'CS003', N'I Like It', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (16, N'PR032', N'CS004', N'Good', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (17, N'PR033', N'CS005', N'Very Nice', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (18, N'PR034', N'CS006', N'Good For me', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (19, N'PR035', N'CS013', N'This is Very Good', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (20, N'PR036', N'CS014', N'Nice', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (21, N'PR037', N'CS015', N'I Like It', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (22, N'PR038', N'CS016', N'Good', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (23, N'PR039', N'CS017', N'Very Nice', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[Comments] ([CommentID], [ProductID], [CustomerID], [Content], [Respond], [CommentDate], [RespondDate], [IsDisplay]) VALUES (24, N'PR040', N'CS018', N'Good For me', N'Thankyou', CAST(N'2018-12-20' AS Date), CAST(N'2018-12-20' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Comments] OFF
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS001', N'thang@mail.com', N'123456', N'Thang', N'Nguyen', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS002', N'duy@mail.com', N'123456', N'Duy', N'Khac', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS003', N'brian@mail.com', N'123456', N'Brian', N'Welcker', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS004', N'tete@mail.com', N'123456', N'Tete', N'Mensa-Annan', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS005', N'syed@mail.com', N'123456', N'Syed', N'Abbas', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS006', N'tuan@mail.com', N'123456', N'Tuan', N'Tran', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS007', N'nam@mail.com', N'123456', N'Nam', N'Nguyen', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS008', N'ronald@mail.com', N'123456', N'Ronald', N'Adina', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS009', N'samuel@mail.com', N'123456', N'Samuel', N'Agcaoili', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS010', N'james@mail.com', N'123456', N'James', N'Aguilar', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS011', N'gail@mail.com', N'123456', N'Gail', N'Erickson', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar3.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS012', N'janice@mail.com', N'123456', N'Janice', N'Galvin', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS013', N'jill@mail.com', N'123456', N'Jill', N'Williams', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar5.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS014', N'catherine@mail.com', N'123456', N'Catherine', N'Abel', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar3.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS015', N'kim@mail.com', N'123456', N'Kim', N'Abercrombie', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS016', N'frances@mail.com', N'123456', N'Frances', N'Adams', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar5.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS017', N'margaret@mail.com', N'123456', N'Margaret', N'Smith', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar3.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS018', N'carla@mail.com', N'123456', N'Carla', N'Adams', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS019', N'kimitry@mail.com', N'123456', N'Kim', N'Alders', 0, N'(099) 999 9999', N'590 CMT8, Q3, HCM', N'content/img/Avatars/CS019.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS020', N'lili@mail.com', N'123456', N'Lili', N'Alameda', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar5.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS021', N'TwannaEvans@mail.com', N'123456', N'Twanna', N'Evans', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar3.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS022', N'AnnEvans@mail.com', N'123456', N'Ann', N'Evans', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS023', N'JohnEvans@mail.com', N'123456', N'John', N'Evans', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS024', N'MarcFaeber@mail.com', N'123456', N'Marc', N'Faeber', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS025', N'FadiFakhouri@mail.com', N'123456', N'Fadi', N'Fakhouri', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS026', N'CarolynFarino@mail.com', N'123456', N'Carolyn', N'Farino', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS027', N'GeriFarrell@mail.com', N'123456', N'Geri', N'Farrell', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar3.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS028', N'HanyingFeng@mail.com', N'123456', N'Hanying', N'Feng', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS029', N'RhodaFinley@mail.com', N'123456', N'Rhoda', N'Finley', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS030', N'DuaneFitzgerald@mail.com', N'123456', N'Duane', N'Fitzgerald', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS031', N'JamesFine@mail.com', N'123456', N'James', N'Fine', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS032', N'KathieFlood@mail.com', N'123456', N'Kathie', N'Flood', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar3.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS033', N'JayFluegel@mail.com', N'123456', N'Jay', N'Fluegel', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS034', N'KellyFocht@mail.com', N'123456', N'Kelly', N'Focht', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS035', N'JeffreyFord@mail.com', N'123456', N'Jeffrey', N'Ford', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS036', N'GarthFort@mail.com', N'123456', N'Garth', N'Fort', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS037', N'DorothyFox@mail.com', N'123456', N'Dorothy', N'Fox', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar4.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS038', N'JudithFrazier@mail.com', N'123456', N'Judith', N'Frazier', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar3.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS039', N'JohnFredericksen@mail.com', N'123456', N'John', N'Fredericksen', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS040', N'SusanFrench@mail.com', N'123456', N'Susan', N'French', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar5.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS041', N'LiamFriedland@mail.com', N'123456', N'Liam', N'Friedland', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS042', N'MihailFrintu@mail.com', N'123456', N'Mihail', N'Frintu', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS043', N'JohnFord@mail.com', N'123456', N'John', N'Ford', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS044', N'PaulFulton@mail.com', N'123456', N'Paul', N'Fulton', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS045', N'DonFunk@mail.com', N'123456', N'Don', N'Funk', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS046', N'BobGage@mail.com', N'123456', N'Bob', N'Gage', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS047', N'AldeenGallagher@mail.com', N'123456', N'Aldeen', N'Gallagher', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar3.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS048', N'MichaelGalos@mail.com', N'123456', N'Michael', N'Galos', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar2.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS049', N'JonGanio@mail.com', N'123456', N'Jon', N'Ganio', 1, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar1.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Customers] ([CustomerID], [Email], [Password], [FirstName], [LastName], [Gender], [Phone], [Address], [Avatar], [CreatedDate], [IsStatus]) VALUES (N'CS050', N'KathleenGarza@mail.com', N'123456', N'Kathleen', N'Garza', 0, N'(096) 737-4508', N'590 CMT8, Q3, HCM', N'content/img/Avatars/default-avatar5.jpg', CAST(N'2018-12-18' AS Date), 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O001', N'CS001', 15000, N'Thang Nguyen', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-10-10' AS Date), N'n/a', CAST(N'2018-10-11' AS Date), N'Cash', N'Processing', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O002', N'CS002', 13000, N'Duy Khac', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-11-10' AS Date), N'n/a', CAST(N'2018-11-11' AS Date), N'Cash', N'Shipping', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O003', N'CS003', 19000, N'Brian Welcker', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-10' AS Date), N'n/a', CAST(N'2018-12-11' AS Date), N'Cash', N'Completed', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O004', N'CS004', 15000, N'Tete Mensa-Annan', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-14' AS Date), N'n/a', CAST(N'2018-12-15' AS Date), N'Cash', N'Processing', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O005', N'CS005', 13000, N'Syed Abbas', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-17' AS Date), N'n/a', CAST(N'2018-12-18' AS Date), N'Cash', N'Shipping', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O006', N'CS006', 19000, N'Tuan Tran', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-20' AS Date), N'n/a', CAST(N'2018-12-21' AS Date), N'Cash', N'Completed', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O007', N'CS007', 15000, N'Nam Nguyen', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-22' AS Date), N'n/a', CAST(N'2018-12-23' AS Date), N'Cash', N'Processing', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O008', N'CS008', 13000, N'Ronald Adina', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-22' AS Date), N'n/a', CAST(N'2018-12-24' AS Date), N'Cash', N'Shipping', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O009', N'CS009', 19000, N'Samuel Agcaoili', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-23' AS Date), N'n/a', CAST(N'2018-12-25' AS Date), N'Cash', N'Completed', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O010', N'CS010', 15000, N'James Aguilar', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-25' AS Date), N'n/a', CAST(N'2018-12-26' AS Date), N'Cash', N'Processing', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O011', N'CS011', 13000, N'Gail Erickson', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-26' AS Date), N'n/a', CAST(N'2018-12-27' AS Date), N'Cash', N'Shipping', 1)
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [Total], [ShipName], [ShipPhone], [ShipAddress], [ShipDate], [ShipNote], [OrderDate], [PaymentMethod], [ProcessStatus], [IsStatus]) VALUES (N'O012', N'CS012', 19000, N'Janice Galvin', N'(096) 737-4508', N'590 CMT8, Q3, HCM', CAST(N'2018-12-27' AS Date), N'n/a', CAST(N'2018-12-28' AS Date), N'Cash', N'Completed', 1)
SET IDENTITY_INSERT [dbo].[OrdersDetails] ON 

INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (43, N'O001', N'PR023', 1100, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (44, N'O001', N'PR034', 720, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (45, N'O001', N'PR041', 650, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (46, N'O002', N'PR024', 1100, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (47, N'O002', N'PR035', 975, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (48, N'O002', N'PR042', 600, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (49, N'O003', N'PR025', 700, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (50, N'O003', N'PR036', 1350, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (51, N'O003', N'PR043', 600, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (52, N'O004', N'PR026', 300, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (53, N'O004', N'PR037', 1350, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (54, N'O004', N'PR044', 600, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (55, N'O005', N'PR027', 1300, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (56, N'O005', N'PR038', 1250, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (57, N'O005', N'PR045', 595, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (58, N'O006', N'PR028', 850, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (59, N'O006', N'PR039', 1000, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (60, N'O006', N'PR046', 500, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (61, N'O007', N'PR023', 1100, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (62, N'O007', N'PR029', 725, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (63, N'O007', N'PR048', 485, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (64, N'O008', N'PR025', 700, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (65, N'O008', N'PR030', 580, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (66, N'O008', N'PR049', 450, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (67, N'O009', N'PR027', 1300, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (68, N'O009', N'PR031', 550, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (69, N'O009', N'PR050', 2050, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (70, N'O010', N'PR028', 850, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (71, N'O010', N'PR032', 450, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (72, N'O010', N'PR051', 1050, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (73, N'O011', N'PR029', 725, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (74, N'O011', N'PR033', 725, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (75, N'O011', N'PR052', 990, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (76, N'O012', N'PR030', 580, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (77, N'O012', N'PR034', 720, 4, 1)
INSERT [dbo].[OrdersDetails] ([OdID], [OrderID], [ProductID], [SellingPrice], [Quantity], [IsStatus]) VALUES (78, N'O012', N'PR053', 850, 4, 1)
SET IDENTITY_INSERT [dbo].[OrdersDetails] OFF
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR023', N'Laptop MSI', 1100, 0, N'CA001', N'BR006', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/MSI-1.jpg', N'content/img/Products/MSI-2.jpg', N'content/img/Products/MSI-3.jpg', N'content/img/Products/MSI -4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR024', N'Laptop Lenovo Thinkpad', 1100, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/LenovoThinkpad-1.jpg', N'content/img/Products/LenovoThinkpad-2.jpg', N'content/img/Products/LenovoThinkpad-3.jpg', N'content/img/Products/LenovoThinkpad-4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR025', N'Laptop Lenovo pad', 700, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/Lenovo-1.png', N'content/img/Products/Lenovo-2.png', N'content/img/Products/Lenovo-3.png', N'content/img/Products/Lenovo-4.png', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR026', N'Lenovo Ideapad', 300, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/Lenovo Ideapad-1.jpg', N'content/img/Products/Lenovo Ideapad-2.jpg', N'content/img/Products/Lenovo Ideapad-3.jpg', N'content/img/Products/Lenovo Ideapad-4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR027', N'Lenovo Legion', 1300, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/lnvLegion.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR028', N'Lenovo IdeaPad320', 850, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/lnvIdeaPad320.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR029', N'Lenovo IdeaPad330', 725, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/lnvIdeaPad330.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR030', N'Lenovo Yoga520', 580, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/lnvYoga520.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR031', N'Lenovo IdeaPad330s', 550, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'New', N'content/img/Products/lnvIdeaPad330s.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR032', N'Lenovo IdeaPad130', 450, 0, N'CA001', N'BR007', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/lnvIdeaPad130.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR033', N'DellVostro', 725, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/DellVostro1.png', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR034', N'Dell5570A', 720, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/Dell5570A-1.png', N'content/img/Products/Dell5570A-2.png', N'content/img/Products/Dell5570A-3.png', N'content/img/Products/Dell5570A-4.png', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR035', N'Dell7370', 975, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'Hot', N'content/img/Products/Dell7370-1.png', N'content/img/Products/Dell7370-2.png', N'content/img/Products/Dell7370-3.png', N'content/img/Products/Dell7370-4.png', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR036', N'Dell7570', 1350, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'New', N'content/img/Products/Dell7570.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR037', N'Dell7373', 1350, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'Hot', N'content/img/Products/Dell7373.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR038', N'Dell5379', 1250, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'Hot', N'content/img/Products/Dell5379.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR039', N'Dell3578', 1000, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/Dell3578.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR040', N'DellVostro5568', 800, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/DellVostro5568.png', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR041', N'Dell3576', 650, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/Dell3576.png', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR042', N'Dell3567', 600, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/Dell3567.png', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR043', N'Dell3476', 600, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/Dell3476.png', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR044', N'DellVostro3568', 590, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/DellVostro3568.png', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR045', N'DellVostro3468', 595, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/DellVostro3468.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR046', N'DellVostro3368', 500, 0, N'CA001', N'BR001', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/DellVostro3368.png', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR048', N'Asus vivo406', 485, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asvivo406-1.png', N'content/img/Products/asvivo406-2.png', N'content/img/Products/asvivo406-3.png', N'content/img/Products/asvivo406-4.png', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR049', N'Asus x541', 450, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asx541-1.jpg', N'content/img/Products/asx541-2.jpg', N'content/img/Products/asx541-3.jpg', N'content/img/Products/asx541-4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR050', N'Asus zenbook', 2050, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/aszenbook.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR051', N'Asus zenbook430', 1050, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/aszenbook430.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR052', N'Asus vivo410UA', 990, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asvivo410UA.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR053', N'Asus vivo510UQ', 850, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asvivo510UQ.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR054', N'Asus vivo510UAi5', 800, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asvivo510UAi5.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR055', N'Asus vivo410UAi5', 750, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asvivo410UAi5.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR056', N'Asus S510Uai3', 650, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asS510Uai3.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR057', N'Asus vivotp410UAi3', 670, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asvivotp410UAi3.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR058', N'Asus vivos410i3', 645, 0, N'CA001', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/asvivos410i3.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR059', N'MacBook-MNYL2ZP-A ', 1800, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'Hot', N'content/img/Products/MacBook12-MNYL2ZP-A-1.jpg', N'content/img/Products/MacBook12-MNYL2ZP-A-2.jpg', N'content/img/Products/MacBook12-MNYL2ZP-A-3.jpg', N'content/img/Products/MacBook12-MNYL2ZP-A-4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR060', N'MacBook-MPXV2', 2250, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/MacBook13MPXV2-1.jpg', N'content/img/Products/MacBook13MPXV2-2.jpg', N'content/img/Products/MacBook13MPXV2-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR061', N'MacBook-MQD32LL', 1150, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/MacBookAirMQD32LL.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR062', N'MacBookAir-MPXT2LL', 1850, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'Hot', N'content/img/Products/MacBook13MPXT2LL.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR063', N'MacBook-MPTT2', 3000, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'New', N'content/img/Products/MacBook15MPTT2-1.png', N'content/img/Products/MacBook15MPTT2-2.jpg', N'content/img/Products/MacBook15MPTT2-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR064', N'MacBook-MPXQ2', 1550, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/MacBook13MPXQ2-2.jpg', N'content/img/Products/MacBook13MPXQ2-3.jpg', NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR065', N'MacBook-MPXU2', 1950, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'New', N'content/img/Products/MacBook13MPXU2-1.jpg', N'content/img/Products/MacBook13MPXU2-2.jpg', N'content/img/Products/MacBook13MPXU2-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR066', N'MacBook-MNYN2', 1915, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/MacBook12MNYN2-1.png', N'content/img/Products/MacBook12MNYN2-2.jpg', N'content/img/Products/MacBook12MNYN2-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR067', N'MacBook-MPXW2', 2550, 0, N'CA001', N'BR004', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'New', N'content/img/Products/MacBook13MPXW2-1.jpg', N'content/img/Products/MacBook13MPXW2-2.jpg', N'content/img/Products/MacBook13MPXW2-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR068', N'HP Envy 13 ah0025 TU', 2550, 3, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'Hot', N'content/img/Products/HPEnvy13ah0025TU-1.png', N'content/img/Products/HPEnvy13ah0025TU-2.jpg', N'content/img/Products/HPEnvy13ah0025TU-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR069', N'HP Pavilion Gaming 15', 1250, 0, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'New', N'content/img/Products/HPPavilionGaming15-cx0179TX-1.jpg', N'content/img/Products/HPPavilionGaming15-cx0179TX-2.jpg', N'content/img/Products/HPPavilionGaming15-cx0179TX-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR070', N'HP 14-cK0066 TU', 350, 0, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/HP14-cK0066TU-1.jpg', N'content/img/Products/HP14-cK0066TU-2.jpg', N'content/img/Products/HP14-cK0066TU-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR071', N'HP Pavilion 14', 750, 0, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/HPPavilion14-ce0023TU-1.jpg', N'content/img/Products/HPPavilion14-ce0023TU-2.jpg', N'content/img/Products/HPPavilion14-ce0023TU-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR072', N'HP Envy 13-ah0027 TU', 1300, 5, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/HPEnvy13-ah0027TU-1.jpg', N'content/img/Products/HPEnvy13-ah0027TU-2.png', N'content/img/Products/HPEnvy13-ah0027TU-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR073', N'HP 15-da0051 TU', 495, 4, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/HP15-da0051TU-1.jpg', N'content/img/Products/HP15-da0051TU-2.jpg', N'content/img/Products/HP15-da0051TU-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR074', N'HP Probook 430G5', 1100, 0, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/HPProbook430G5-2XR79PA-1.png', N'content/img/Products/HPProbook430G5-2XR79PA-2.jpg', N'content/img/Products/HPProbook430G5-2XR79PA-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR075', N'HP Pavilion 14', 750, 2, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/HPPavilion14-ce0024TU-1.jpg', N'content/img/Products/HPPavilion14-ce0024TU-2.jpg', N'content/img/Products/HPPavilion14-ce0024TU-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR076', N'HP Pavilion X36014', 650, 0, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/HPPavilionX36014-cd0082TU-1.jpg', N'content/img/Products/HPPavilionX36014-cd0082TU-2.jpg', N'content/img/Products/HPPavilionX36014-cd0082TU-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR077', N'HP Pavilion15-cc058TX', 925, 0, N'CA001', N'BR002', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/HPPavilion15-cc058TX-1.jpg', N'content/img/Products/HPPavilion15-cc058TX-2.jpg', N'content/img/Products/HPPavilion15-cc058TX-3.jpg', NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR078', N'Asus Cerberus', 50, 0, N'CA004', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/Asus Cerberus.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR079', N'Asus Strix Wireless', 85, 0, N'CA004', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/AsusStrixWireless.jpg', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR080', N'Asus Strix Wireless', 25, 0, N'CA003', N'BR003', N'A mobile phone is a wireless handheld device that allows users to make and receive calls and to send text messages, among other features. ', 10, N'--', N'content/img/Products/CameraQuestekWinQB.png', NULL, NULL, NULL, 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR081', N'Mouse Dell', 1000, 0, N'CA004', N'BR001', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/dell/m1.jpg', N'content/img/Products/dell/m1.jpg', N'content/img/Products/dell/m1.jpg', N'content/img/Products/dell/m1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR082', N'Mouse Dell', 1200, 0, N'CA004', N'BR001', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/dell/m2.jpg', N'content/img/Products/dell/m2.jpg', N'content/img/Products/dell/m2.jpg', N'content/img/Products/dell/m2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR083', N'Mouse Dell', 1000, 0, N'CA004', N'BR001', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/dell/m3.jpg', N'content/img/Products/dell/m3.jpg', N'content/img/Products/dell/m3.jpg', N'content/img/Products/dell/m3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR084', N'Mouse Dell', 1300, 0, N'CA004', N'BR001', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/dell/m4.jpg', N'content/img/Products/dell/m4.jpg', N'content/img/Products/dell/m4.jpg', N'content/img/Products/dell/m4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR085', N'Keyboard Acer', 1200, 0, N'CA003', N'BR008', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/acer/k1.jpg', N'content/img/Products/acer/k1.jpg', N'content/img/Products/acer/k1.jpg', N'content/img/Products/acer/k1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR086', N'Keyboard Acer', 1200, 0, N'CA003', N'BR008', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/acer/k2.jpg', N'content/img/Products/acer/k2.jpg', N'content/img/Products/acer/k2.jpg', N'content/img/Products/acer/k2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR087', N'Keyboard Acer', 1200, 0, N'CA003', N'BR008', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/acer/k3.jpg', N'content/img/Products/acer/k3.jpg', N'content/img/Products/acer/k3.jpg', N'content/img/Products/acer/k3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR088', N'Mouse Acer', 1200, 0, N'CA004', N'BR008', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/acer/m1.jpg', N'content/img/Products/acer/m1.jpg', N'content/img/Products/acer/m1.jpg', N'content/img/Products/acer/m1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR089', N'Mouse Acer', 1200, 0, N'CA004', N'BR008', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/acer/m2.jpg', N'content/img/Products/acer/m2.jpg', N'content/img/Products/acer/m2.jpg', N'content/img/Products/acer/m2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR090', N'Mouse Acer', 1200, 0, N'CA004', N'BR008', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/acer/m3.jpg', N'content/img/Products/acer/m3.jpg', N'content/img/Products/acer/m3.jpg', N'content/img/Products/acer/m3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR091', N'Mouse Apple', 1200, 0, N'CA004', N'BR004', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/apple/m1.jpg', N'content/img/Products/apple/m1.jpg', N'content/img/Products/apple/m1.jpg', N'content/img/Products/apple/m1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR092', N'Mouse Apple', 1200, 0, N'CA004', N'BR004', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/apple/m2.jpg', N'content/img/Products/apple/m2.jpg', N'content/img/Products/apple/m2.jpg', N'content/img/Products/apple/m2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR093', N'Keyboard Apple', 1200, 0, N'CA003', N'BR004', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/apple/k2.jpg', N'content/img/Products/apple/k2.jpg', N'content/img/Products/apple/k2.jpg', N'content/img/Products/apple/k2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR094', N'Keyboard Apple', 1200, 0, N'CA003', N'BR004', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/apple/k1.jpg', N'content/img/Products/apple/k1.jpg', N'content/img/Products/apple/k1.jpg', N'content/img/Products/apple/k1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR095', N'Keyboard Apple', 1200, 0, N'CA003', N'BR004', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/apple/k3.jpg', N'content/img/Products/apple/k3.jpg', N'content/img/Products/apple/k3.jpg', N'content/img/Products/apple/k3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR096', N'Headphone Asus', 1200, 0, N'CA002', N'BR003', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/asus/1.jpg', N'content/img/Products/asus/1.jpg', N'content/img/Products/asus/1.jpg', N'content/img/Products/asus/1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR097', N'Headphone Asus', 1200, 0, N'CA002', N'BR003', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/asus/2.jpg', N'content/img/Products/asus/2.jpg', N'content/img/Products/asus/2.jpg', N'content/img/Products/asus/2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR098', N'Headphone Asus', 1200, 0, N'CA002', N'BR003', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/asus/3.jpg', N'content/img/Products/asus/3.jpg', N'content/img/Products/asus/3.jpg', N'content/img/Products/asus/3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR099', N'Headphone Asus', 1200, 0, N'CA002', N'BR003', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/asus/4.jpg', N'content/img/Products/asus/4.jpg', N'content/img/Products/asus/4.jpg', N'content/img/Products/asus/4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR100', N'Headphone Asus', 1200, 0, N'CA002', N'BR003', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/asus/5.jpg', N'content/img/Products/asus/5.jpg', N'content/img/Products/asus/5.jpg', N'content/img/Products/asus/5.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR101', N'Keyboard Asus', 1200, 0, N'CA003', N'BR003', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/asus/k2.jpg', N'content/img/Products/asus/k2.jpg', N'content/img/Products/asus/k2.jpg', N'content/img/Products/asus/k2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR102', N'Keyboard Asus', 1200, 0, N'CA003', N'BR003', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/asus/k1.jpg', N'content/img/Products/asus/k1.jpg', N'content/img/Products/asus/k1.jpg', N'content/img/Products/asus/k1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR103', N'Keyboard Asus', 1200, 0, N'CA003', N'BR003', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/asus/k3.jpg', N'content/img/Products/asus/k3.jpg', N'content/img/Products/asus/k3.jpg', N'content/img/Products/asus/k3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR104', N'Mouse Asus', 1200, 0, N'CA004', N'BR003', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/asus/m1.jpg', N'content/img/Products/asus/m1.jpg', N'content/img/Products/asus/m1.jpg', N'content/img/Products/asus/m1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR105', N'Mouse Asus', 1200, 0, N'CA004', N'BR003', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/asus/m2.jpg', N'content/img/Products/asus/m2.jpg', N'content/img/Products/asus/m2.jpg', N'content/img/Products/asus/m2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR106', N'Mouse Asus', 1200, 0, N'CA004', N'BR003', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/asus/m3.jpg', N'content/img/Products/asus/m3.jpg', N'content/img/Products/asus/m3.jpg', N'content/img/Products/asus/m3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR107', N'Mouse Asus', 1200, 0, N'CA004', N'BR003', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/asus/m4.jpg', N'content/img/Products/asus/m4.jpg', N'content/img/Products/asus/m4.jpg', N'content/img/Products/asus/m4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR108', N'Mouse HP', 1200, 0, N'CA004', N'BR002', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/HP/m1.jpg', N'content/img/Products/HP/m1.jpg', N'content/img/Products/HP/m1.jpg', N'content/img/Products/HP/m1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR109', N'Mouse HP', 1200, 0, N'CA004', N'BR002', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/HP/m2.jpg', N'content/img/Products/HP/m2.jpg', N'content/img/Products/HP/m2.jpg', N'content/img/Products/HP/m2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR110', N'Mouse HP', 1200, 0, N'CA004', N'BR002', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/HP/m3.jpg', N'content/img/Products/HP/m3.jpg', N'content/img/Products/HP/m3.jpg', N'content/img/Products/HP/m3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR111', N'Mouse HP', 1200, 0, N'CA004', N'BR002', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/HP/m4.jpg', N'content/img/Products/HP/m4.jpg', N'content/img/Products/HP/m4.jpg', N'content/img/Products/HP/m4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR112', N'Mouse HP', 1200, 0, N'CA004', N'BR002', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/HP/m5.jpg', N'content/img/Products/HP/m5.jpg', N'content/img/Products/HP/m5.jpg', N'content/img/Products/HP/m5.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR113', N'Headphone HP', 1200, 0, N'CA002', N'BR002', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/HP/1.jpg', N'content/img/Products/HP/1.jpg', N'content/img/Products/HP/1.jpg', N'content/img/Products/HP/1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR114', N'Headphone HP', 1200, 0, N'CA002', N'BR002', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/HP/2.jpg', N'content/img/Products/HP/2.jpg', N'content/img/Products/HP/2.jpg', N'content/img/Products/HP/2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR115', N'Headphone HP', 1200, 0, N'CA002', N'BR002', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/HP/3.jpg', N'content/img/Products/HP/3.jpg', N'content/img/Products/HP/3.jpg', N'content/img/Products/HP/3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR116', N'Headphone HP', 1200, 0, N'CA002', N'BR002', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/HP/4.jpg', N'content/img/Products/HP/4.jpg', N'content/img/Products/HP/4.jpg', N'content/img/Products/HP/4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR117', N'Keyboard HP', 1200, 0, N'CA003', N'BR002', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/HP/k2.jpg', N'content/img/Products/HP/k2.jpg', N'content/img/Products/HP/k2.jpg', N'content/img/Products/HP/k2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR118', N'Keyboard HP', 1200, 0, N'CA003', N'BR002', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/HP/k1.jpg', N'content/img/Products/HP/k1.jpg', N'content/img/Products/HP/k1.jpg', N'content/img/Products/HP/k1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR119', N'Keyboard HP', 1200, 0, N'CA003', N'BR002', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/HP/k3.jpg', N'content/img/Products/HP/k3.jpg', N'content/img/Products/HP/k3.jpg', N'content/img/Products/HP/k3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR120', N'Keyboard lenovo', 1200, 0, N'CA003', N'BR007', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/lenovo/k2.jpg', N'content/img/Products/lenovo/k2.jpg', N'content/img/Products/lenovo/k2.jpg', N'content/img/Products/lenovo/k2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR121', N'Keyboard lenovo', 1200, 0, N'CA003', N'BR007', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/lenovo/k1.jpg', N'content/img/Products/lenovo/k1.jpg', N'content/img/Products/lenovo/k1.jpg', N'content/img/Products/lenovo/k1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR122', N'Keyboard lenovo', 1200, 0, N'CA003', N'BR007', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/lenovo/k3.jpg', N'content/img/Products/lenovo/k3.jpg', N'content/img/Products/lenovo/k3.jpg', N'content/img/Products/lenovo/k3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR123', N'Headphone lenovo', 1200, 0, N'CA002', N'BR007', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/lenovo/1.jpg', N'content/img/Products/lenovo/1.jpg', N'content/img/Products/lenovo/1.jpg', N'content/img/Products/lenovo/1.jpg', 1)
GO
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR124', N'Headphone lenovo', 1200, 0, N'CA002', N'BR007', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/lenovo/2.jpg', N'content/img/Products/lenovo/2.jpg', N'content/img/Products/lenovo/2.jpg', N'content/img/Products/lenovo/2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR125', N'Headphone lenovo', 1200, 0, N'CA002', N'BR007', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/lenovo/3.jpg', N'content/img/Products/lenovo/3.jpg', N'content/img/Products/lenovo/3.jpg', N'content/img/Products/lenovo/3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR126', N'Mouse lenovo', 1200, 0, N'CA004', N'BR007', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/lenovo/m1.jpg', N'content/img/Products/lenovo/m1.jpg', N'content/img/Products/lenovo/m1.jpg', N'content/img/Products/lenovo/m1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR127', N'Mouse lenovo', 1200, 0, N'CA004', N'BR007', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/lenovo/m2.jpg', N'content/img/Products/lenovo/m2.jpg', N'content/img/Products/lenovo/m2.jpg', N'content/img/Products/lenovo/m2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR128', N'Mouse lenovo', 1200, 0, N'CA004', N'BR007', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/lenovo/m3.jpg', N'content/img/Products/lenovo/m3.jpg', N'content/img/Products/lenovo/m3.jpg', N'content/img/Products/lenovo/m3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR129', N'Mouse lenovo', 1200, 0, N'CA004', N'BR007', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/lenovo/m4.jpg', N'content/img/Products/lenovo/m4.jpg', N'content/img/Products/lenovo/m4.jpg', N'content/img/Products/lenovo/m4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR130', N'Mouse MSI', 1200, 0, N'CA004', N'BR006', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/MSI/m1.jpg', N'content/img/Products/MSI/m1.jpg', N'content/img/Products/MSI/m1.jpg', N'content/img/Products/MSI/m1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR131', N'Mouse MSI', 1200, 0, N'CA004', N'BR006', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/MSI/m2.jpg', N'content/img/Products/MSI/m2.jpg', N'content/img/Products/MSI/m2.jpg', N'content/img/Products/MSI/m2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR132', N'Mouse MSI', 1200, 0, N'CA004', N'BR006', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/MSI/m3.jpg', N'content/img/Products/MSI/m3.jpg', N'content/img/Products/MSI/m3.jpg', N'content/img/Products/MSI/m3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR133', N'Headphone MSI', 1200, 0, N'CA002', N'BR006', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/MSI/1.jpg', N'content/img/Products/MSI/1.jpg', N'content/img/Products/MSI/1.jpg', N'content/img/Products/MSI/1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR134', N'Headphone MSI', 1200, 0, N'CA002', N'BR006', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/MSI/2.jpg', N'content/img/Products/MSI/2.jpg', N'content/img/Products/MSI/2.jpg', N'content/img/Products/MSI/2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR135', N'Headphone MSI', 1200, 0, N'CA002', N'BR006', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/MSI/3.jpg', N'content/img/Products/MSI/3.jpg', N'content/img/Products/MSI/3.jpg', N'content/img/Products/MSI/3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR136', N'Keyboard MSI', 1200, 0, N'CA003', N'BR006', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/MSI/k2.jpg', N'content/img/Products/MSI/k2.jpg', N'content/img/Products/MSI/k2.jpg', N'content/img/Products/MSI/k2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR137', N'Keyboard MSI', 1200, 0, N'CA003', N'BR006', N'A computer keyboard 
is an input device used to enter characters and functions into the 
computer system by pressing buttons, or keys.', 10, N'--', N'content/img/Products/MSI/k1.jpg', N'content/img/Products/MSI/k1.jpg', N'content/img/Products/MSI/k1.jpg', N'content/img/Products/MSI/k1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR138', N'Headphone Sony', 1200, 0, N'CA002', N'BR005', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/Sony/1.jpg', N'content/img/Products/Sony/1.jpg', N'content/img/Products/Sony/1.jpg', N'content/img/Products/Sony/1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR139', N'Headphone Sony', 1200, 0, N'CA002', N'BR005', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/Sony/2.jpg', N'content/img/Products/Sony/2.jpg', N'content/img/Products/Sony/2.jpg', N'content/img/Products/Sony/2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR140', N'Headphone Sony', 1200, 0, N'CA002', N'BR005', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/Sony/3.jpg', N'content/img/Products/Sony/3.jpg', N'content/img/Products/Sony/3.jpg', N'content/img/Products/Sony/3.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR141', N'Headphone Sony', 1200, 0, N'CA002', N'BR005', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/Sony/4.jpg', N'content/img/Products/Sony/4.jpg', N'content/img/Products/Sony/4.jpg', N'content/img/Products/Sony/4.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR142', N'Headphone Sony', 1200, 0, N'CA002', N'BR005', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/Sony/5.jpg', N'content/img/Products/Sony/5.jpg', N'content/img/Products/Sony/5.jpg', N'content/img/Products/Sony/5.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR143', N'Headphone Sony', 1200, 0, N'CA002', N'BR005', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/Sony/6.jpg', N'content/img/Products/Sony/6.jpg', N'content/img/Products/Sony/6.jpg', N'content/img/Products/Sony/6.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR145', N'Headphone Sony', 1200, 0, N'CA002', N'BR005', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/Sony/7.jpg', N'content/img/Products/Sony/7.jpg', N'content/img/Products/Sony/7.jpg', N'content/img/Products/Sony/7.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR146', N'Headphone Sony', 1200, 0, N'CA002', N'BR005', N'They are electroacoustic 
transducers, which convert an  electrical 
signal to a corresponding sound.', 10, N'--', N'content/img/Products/Sony/8.jpg', N'content/img/Products/Sony/8.jpg', N'content/img/Products/Sony/8.jpg', N'content/img/Products/Sony/8.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR147', N'Mouse Sony', 1200, 0, N'CA004', N'BR005', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/MSonySI/m1.jpg', N'content/img/Products/Sony/m1.jpg', N'content/img/Products/Sony/m1.jpg', N'content/img/Products/Sony/m1.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR148', N'Mouse Sony', 1200, 0, N'CA004', N'BR005', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/Sony/m2.jpg', N'content/img/Products/Sony/m2.jpg', N'content/img/Products/Sony/m2.jpg', N'content/img/Products/Sony/m2.jpg', 1)
INSERT [dbo].[Products] ([ProductID], [ProductName], [Price], [DiscountProduct], [CategoryID], [BrandID], [Descriptions], [Quantity], [Feature], [Image1], [Image2], [Image3], [Image4], [IsStatus]) VALUES (N'PR149', N'Mouse Sony', 1200, 0, N'CA004', N'BR005', N'It’s lighter, has fewer 
moving parts thanks to its built-in battery and
 continuous bottom shell, and has an optimized foot design.', 10, N'--', N'content/img/Products/Sony/m3.jpg', N'content/img/Products/Sony/m3.jpg', N'content/img/Products/Sony/m3.jpg', N'content/img/Products/Sony/m3.jpg', 1)
SET IDENTITY_INSERT [dbo].[ProductsDetails] ON 

INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (1, N'PR023', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (2, N'PR024', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (3, N'PR025', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (4, N'PR026', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (5, N'PR027', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (6, N'PR028', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (7, N'PR029', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (8, N'PR030', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (9, N'PR031', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (10, N'PR032', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (11, N'PR033', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (12, N'PR034', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (13, N'PR035', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (14, N'PR036', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (15, N'PR037', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (16, N'PR038', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (17, N'PR039', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (18, N'PR040', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (19, N'PR041', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (20, N'PR042', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (21, N'PR043', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (22, N'PR044', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (23, N'PR045', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (24, N'PR046', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (25, N'PR048', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (26, N'PR049', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (27, N'PR050', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (28, N'PR051', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (29, N'PR052', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (30, N'PR053', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (31, N'PR054', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (32, N'PR055', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (33, N'PR056', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (34, N'PR057', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (35, N'PR058', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (36, N'PR059', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (37, N'PR060', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (38, N'PR061', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (39, N'PR062', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (40, N'PR063', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (41, N'PR064', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (42, N'PR065', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (43, N'PR066', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (44, N'PR067', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (45, N'PR068', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (46, N'PR069', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (47, N'PR070', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (48, N'PR071', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (49, N'PR072', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (50, N'PR073', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (51, N'PR074', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (52, N'PR075', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (53, N'PR076', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (54, N'PR077', N'Core i3-8130U 2.20Ghz', N'4GB DDR4 Bus 2400Mhz', N'Intel UHD 620M', N'1 TB SATA3 + SSD M.2 PCIe', N'2MP', N'15.6 inch backlit FHD(1920x1080) IPS', N'3 Cells 42Whrs', N'1.6 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (55, N'PR081', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (56, N'PR082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (57, N'PR083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (58, N'PR084', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (59, N'PR085', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (60, N'PR086', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (61, N'PR087', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (62, N'PR088', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (63, N'PR089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (64, N'PR090', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (65, N'PR091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (66, N'PR092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (67, N'PR093', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (68, N'PR094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (69, N'PR095', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (70, N'PR096', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (71, N'PR097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (72, N'PR098', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (73, N'PR099', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (74, N'PR100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (75, N'PR101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (76, N'PR102', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (77, N'PR103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (78, N'PR104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (79, N'PR105', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (80, N'PR106', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (81, N'PR107', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (82, N'PR108', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (83, N'PR109', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (84, N'PR110', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (85, N'PR111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (86, N'PR112', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (87, N'PR113', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (88, N'PR114', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (89, N'PR115', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (90, N'PR116', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (91, N'PR117', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (92, N'PR118', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (93, N'PR119', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (94, N'PR120', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (95, N'PR121', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (96, N'PR122', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (97, N'PR123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (98, N'PR124', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (99, N'PR125', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
GO
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (100, N'PR126', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (101, N'PR127', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (102, N'PR128', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (103, N'PR129', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (104, N'PR130', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (105, N'PR131', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (106, N'PR132', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (107, N'PR133', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (108, N'PR134', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (109, N'PR135', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (110, N'PR136', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (111, N'PR137', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (112, N'PR138', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (113, N'PR139', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (114, N'PR140', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (115, N'PR141', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (116, N'PR142', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (117, N'PR143', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (118, N'PR145', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (119, N'PR146', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (120, N'PR147', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (121, N'PR148', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
INSERT [dbo].[ProductsDetails] ([ProductDetailsID], [ProductID], [CPU], [Memory], [VGA], [HDD], [Camera], [Display], [Battery], [Weights]) VALUES (122, N'PR149', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1.2 Kg')
SET IDENTITY_INSERT [dbo].[ProductsDetails] OFF
SET IDENTITY_INSERT [dbo].[Ratings] ON 

INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (1, N'PR023', N'CS001', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (2, N'PR024', N'CS002', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (3, N'PR025', N'CS003', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (4, N'PR026', N'CS004', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (5, N'PR028', N'CS006', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (6, N'PR027', N'CS005', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (7, N'PR029', N'CS007', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (8, N'PR030', N'CS008', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (9, N'PR031', N'CS009', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (10, N'PR032', N'CS010', 4, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (11, N'PR023', N'CS011', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (12, N'PR024', N'CS012', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (13, N'PR025', N'CS013', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (14, N'PR026', N'CS014', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (15, N'PR027', N'CS015', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (16, N'PR028', N'CS016', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (17, N'PR029', N'CS017', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (18, N'PR030', N'CS018', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (19, N'PR031', N'CS019', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (20, N'PR032', N'CS020', 2, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (21, N'PR023', N'CS021', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (22, N'PR024', N'CS022', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (23, N'PR025', N'CS023', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (24, N'PR026', N'CS024', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (25, N'PR027', N'CS025', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (26, N'PR028', N'CS026', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (27, N'PR029', N'CS027', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (28, N'PR030', N'CS028', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (29, N'PR031', N'CS029', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (30, N'PR032', N'CS030', 3, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (31, N'PR023', N'CS031', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (32, N'PR024', N'CS032', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (33, N'PR025', N'CS033', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (34, N'PR026', N'CS034', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (35, N'PR027', N'CS035', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (36, N'PR028', N'CS036', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (37, N'PR029', N'CS037', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (38, N'PR030', N'CS038', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (39, N'PR031', N'CS039', 5, CAST(N'2018-12-20' AS Date))
INSERT [dbo].[Ratings] ([RatingID], [ProductID], [CustomerID], [Rate], [RatingDate]) VALUES (40, N'PR032', N'CS040', 5, CAST(N'2018-12-20' AS Date))
SET IDENTITY_INSERT [dbo].[Ratings] OFF
SET IDENTITY_INSERT [dbo].[WishAndCart] ON 

INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (2, N'CS001', N'PR067', CAST(N'2018-12-20' AS Date), 0)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (3, N'CS001', N'PR065', CAST(N'2018-12-20' AS Date), 0)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (5, N'CS001', N'PR079', CAST(N'2018-12-20' AS Date), 0)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (9, N'CS002', N'PR024', CAST(N'2018-12-20' AS Date), 2)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (10, N'CS003', N'PR025', CAST(N'2018-12-20' AS Date), 3)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (11, N'CS004', N'PR026', CAST(N'2018-12-20' AS Date), 4)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (12, N'CS005', N'PR027', CAST(N'2018-12-20' AS Date), 5)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (13, N'CS006', N'PR028', CAST(N'2018-12-20' AS Date), 6)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (14, N'CS007', N'PR029', CAST(N'2018-12-20' AS Date), 7)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (15, N'CS008', N'PR030', CAST(N'2018-12-20' AS Date), 8)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (16, N'CS009', N'PR031', CAST(N'2018-12-20' AS Date), 9)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (17, N'CS010', N'PR032', CAST(N'2018-12-20' AS Date), 10)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (18, N'CS011', N'PR033', CAST(N'2018-12-20' AS Date), 9)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (19, N'CS012', N'PR034', CAST(N'2018-12-20' AS Date), 8)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (20, N'CS013', N'PR035', CAST(N'2018-12-20' AS Date), 1)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (21, N'CS014', N'PR036', CAST(N'2018-12-20' AS Date), 2)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (22, N'CS015', N'PR037', CAST(N'2018-12-20' AS Date), 3)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (23, N'CS016', N'PR038', CAST(N'2018-12-20' AS Date), 4)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (24, N'CS017', N'PR039', CAST(N'2018-12-20' AS Date), 5)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (25, N'CS018', N'PR040', CAST(N'2018-12-20' AS Date), 6)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (27, N'CS020', N'PR042', CAST(N'2018-12-20' AS Date), 8)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (28, N'CS021', N'PR043', CAST(N'2018-12-20' AS Date), 9)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (29, N'CS022', N'PR044', CAST(N'2018-12-20' AS Date), 10)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (30, N'CS023', N'PR045', CAST(N'2018-12-20' AS Date), 9)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (31, N'CS024', N'PR046', CAST(N'2018-12-20' AS Date), 8)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (32, N'CS019', N'PR041', CAST(N'2018-12-20' AS Date), 0)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (36, N'CS019', N'PR146', CAST(N'2018-12-22' AS Date), 0)
INSERT [dbo].[WishAndCart] ([WishlistID], [CustomerID], [ProductID], [DateCreated], [IsCart]) VALUES (37, N'CS019', N'PR148', CAST(N'2018-12-22' AS Date), 0)
SET IDENTITY_INSERT [dbo].[WishAndCart] OFF
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_CommentCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_CommentCustomer]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_CommentProduct] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_CommentProduct]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[OrdersDetails]  WITH CHECK ADD FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[OrdersDetails]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_ProductsBrands] FOREIGN KEY([BrandID])
REFERENCES [dbo].[Brands] ([BrandID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_ProductsBrands]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_ProductsCategories] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([CategoryID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_ProductsCategories]
GO
ALTER TABLE [dbo].[ProductsDetails]  WITH CHECK ADD  CONSTRAINT [FK_ProductsProDetails] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[ProductsDetails] CHECK CONSTRAINT [FK_ProductsProDetails]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_RatingCustomer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_RatingCustomer]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_RatingProduct] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_RatingProduct]
GO
ALTER TABLE [dbo].[WishAndCart]  WITH CHECK ADD FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[WishAndCart]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
USE [master]
GO
ALTER DATABASE [psdb] SET  READ_WRITE 
GO
